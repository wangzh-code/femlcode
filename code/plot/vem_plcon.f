!$Id:$
      subroutine vem_plcon(k, nc, nel, xl, iplt, icl, v, vc, cont)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  date: 2020-4-27
!....  Email: wangzh@hrbust.edu.cn
!....  Author: Zhi-hai Wang

!-----[--.----+----.----+----.-----------------------------------------]
      implicit   none

      logical       :: cont
      integer       :: k, nc,nel
      integer       :: iplt(*), icl(*)
      real (kind=8) :: xl(3,*), v(*), vc(*)

      write(*,*) ' This option is available only in the VEM version'

      end subroutine vem_plcon
