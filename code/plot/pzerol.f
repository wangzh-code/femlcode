!$Id:$
      subroutine pzerol(fl,val,nn)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  date: 2020-4-27
!....  Email: wangzh@hrbust.edu.cn
!....  Author: Zhi-hai Wang

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Set an array to logical value = val

!      Inputs:
!         val       - Logical value: true or false
!         nn        - Length of array to set

!      Outputs:
!         fl(*)     - Array set to logical state val
!-----[--.----+----.----+----.-----------------------------------------]

      implicit  none

      integer       :: n,nn
      logical       :: fl(nn),val

      save

      do n = 1,nn
        fl(n) = val
      end do

      end subroutine pzerol
