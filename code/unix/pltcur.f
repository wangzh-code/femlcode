!$Id:$
      subroutine pltcur()

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--+---------+---------+---------+---------+---------+---------+-]

!     Routine to turn on screen cursor for text window
!     N.B. Not needed for UNIX version

      end subroutine pltcur
