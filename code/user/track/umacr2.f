!$Id:$
      subroutine umacr2(lct,ctl)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Regents of the University of California
!                               All rights reserved
!      author: Zhi-hai Wang
!      email: wangzh@hrbust.edu.cn
!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose:  track subroutine

!      Inputs:
!         lct       - Command character parameters
!         ctl(3)    - Command numerical parameters

!      Outputs:
!         N.B.  Users are responsible for command actions.  See
!               programmers manual for example.
!-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'umac1.h'
      include  'subpot.h'

      character (len=15) :: lct
      logical       :: pcomp
      real (kind=8) :: ctl(3)

      save

!     Set command word

      if(pcomp(uct,'mac2',4)) then
         uct = 'spoi'                     ! Specify 'name'
      elseif(urest.eq.1) then           ! Read  restart data

      elseif(urest.eq.2) then           ! Write restart data
      elseif(pcomp(lct, 'clean',5)) then
         !clean pointer and untrack subuoutine
         np_sub = 0
         !set defaut tack suboutine name
         call subp()
      elseif(pcomp(lct,'pcontr', 6)) then ! track pcontr.f
         np_sub(1) = 1
      elseif(pcomp(lct,'elmlib', 6)) then !track elmlib.f
         np_sub(2) = 1
      elseif(pcomp(lct,'palloc', 6)) then !track palloc.f
         np_sub(3) = 1
      elseif(pcomp(lct,'pmacr3', 6)) then !track pmacr3.f
         np_sub(4) = 1
      elseif(pcomp(lct,'pmatin', 6)) then !track pmatin.f
         np_sub(5) = 1
      elseif(pcomp(lct,'pform', 5)) then !track pfrom.f
         np_sub(6) = 1
      elseif(pcomp(lct,'forme', 6)) then !track forme.f
         np_sub(7) = 1
      elseif(pcomp(lct,'umarc1', 6)) then !track umarc1.f
         np_sub(8) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(9) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(10) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(11) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(12) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(13) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(14) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(15) = 1 
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(15) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(17) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(18) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(19) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(29) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(21) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(22) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(23) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(24) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(25) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(26) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(27) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(28) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(29) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(30) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(31) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(32) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(33) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(34) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(35) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(36) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(37) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(38) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(39) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(40) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(41) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(42) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(43) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(44) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(45) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(46) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(47) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(48) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
         np_sub(49) = 1
      elseif(pcomp(lct,'XXXXXX', 6)) then !track XXXXXX.f
      else                              ! Perform user operation

      endif

      end subroutine umacr2
