c$Id:$
      subroutine subp()

c      * * F E A P * * A Finite Element Analysis Program

c....  Copyright (c) 1984-2020: Regents of the University of California
c                               All rights reserved

c-----[--.----+----.----+----.-----------------------------------------]
c     Modification log                                Date (dd/mm/year)
c       Original version                                    01/11/2006
c-----[--.----+----.----+----.-----------------------------------------]
c      Purpose: initialize pointer for subroutine
c      Inputs:
c      Output:
c-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'subpot.h'

      integer   i
      save
c     Define and store names

      data   (name_sub(i),i=1,num_sub)/

     &    'pcontr',       !  1: pcontr.f
     &    'elmlib',       !  2: elmlib.f
     &    'palloc',       !  3: palloc.f
     &    'pmacr3',       !  4: pmacr3.f
     &    'pmatin',       !  5: pamtin.f
     &    'pfrom',        !  6: pfrom.f
     &    'forme',        !  7: forme.f
     &    'umarc1',       !  8: umarc1.f
     &    'sethis',       !  9: sethis.f
     &    'seteq',        !  10:seteq.f
     &    'pextnd',       !  11:pextnd.f
     &    'presol',       !  12:presol.f:
     &    'pmacr2',       !  13:pmacr2.f
     &    'plbou',        !  14:plbou.f
     &    'phillmandel',  !  15:phillmandel.f
     &    'pblend3',      !  16:pblend3.f
     &    'iters',        !  17:iters.f
     &    'peigsv',       !  18:peigsv.f
     &    'pblend2a',     !  19:pblend2a
     &    'restrt',       !  20:restrt.f
     &    'propld',       !  21:propld.f
     &    'pinitl',       !  22:pintil.f
     &    'pmacr1',       !  23:pmacr1.f
     &    'profil',       !  24:profil.f
     &    'pesruf',       !  25:pesruf.f
     &    'reader',       !  26:reader.f
     &    'blkgen',       !  27:blkgen.f
     &    'pblend1a',     !  28:pblend1a.f
     &    'pnewprob',     !  29:pnewprob.f
     &    'pmesh',        !  30:pmesh.f
     &    'psolve',       !  31:psolve.f
     &    'optid',        !  32:optid.f
     &    'plend1b',      !  33:plend1b
     &    'plend2b',      !  34:plend2b
     &    'pplotf',       !  35:pplotf,f
     &    'pline',        !  36:pline.f
     &    'plfacx',       !  37:plfacx.f
     &    'uiters',       !  38:uiters.f
     &    'ploadc',       !  39:ploadc.f
     &    '1     ',       !  40:
     &    '1     ',       !  41:
     &    '1     ',       !  42:
     &    '1     ',       !  43:
     &    '1     ',       !  44:
     &    '1     ',       !  45:
     &    '1     ',       !  46:
     &    '1     ',       !  47:
     &    '1     ',       !  48:
     &    '1     ',       !  49:
     &    '1     '/       !  50:

      end subroutine subp
