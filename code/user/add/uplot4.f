!$Id:$
      subroutine uplot4(ctl)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Dummy user plot routine

!      Inputs:
!         ctl(3) - Parameters for plots

!      Outputs:
!         none   - Users are responsible for generating outputs
!                  through common blocks, etc.  See programmer
!                  manual for example.
!-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include 'umac1.h'  ! uct

      logical       :: pcomp
      real (kind=8) :: ctl(3)

!     Provide user plot name

      if (pcomp(uct,'plt4',4)) then
!         uct = 'name'

!     Perform user plot function

      else

      end if

      end subroutine uplot4
