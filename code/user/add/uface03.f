!$Id:$
        subroutine uface03(uptyp,nel,iu,ufac)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Set plot data for user element

!      Inputs:
!         uptyp   - User element topology
!         nel     - Number of element nodes

!      Output:
!         iu(4,*) - 4-node quadrilateral face
!         ufac    - Number of faces
!-----[--.----+----.----+----.-----------------------------------------]
      implicit   none

      integer        :: uptyp,nel,ufac
      integer        :: iu(4,*)

      end subroutine uface03
