!$Id:$
      subroutine uprob

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!     Modification log                                Date (dd/mm/year)
!       Original version                                    10/04/2009
!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Dummy user problem file

!      Inputs:
!         none

!      Outputs:
!         none   - Users are responsible for generating problem inputs
!-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

!     Perform user problem inputs

      end subroutine uprob
