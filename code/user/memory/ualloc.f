c $Id:$
      logical function ualloc(num,name,length,precis)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
c-----[--.----+----.----+----.-----------------------------------------]
c      Purpose: Define, delete, or resize a user dictionary entry.
c               Pointer defined for integer (single) and real
c               (double precision) arrays.

c               N.B. Currently limited to 200 names by dimension of
c               common blocks 'allotd','allotn','pointer'

c      Inputs:
c         num        - Entry number for array (see below)
c         name       - Name of array          (see below)
c         length     - Length of array defined: =0 for delete
c         precis     - Precision of array: 1 = integers; 2 = reals

c      Output:
c         up(num)    - Pointer to first word of array in blank common
c-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'allotd.h'

      logical   usetmem
      character name*(*)
      integer   i, num,length,precis

c     Storage definitions for UALLOC variables

      integer   list

      parameter (list = 200)


      character names(list)*5

      save

c     Define and store names

      data   (names(i),i=1,list)/

     &         'SBDI1', !     1: Displacements for Shear Band problems, array 1
     &         'SBDI2', !     2: Displacements for Shear Band problems, array 2
     &         'TCURI', !     3: Index for time n+1 Displacements
     &         'TPREI', !     4: Index for time n   Displacements
     &         'RTEXP', !     5: Rates for explicit shear band problem
     &         'PVDCT', !     6: Counter for PVD File
     &         'ELEIL', !     7: Element eigenvalues
     &         'ELEIC', !     8: Element eigenvectors
     &         'ELEIN', !     9: Element number for eigenvalue calculation
     &         'SBELT', !    10: Element type identifier for shear band proble only in use by NURBS elements and NURBS related subroutines
     &         'PROJI', !    11: Info for projections
     &         'NPROJ', !    12: Number of projected quantities
     &         'FLPEQ', !    13: Flag to flip equation numbers
     &         'UTEM1', !    14: temporary
     &         'RELBC', !    15: Flag for released bc
     &         'Puuuu', !    16: degree of B-spline in x direction
     &         'Quuuu', !    17: degree of B-spline in y direction
     &         'Ruuuu', !    18: degree of B-spline in z direction
     &         'NCPuu', !    19: number of Ctrl Pts in x direction
     &         'MCPuu', !    20: number of Ctrl Pts in y direction
     &         'OCPuu', !    21: number of Ctrl Pts in z direction
     &         'UKNOT', !    22: knot vector in x direction
     &         'VKNOT', !    23: knot vector in y direction
     &         'WKNOT', !    24: knot vector in z direction
     &         'CPNET', !    25: control points net
     &         'INNuu', !    26: nurbs coordinate
     &         'IENuu', !    27: connectivity matrix
     &         'ELMTN', !    28: current element in assembly loop
     &         'ALENL', !    29: List of all nodes adjacent to a given node for ALE mesh rezoning algorithm
     &         'ALEBN', !    30: List of Free boundary nodes
     &         'ALEDI', !    31: Mesh displacement increment
     &         'ALEDP', !    32: Mesh displacement at time n
     &         'ALEV1', !    33: Mesh velocity array 1
     &         'ALEV2', !    34: Mesh velocity array 2
     &         'ACURI', !    35: Index for time n+1 ALE arrays
     &         'APREI', !    36: Index for time n   ALE arrays
     &         'INTND', !    37: Current node coordinates for interpolation
     &         'INTDF', !    38: Current node interpolated values
     &         'TRDOF', !    39: Mesh to mesh transfer code
     &         'NPPSD', !    40: Number of Post Processing Subdivision for NRUBS output
     &         'NNZEL', !    41: Number of non-zero elements on the NURBS mesh
     &         'UTEM2', !    42: User temporary
     &         'ONUMP', !    43: Number of nodes in the old mesh
     &         'ONUML', !    44: Number of elements in the old mesh
     &         'OCOOR', !    45: Nodes of old mesh
     &         'OELEM', !    46: Connectivity of the old mesh
     &         'Peeee', !    47: degree of B-spline in x direction
     &         'Qeeee', !    48: degree of B-spline in y direction
     &         'Reeee', !    49: degree of B-spline in z direction
     &         'NCPee', !    50: number of Ctrl Pts in x direction
     &         'MCPee', !    51: number of Ctrl Pts in y direction
     &         'OCPee', !    52: number of Ctrl Pts in z direction
     &         'UKeee', !    53: knot vector in x direction
     &         'VKeee', !    54: knot vector in y direction
     &         'WKeee', !    55: knot vector in z direction
     &         'CNETe', !    56: control points net
     &         'INNee', !    57: nurbs coordinate
     &         'IENee', !    58: connectivity matrix
     &         'TFLDU', !    59: NURBS output fields for primary variables
     &         'TFLDE', !    60: NURBS output fields for secondary variables
     &         'IENPL', !    61: NURBS postprocessing connectivity
     &         'ELERQ', !    62: Element Sol. Rayleigh-Quotient and others
     &         'ELEDP', !    63: Element Solution (dot) eigenvectors
     &         'LASTN', !    64: Last node for adding middle nodes
     &         'GPCRD', !    65: Gauss Points coordinate, size=numel*lint*3
     &         'NUMPP', !    66: Plotting mesh information
     &         'NPLND', !    67: Number of noded in the plotting mesh
     &         'GPNEI', !    68: Number of gauss points used to compute the non local average value at the current GP
     &         'SEMIB', !    69: Something to do with releasing the Dirichlet BCs
     &         'LSARM', !    70: Flag to use Armijo rule in line search **CHECK**
     &         'DUARR', !    71: du array for SNES
     &         'TEMP3', !    72:
     &         'UPRDO', !    73: User prop load displacement offset
     &         'GLBEV', !    74: Global Eigenvectors
     &         'Ukntb', !    75: Bbar Knot vector in u direction
     &         'Vkntb', !    76: Bbar Knot vector in v direction
     &         'CPntb', !    77: Bbar Control Point Net
     &         'INNbb', !    78: NURBS coordinate for Bbar
     &         'IENbb', !    79: Element connectivity for Bbar
     &         'Bbarf', !    80: NURBS Bbar flag
     &         'ELMbb', !    81: 
     &         'EEIGV', !    82: Elements Eigenvalues
     &         'EEIGN', !    83: Number of element eigenvalues to store
     &         'GLBEN', !    84: Number of Global Eigenpairs to store
     &         'GLVAL', !    85: Global Eigenvalues
     &         'NPSXL', !    86: Node coord for NURBS-PS element 22
     &         'NPSIE', !    87: Local connectivity for NURBS-PS
     &         'STOPI', !    88: Integers for stopping criteria
     &         'STOPD', !    89: doubles for stopping criteria
     &         'USNTM', !    90:
     &         'NSTOP', !    91: Number of stopping criteria
     &         'FLDNM', !    92: Field Norms
     &         'Puubb', !    93: degree of B-spline in x direction for bbar
     &         'Quubb', !    94: degree of B-spline in y direction for bbar
     &         'NCPub', !    95: number of Ctrl Pts in x direction for bbar
     &         'MCPub', !    96: number of Ctrl Pts in y direction for bbar
     &         'Ngpts', !    97: total number of gauss points used for integration = lint ~ lgp**2
     &         'LSTRP', !    98: Line search or trust region parameters
     &         'LPRED', !    99: Flag for local predictor
     &         'PTCDL', !   100: Psuedo transient continuation flag
     &         'GPVAL', !   101: Gauss-Point Values for Stability
     &         'GPVnu', !   102: Number of Fields and GP to export
     &         'ERRCV', !   103: Error teria flag
     &         'STAGN', !   104: Info for staggered scheme
     &         'NOMON', !   105: Flag to not monitor a field
     &         'FSIND', !   106: Field indices
     &         'FSMAX', !   107: Max dofs per field
     &         'OFDNZ', !   108: Nonzeros of local jacobian
     &         'FLLRO', !   109: Fill ratio needed for lu
     &         'GPIEN', !   110: Connectivities for each GP.
     &         'LTOGN', !   111: Local to global ordering of nodes in parallel
     &         'GTOLN', !   112: Global to local ordering of nodes in parallel
     &         'LTOGE', !   113: Local to global ordering of equations in parallel
     &         'GOSTP', !   114: Ghost points flag (ghost=0, not ghost=1)
     &         'GLBRQ', !   115: Glob RayQ and Error
     &         'GLBDP', !   116: Glob Dot-products
     &         'OFDNP', !   117:
     &         'MXITR', !   118:
     &         'PFILM', !   119:       
     &         'GHEVL', !   120: Glob Herm Eigenvalues
     &         'EQCOD', !   121: Equation codes for local matrix dofs       
     &         'LGSFL', !   122: Flag for local update of local dofs
     &         'LOCIN', !   123: Settigns for local inverses       
     &         'LOCOP', !   124: Settings for local operations  
     &         'NSTGB', !   125: Number of equations for each nest block       
     &         'NSTLC', !   126: Number of processor equations for each nest block       
     &         'MTNST', !   127: Info for mat nest       
     &         'DFCOD', !   128: Equation codes for dofs      
     &         'NSTGL', !   129: Global to local mapping for        
     &         'NSTNZ', !   130: Nonzeros for nest matrices       
     &         'NZOFF', !   131: Offsets for NSTNZ       
     &         'NSTNM', !   132: Nest indicator       
     &         'NSTSU', !   133: Flag for nest matrix storage setup       
     &         'LINEI', !   134: Integer array for line integrals       
     &         'LINED', !   135: Double array for line integrals       
     &         'SOLVE', !   136: Solver options code
     &         'LOCSL', !   137: Locally computed solution increments       
     &         'LUPFL', !   138: Flag for local updates       
     &         'RESD0', !   139: Flag for initial residual calculation
     &         'NOCNV', !   140: Flag to force no nonlinear convergence       
     &         'LCPRJ', !   141: Projections for local increments
     &         'RMUPT', !   142: Increment for return mapping step
     &         'GPSOL', !   143: Gauss point solution quantitites       
     &         'GPNRM', !   144: Norms for gp residuals       
     &         'GPDAT', !   145: Length of 144       
     &         'GPINC', !   146: Increment in gauss point quantitites
     &         'GPSLC', !   147: Element gauss point solution quantities
     &         'GPSTM', !   148: Temporary storage for GPSOL
     &         'NOMGP', !   149: Flag to not monitor GP residuals
     &         'EIGRQ', !   150: Element Instantaneous Growth and RQ
     &         'ENABS', !   151: Element Numerical Abscissa
     &         'EDENS', !   152: Topology Optimization Element Densities
     &         'ESENS', !   153: Topology Optimization Usually Volume Fraction Sensitivities
     &         'ELVOL', !   154: Topology Optimization Element Volumes
     &         'DENEX', !   155: Topology Optimization Element Density Exponent
     &         'CSENS', !   156: Topology Optimization Flag to Compute Sensitivities (deprecated)
     &         'TOFLG', !   157: Topology Optimization General Flags for Path-Dependent Sensitivities
     &         'ADJT1', !   158: Topology Optimization Adjoint Vector 1 for Constraint 1 (Holds the Adjoint for nodal dofs)
     &         'ADJT2', !   159: Topology Optimization Adjoint Vector 1 for Constraint 2 (Holds the Adjoint for nodal dofs)
     &         'CNSTR', !   160: Topology Optimization Vector of Constraint Values
     &         'CSEN1', !   161: Topology Optimization Constraint 1 Sensitivities
     &         'CSEN2', !   162: Topology Optimization Constraint 2 Sensitivities
     &         'REACT', !   163: Reaction Forces Computed Since FEAP Cannot Do This Correctly
     &         'PRMAG', !   164: Proportional Load Magnitude
     &         'AJT1W', !   165: Topology Optimization Adjoint Vector 2 for Constraint 1 (Holds the Adjoint for gauss pt dofs)
     &         'AJT2W', !   166: Topology Optimization Adjoint Vector 2 for Constraint 2 (Holds the Adjoint for gauss pt dofs)
     &         'PTOPO', !   167: Topology Optimization Pij constants for MMA Optimizer
     &         'QTOPO', !   168: Topology Optimization Qij constants for MMA Optimizer
     &         'ASYMP', !   169: Topology Optimization Upper and Lower Asymptotes for MMA Optimizer
     &         'PSENS', !   170: Topology Optimization Projected Density Sensitivity Scale Factors (chain rule)
     &         'SAVEU', !   171: Save the current solution vector into this vector
     &         'STGFL', !   172: Flag used for staggered solution with phase field fracture
     &         'ASIZE', !   173: Topology Optimization simulation data array sizes
     &         'WSENS', !   174: Topology Optimization store W+ and W- work sensitivities
     &         'WADJT', !   175: Topology Optimization store W- work nodal adjoint vectors
     &         'WGPAD', !   176: Topology Optimization store W- work gauss point adjoint vectors
     &         'CKINT', !   177: Topology Optimization used for storing values of the time integral of the crack surface functional
     &         'CISEN', !   178: Topology Optimization Crack Surface Functional Integral Sensitivities
     &         'CNADT', !   179: Topology Optimization Crack Surface Functional Integral Nodal Adjoint Vector
     &         'CGPAD', !   180: Topology Optimization Crack Surface Functional Integral Gauss Pt Adjoint Vector
     &         '     ', !   181: <empty>
     &         '     ', !   182: <empty>
     &         '     ', !   183: <empty>
     &         '     ', !   184: <empty>
     &         '     ', !   185: <empty>
     &         '     ', !   186: <empty>
     &         '     ', !   187: <empty>
     &         '     ', !   188: <empty>
     &         '     ', !   189: <empty>
     &         '     ', !   190: <empty>
     &         '     ', !   191: <empty>
     &         '     ', !   192: <empty>
     &         '     ', !   193: <empty>
     &         '     ', !   194: <empty>
     &         '     ', !   195: <empty>
     &         '     ', !   196: <empty>
     &         '     ', !   197: <empty>
     &         '     ', !   198: <empty>
     &         '     ', !   199: <empty>
     &         '     '/ !   200: <empty>
c     Do memory management operations
      ualloc = usetmem(list,names,num,name,length,precis)
      end function ualloc
