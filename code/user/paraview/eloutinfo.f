!$Id:$
c      -----

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2017: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
c----------------------------------------------------------------
c     --- Output array names and # of components ---
      !subroutine getNames{{{1
      subroutine getnames(names)
c     Description: Function to set the names and numbers of the fields
c                  that can be exported by the functions that write
c                  to the VTU file.
    
      implicit none

      include  'prjnam.h'

      character names(numNames)*30

      names='Empty Slot'

      names(1 ) = 'Displacements'
      names(2 ) = 'Velocities'
      names(3 ) = 'Accelerations'
      names(4 ) = 'Temperature'
      names(5 ) = 'Stresses'
      names(6 ) = 'Eq_Plastic_Strain'
      names(7 ) = 'Von_Mises_Stress'
      names(8 ) = 'Strain'
      names(9 ) = 'Diffusive_Term'
      names(10) = 'Source_Term'
      names(11) = 'Stability_Criteria'
      names(12) = 'Global_Ev_Displacements 0'
      names(13) = 'Global_Ev_Temperature 0'
      names(14) = 'Global_Ev_Stresses 0'
      names(15) = 'Global_Ev_EQPS 0'
      names(16) = 'Phase_Field'
      names(17) = 'El_Helmholtz_Free_Energy'
      names(18) = 'Phase_Field_Diffusive Term'
      names(19) = 'Phase Field Velocity'
      names(20) = 'Phase Field Acceleration'
      names(21) = 'Global Ev. Phase Field'
      names(22) = 'Effective Gc'
      names(23) = 'Phase_Field_Source_Term'
      names(24) = 'Surface_Energy'
      names(25) = 'Global_Ev_Velocity 0'
      names(26) = 'Rate of Deformation'
      names(27) = 'Mechanical Work Rate'
      names(28) = 'Damage'
      names(29) = 'Principal_Stress'
      names(30) = 'Principal_Strain'
      names(31) = 'Hydrostatic Stress'
      names(32) = 'Druckers Postulate'
      names(33) = 'Element Eigv - Real Part'
      names(34) = 'Strain rate'
      names(35) = 'Int. Variable Free Energy'
      names(36) = 'Rate of Energy Storage'
      names(37) = 'Rate of Stress'
      names(38) = 'TQ Fraction'
      names(39) = 'WMA Stability Criterion'
      names(40) = 'Jacobian Determinant'
      names(41) = 'Elastic Left CG Tensor'
      names(42) = 'NLTQ Stability Criterion'
      names(43) = 'Vol. Strain'
      names(44) = 'Vol. El. Strain Energy'
      names(45) = 'Dev. El. Strain Energy'
      names(46) = 'El. Strain'
      names(47) = 'Undamaged Stress'
      names(48) = 'Damaged Stress'
      names(49) = 'Shear Slip'
      names(50) = 'Internal Variable'
      names(51) = 'Local Damage'
      names(52) = 'Non Local Damage'
      names(53) = 'Shear Slip Rate'
      names(54) = 'Inelastic Work Rate'
      names(55) = 'Incremental Vol Change'
      names(56) = 'Incremental Vol Change B.E.'
      names(57) = 'Incremental Vol Change Exp'
      names(58) = 'Stored Work Rate'
      names(59) = 'Eq. Plastic Strain Rate'
      names(60) = 'Principal Elastic Stretch'
      names(61) = 'Reacted Fraction'
      names(62) = 'Reaction Rate'
      names(63) = 'Global Ev. Displacements 1'
      names(64) = 'Global Ev. Velocity 1'
      names(65) = 'Global Ev. Temperature 1'
      names(66) = 'Global Ev. Stresses 1'
      names(67) = 'Global Ev. EQPS 1'
      names(68) = 'Global Ev. Displacements 2'
      names(69) = 'Global Ev. Velocity 2'
      names(70) = 'Global Ev. Temperature 2'
      names(71) = 'Global Ev. Stresses 2'
      names(72) = 'Global Ev. EQPS 2'
      names(73) = 'Global Ev. Displacements 3'
      names(74) = 'Global Ev. Velocity 3'
      names(75) = 'Global Ev. Temperature 3'
      names(76) = 'Global Ev. Stresses 3'
      names(77) = 'Global Ev. EQPS 3'
      names(78) = 'PK1 Stress'
      names(79) = 'Degradation Function'
      names(80) = 'Degraded Free Energy'
      names(81) = 'Phase Field Dissipative Term'
      names(82) = 'Dev. Stress'
      names(83) = 'Vol. Stress'
      names(84) = 'Elastic Work Rate'
      names(85) = 'Thermal Work Rate'
      names(86) = 'Kinetic Energy Rate'
      names(87) = 'Phase Field Gradient'
      names(88) = 'Kinetic_Energy'
      names(89) = 'PF Param A'
      names(90) = 'PF Param B'
      names(91) = 'PF RootsR'
      names(92) = 'Damage Rate'
      names(93) = 'Damage Accel.'
      names(94) = 'PF Phi'
      names(95) = 'Dissipated Energy Rate'
      names(96) = 'X (Eq. Stress)'
      names(97) = 'X Rate (dXdt)'
      names(98) = 'X Accel.(d2Xdt2)'
      names(99) = 'PF Phicr'
      names(100) = 'Global Ev. PhaseField 0'
      names(101) = 'Global Ev. PhaseField 1'
      names(102) = 'Global Ev. PhaseField 2'
      names(103) = 'Global Ev. PhaseField 3'
      names(104) = 'Element RQ'
      names(105) = 'Element IG'
      names(106) = 'Element NA'
      names(107) = 'Permeability'
      names(108) = 'Fluid_pressure'
      names(109) = 'Degraded_El_Free_Energy'
      names(110) = 'Pressure_derivative'
      names(111) = 'Damage_diss_energy_rate'
      names(112) = 'Viscous_diss_energy_rate'
      names(113) = 'Reaction_forces'
      names(114) = 'Heat_flux'
      names(115) = 'Gradc'
      names(116) = 'Thermal_Degradation_Function'
      names(117) = 'Reac_forces'
      names(150) = 'Tempdot' 
      names(151) = 'Tempddot' 
      names(152) = 'Tempgra' 
      names(153) = 'Thermal_Strain' 
      names(154) = 'Elastic_Strain' 
      names(155) = 'FlucE' 
      names(156) = 'Ela_Vol_Strain_Ene' 
      names(157) = 'Ela_Dev_Strain_Ene' 
      names(158) = 'Vis_Dissipation' 
      names(159) = 'Ela_Vol_Stress' 

      names(160) = 'Ela_Dev_Stress' 
      names(161) = 'Vis_Dev_Stress' 
      names(162) = 'Elastic_Strain_Energy' 
      names(163) = 'Dev_Strain'
      names(164) = 'Total_Energy'
      names(165) = 'Damaged_Energy'
      names(166) = 'Vis_Strain'
      names(167) = 'HelmP'
      names(168) = 'Time'
      names(169) = 'StressesInPrincipal'
      names(170) = 'StrainInPrincipal'
      names(171) = 'PrincipalDir1'
      names(172) = 'PrincipalDir2'
      names(173) = 'PrincipalDir3'
      names(174) = 'FlucGc'
      names(175) = 'RhoBone'
      names(176) = 'Phase_Field_New'
      names(177) = 'Separations'
      names(178) = 'Tractions'
      names(179) = 'Coh_Dissipated_Energy' !Reserved for Miguel
      
      names(180) = 'RJGL0' !Reserved for Juan
      names(181) = 'RJGL1' !Reserved for Juan
      names(182) = 'RJGL2' !Reserved for Juan
      names(183) = 'RJGL3' !Reserved for Juan
      names(184) = 'RJGL4' !Reserved for Juan
      names(185) = 'RJGL5' !Reserved for Juan
      names(186) = 'RJGL6' !Reserved for Juan
      names(187) = 'RJGL7' !Reserved for Juan
      names(188) = 'RJGL8' !Reserved for Juan
      names(189) = 'RJGL9' !Reserved for Juan
      
      names(190) = 'RLuc0' !Reserved for Luc
      names(191) = 'RLuc1' !Reserved for Luc
      names(192) = 'RLuc2' !Reserved for Luc
      names(193) = 'RLuc3' !Reserved for Luc
      names(194) = 'RLuc4' !Reserved for Luc
      names(195) = 'RLuc5' !Reserved for Luc
      names(196) = 'RLuc6' !Reserved for Luc
      names(197) = 'RLuc7' !Reserved for Luc
      names(198) = 'Sensitivity_1' !for topology optimization (Jonathan Russ)
      names(199) = 'Sensitivity_2' !for topology optimization (Jonathan Russ)
      names(200) = 'Element_Densities' !for topology optimization (Jonathan Russ)

      end subroutine getnames
      !1}}}
      !subroutine getncomp{{{1
      subroutine getncomp(nc,i)

      implicit none

      include  'prjnam.h'

      integer ncompa(numnames),i,nc

               !ncomp,--------------]
      ncompa(1 ) = 3!'Displacements'
      ncompa(2 ) = 3!'Velocities'
      ncompa(3 ) = 3!'Accelerations'
      ncompa(4 ) = 1!'Temperature'
      ncompa(5 ) = 6!'Stresses'
      ncompa(6 ) = 1!'Eq. Plastic Strain'
      ncompa(7 ) = 1!'Von Mises Stress'
      ncompa(8 ) = 6!'Strain'
      ncompa(9 ) = 1!'Diffusive Term'
      ncompa(10) = 1!'Source Term'
      ncompa(11) = 1!'Stability Criteria'
      ncompa(12) = 3!'Global Ev. Displacements'
      ncompa(13) = 1!'Global Ev. Temperature'
      ncompa(14) = 6!'Global Ev. Stresses'
      ncompa(15) = 1!'Global Ev. EQPS'
      ncompa(16) = 1!'Phase_Field'
      ncompa(17) = 1!'El_Helmholtz_Free_Energy'
      ncompa(18) = 1!'Phase_Field_Diffusive_Term'
      ncompa(19) = 1!'Phase_Field_Velocity'
      ncompa(20) = 1!'Phase Field Acceleration'
      ncompa(21) = 1!'Global Ev. Phase Field'
      ncompa(22) = 1!'Effective Gc'
      ncompa(23) = 1!'Phase_Field_Source_Term'
      ncompa(24) = 1!'Surface_Energy'
      ncompa(25) = 3!'Global Ev. Velocity'
      ncompa(26) = 6!'Rate_of_Deformation'
      ncompa(27) = 1!'Mechanical_Work_Rate'
      ncompa(28) = 1!'Damage'
      ncompa(29) = 3!'Principal_Stress'
      ncompa(30) = 3!'Principal_Strain'
      ncompa(31) = 1!'Hydrostatic_Stress'
      ncompa(32) = 1!'Druckers_Postulate'
      ncompa(33) = 1!'Element Eigv - Real Part'
      ncompa(34) = 6!'Strain_rate'
      ncompa(35) = 1!'Int. Variable Free Energy'
      ncompa(36) = 6!'Rate of Energy Storage'
      ncompa(37) = 6!'Rate of Stress'
      ncompa(38) = 1!'TQ Fraction'
      ncompa(39) = 1!'WMA Stability Criterion'
      ncompa(40) = 1!'Jacobian Determinant'
      ncompa(41) = 6!'Elastic Left CG Tensor'
      ncompa(42) = 1!'NLTQ Stability Criterion'
      ncompa(43) = 1!'Vol. Strain'
      ncompa(44) = 1!'Vol. El. Strain Energy'
      ncompa(45) = 1!'Dev. El. Strain Energy'
      ncompa(46) = 6!'El. Strain'
      ncompa(47) = 6!'Undamaged Stress'
      ncompa(48) = 6!'Damaged Stress'
      ncompa(49) = 1!'Shear Slip'
      ncompa(50) = 1!'Internal Variable'
      ncompa(51) = 1!'Local Damage'
      ncompa(52) = 1!'Non Local Damage'
      ncompa(53) = 12!'Shear Slip Rate' !!!!This needs to be adjustable
      ncompa(54) = 1!'Inelastic Work Rate'
      ncompa(55) = 1!'Incremental Volume Change'
      ncompa(56) = 1!'Incremental Volume Change BE'
      ncompa(57) = 1!'Incremental Volume Change Exp'
      ncompa(58) = 1!'Stored Work Rate'
      ncompa(59) = 1!'Eq. Plastic Strain Rate'
      ncompa(60) = 3!'Principal Elastic Stretch'
      ncompa(61) = 1!'Reacted Fraction'
      ncompa(62) = 1!'Reaction Rate'
      ncompa(78) = 9!'PK1 Stress'
      ncompa(79) = 1!'Degradation Function'
      ncompa(80) = 1!'Degraded Free Energy'
      ncompa(81) = 1!'Phase Field Dissipative Term'
      ncompa(82) = 6!'Dev. Stress'
      ncompa(83) = 6!'Vol. Stress'
      ncompa(84) = 1!'Elastic Work Rate'
      ncompa(85) = 1!'Thermal Work Rate'
      ncompa(86) = 1!'Kinetic Energy Rate'
      ncompa(87) = 3!'Phase Field Gradient'
      ncompa(88) = 1!'Kinetic Energy'
      ncompa(89) = 1!'PF Param A'
      ncompa(90) = 1!'PF Param B'
      ncompa(91) = 5!'PF RootsR'
      ncompa(92) = 1!'Damage Rate'
      ncompa(93) = 1!'Damage Accel.'
      ncompa(94) = 1!'PF Phi'
      ncompa(95) = 1!'Dissipated Energy Rate'
      ncompa(96) = 1!'X (Eq. Stress)'
      ncompa(97) = 1!'X Rate (dXdt)'
      ncompa(98) = 1!'X Accel.(d2Xdt2)'
      ncompa(99) = 1!'PF Phicr
      ncompa(100) = 1!'Global Ev. PhaseField 0'
      ncompa(101) = 1!'Global Ev. PhaseField 1'
      ncompa(102) = 1!'Global Ev. PhaseField 2'
      ncompa(103) = 1!'Global Ev. PhaseField 3'
      ncompa(104) = 1!'Element RQ'
      ncompa(105) = 1!'Element IG'
      ncompa(106) = 1!'Element NA'
      ncompa(107) = 1!'Permeability'
      ncompa(108) = 1!'Fluidpressure'
      ncompa(109) = 1!'Degraded El. Free Energy'
      ncompa(110) = 2!'Pressure Derivative'
      ncompa(111) = 1!'Damage diss. energy rate'
      ncompa(112) = 1!'Viscous diss. energy rate'
      ncompa(113) = 3!'Reaction_forces'
      ncompa(114) = 2!'Heat flux'
      ncompa(115) = 2!'Gradc' 2D
      ncompa(116) = 1!'Thermal Degradation Function'
      ncompa(117) = 3!'React_forces'
      ncompa(150) = 1 !'Temperature_velocity' 
      ncompa(151) = 1 !'Temperature_acceleration'
      ncompa(152) = 3 !'Temperature_gradient'
      ncompa(153) = 6 !'Thermal_strain'
      ncompa(154) = 6 !'Elastic_strain'
      ncompa(155) = 1 !'Young's Modulus fluctuation'
      ncompa(156) = 1 !'Ela_Vol_Strain Ene'
      ncompa(157) = 1 !'Ela_Dev_Strain Ene'
      ncompa(158) = 1 !'Vis_Dev_Dissipation'
      ncompa(159) = 1 !'Ela_Vol_Stress'
      ncompa(160) = 6 !'Ela_Dev_Stress'
      ncompa(161) = 6 !'Vis_Dev_Stress'
      ncompa(162) = 1 !'Elastic_Strain_Energy'
      ncompa(163) = 6 !'Dev_Strain'
      ncompa(164) = 1 !'Total_Energy'
      ncompa(165) = 1 !'Damaged_Energ'
      ncompa(166) = 6 !'Viscous_Strain'
      ncompa(167) = 1 !'HelmP'
      ncompa(168) = 1 !'Time'
      ncompa(169) = 6 !'StressesInPrincipal'
      ncompa(170) = 6 !'StrainInPrincipal'
      ncompa(171) = 1 !'PrincipalDir1'
      ncompa(172) = 1 !'PrincipalDir2'
      ncompa(173) = 1 !'PrincipalDir3'
      ncompa(174) = 1 !'FlucGc'
      ncompa(175) = 1 !'RhoBone'
      ncompa(176) = 1 !'Phase_Field_New'
      ncompa(177) = 2 !'Separations'
      ncompa(178) = 2 !'Tractions'
      ncompa(179) = 2 !'Coh_Dissipated_Energy'
      ncompa(180) = 1 !'RJGL0'
      ncompa(181) = 1 !'RJGL1'
      ncompa(182) = 1 !'RJGL2'
      ncompa(183) = 1 !'RJGL3'
      ncompa(184) = 1 !'RJGL4'
      ncompa(185) = 1 !'RJGL5'
      ncompa(186) = 1 !'RJGL6'
      ncompa(187) = 1 !'RJGL7'
      ncompa(188) = 1 !'RJGL8'
      ncompa(189) = 1 !'RJGL9'
      ncompa(190) = 1 !'RLuc0'
      ncompa(191) = 1 !'RLuc1'
      ncompa(192) = 1 !'RLuc2'
      ncompa(193) = 1 !'RLuc3'
      ncompa(194) = 1 !'RLuc4'
      ncompa(195) = 1 !'RLuc5'
      ncompa(196) = 1 !'RLuc6'
      ncompa(197) = 1 !'RLuc7'
      ncompa(198) = 1 !Sensitivity 1
      ncompa(199) = 1 !Sensitivity 2
      ncompa(200) = 1 !Element Densities

      nc = ncompa(i)

      end
      !1}}}

c     --- Element node setup ---
!     subroutine getnumnpp{{{1      
      subroutine getnumnpp(uelpn,numnpp)

      implicit none

      include  'cdat1.h'
      include  'cdata.h'
      include  'sdata.h'
      include  'pointer.h'
      include  'comblk.h'
      include  'iofile.h'

      integer uelpn(*),uelmn(99,2),l,mat,elt,i,nenc,cntr
c      integer uelpn(*),uelmn(50,2),l,mat,elt,i,nenc,cntr
      integer node,ii,bserchi,j,numnpp
      
c      l = 50
      l = 99

      call pzeroi(uelpn,2*numnp)      
      call getelnums(uelmn)

      cntr = 0
      do i = 1,numel
          mat = mr(np(33) + nen1*(i-1) + nen1-1) !material set number
          elt = mr(np(32) + nie*(mat-1) + nie-2) !element type for material set mat
          if (elt.gt. 0) then
             nenc = uelmn(elt,1)
          else
             nenc = nen
          endif
          do ii = 1,nenc
             node = mr(np(33) + (ii-1) + nen1*(i-1))
             if (node .ne. 0 .and. uelpn(node) .eq. 0) then
                cntr = cntr + 1
                uelpn(node) = cntr
             endif
          enddo
       end do
       numnpp = cntr
       !determine local to global node mapping
       cntr = 0
       do i = 1,numnp
           if (uelpn(i) .ne. 0) then
               cntr = cntr + 1
               node = 0
               j = 0
               do while (numnp.gt.j)
                   if (uelpn(j).eq. cntr) then
                      node = j
                      j = numnp+1
                   endif
                   j  = j+1
               enddo
               uelpn(numnp+i) = node
           endif
       enddo
       

      end
!1}}}      
!     subroutine getelnums{{{1      
      subroutine getelnums(uelmn)
         !Function to get number of main nodes (used for plotting and
         !typically element independent) and number of internal nodes
         !(used for internal computations)

      implicit none

      include  'cdat1.h'
      include  'cdata.h'
      include  'sdata.h'
      include  'pointer.h'
      include  'comblk.h'
      include  'iofile.h'
      include  'elname.h'

      integer uelmn(num_user_el,2),l,mat,elt,i,nenc,cntr
c      integer uelmn(50,2),l,mat,elt,i,nenc,cntr
      integer node,ii,bserchi,j
      
c      l = 50
c      l = 99

c     uelmn(el,1:2)  = (num of plotting nodes, num of internal nodes)
      uelmn          = 0
      uelmn(1 ,1:2)  = (/2,0/)

      uelmn(2 ,1:2)  = (/4,0/)
      
      uelmn(3 ,1:2)  = (/4,0/)
                         
      uelmn(4 ,1:2)  = (/4,0/)
      
      uelmn(5 ,1:2)  = (/4,0/)

      uelmn(6 ,1:2)  = (/4,4/)
      
      uelmn(7 ,1:2)  = (/4,5/)
      
      uelmn(8 ,1:2)  = (/4,5/)
      
      uelmn(9 ,1:2)  = (/4,4/)
      
      uelmn(13,1:2)  = (/4,4/)
      
      uelmn(14,1:2)  = (/1,0/)

      uelmn(15,1:2)  = (/4,4/)

      uelmn(16,1:2)  = (/2,0/)
      
      uelmn(17,1:2)  = (/4,0/)
      
      uelmn(18,1:2)  = (/4,0/)

      uelmn(19,1:2)  = (/4,0/)

      uelmn(20,1:2)  = (/4,0/)

      uelmn(21,1:2)  = (/4,0/)

      uelmn(22,1:2)  = (/4,0/)

      uelmn(23,1:2)  = (/4,0/)
      
      uelmn(24,1:2)  = (/4,0/)
      
      uelmn(25,1:2)  = (/2,1/)
      
      uelmn(26,1:2)  = (/4,0/)
      
      uelmn(27,1:2)  = (/4,0/)
      
      uelmn(28,1:2)  = (/4,0/)
      
      uelmn(29,1:2)  = (/4,0/)
      
      uelmn(30,1:2)  = (/4,0/)

      uelmn(31,1:2)  = (/4,0/)
      
      uelmn(32,1:2)  = (/4,0/)
      
      uelmn(33,1:2)  = (/4,0/)
      
      uelmn(34,1:2)  = (/8,0/)
      
      uelmn(35,1:2)  = (/4,0/)
      
      uelmn(36,1:2)  = (/4,0/)
      
      uelmn(37,1:2)  = (/8,0/)
      
      uelmn(38,1:2)  = (/4,0/)
      
      uelmn(39,1:2)  = (/4,0/)
      
      uelmn(40,1:2)  = (/4,0/)
      
      uelmn(41,1:2)  = (/4,0/)
      
      uelmn(42,1:2)  = (/4,0/)
      
      uelmn(43,1:2)  = (/4,0/)
      
      uelmn(44,1:2)  = (/4,0/)

      uelmn(45,1:2)  = (/4,0/)
      
      uelmn(46,1:2)  = (/4,0/)

      uelmn(47,1:2)  = (/3,0/)

      uelmn(48,1:2)  = (/4,0/)
      
      uelmn(49,1:2)  = (/4,0/)
      
      uelmn(50,1:2)  = (/4,4/)
                    
      uelmn(51,1:2)  = (/4,0/)
     
      uelmn(52,1:2)  = (/4,0/)
     
      uelmn(53,1:2)  = (/4,0/)
     
      uelmn(54,1:2)  = (/4,0/)
     
      uelmn(55,1:2)  = (/4,0/)
     
      uelmn(56,1:2)  = (/4,0/)
     
      uelmn(57,1:2)  = (/8,0/)
     
      uelmn(58,1:2)  = (/4,0/)
     
      uelmn(59,1:2)  = (/8,0/)
     
      uelmn(60,1:2)  = (/4,0/)
              
      uelmn(61,1:2)  = (/8,0/)
     
      uelmn(62,1:2)  = (/4,0/)
     
      uelmn(63,1:2)  = (/10,0/)
     
      uelmn(64,1:2)  = (/4,0/)
     
      uelmn(65,1:2)  = (/4,0/)
     
      uelmn(66,1:2)  = (/4,0/)
     
      uelmn(67,1:2)  = (/4,0/)
     
      uelmn(68,1:2)  = (/4,0/)
     
      uelmn(69,1:2)  = (/4,0/)
     
      uelmn(70,1:2)  = (/4,0/)
              
      uelmn(71,1:2)  = (/4,0/)
     
      uelmn(72,1:2)  = (/4,0/)
     
      uelmn(73,1:2)  = (/4,0/)
     
      uelmn(74,1:2)  = (/4,0/)
     
      uelmn(75,1:2)  = (/4,0/)
     
      uelmn(76,1:2)  = (/4,0/)
     
      uelmn(77,1:2)  = (/4,0/)
     
      uelmn(78,1:2)  = (/4,0/)
     
      uelmn(79,1:2)  = (/4,0/)
     
      uelmn(80,1:2)  = (/4,0/)
              
      uelmn(81,1:2)  = (/4,0/)
     
      uelmn(82,1:2)  = (/4,0/)
     
      uelmn(83,1:2)  = (/4,0/)
     
      uelmn(84,1:2)  = (/4,0/)
     
      uelmn(85,1:2)  = (/4,0/)
     
      uelmn(86,1:2)  = (/4,0/)
     
      uelmn(87,1:2)  = (/4,0/)
     
      uelmn(88,1:2)  = (/4,0/)
     
      uelmn(89,1:2)  = (/4,0/)
     
      uelmn(90,1:2)  = (/4,0/)
              
      uelmn(91,1:2)  = (/4,0/)
     
      uelmn(92,1:2)  = (/4,0/)
     
      uelmn(93,1:2)  = (/4,0/)
     
      uelmn(94,1:2)  = (/4,0/)
     
      uelmn(95,1:2)  = (/4,0/)
     
      uelmn(96,1:2)  = (/4,0/)
     
      uelmn(97,1:2)  = (/4,0/)
     
      uelmn(98,1:2)  = (/4,0/)
     
      uelmn(99,1:2)  = (/4,0/)
      uelmn(100,1:2) = (/4,0/)
     
      end
!1}}}      

c     --- Send to program/pelmin.f  -?-
      !subroutine getElfromMat(mat,ie,iel) {{{
      subroutine getElfromMat(mat,ie,iel)

      implicit none

      include  'cdat1.h'

      integer  mat,iel,ie(nie,*)


      iel = ie(nie-1,mat)

      end
c     }}}      
      !subroutine setMiddleNode(nty,x,nn) {{{
      subroutine setMiddleNode(nty,x,nn)

      implicit none

      include  'sdata.h'

      integer  nty(*),nn,i
      real*8   x(ndm,*)

      nty(nn) = 0
      do i = 1,ndm
          x(i,nn) = 0.d0
      enddo

      end
      !}}}
