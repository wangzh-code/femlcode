!$Id:$
      subroutine proj_matc(ix,nen,nen1,numel, ima,numnp, numnm)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose:  Determine number of materials joining at each node

!      Inputs:
!         ix(nen1,*) - Element connection data and material numbers
!         nen        - Number of nodes on element
!         nen1       - Dimension of ix array
!         numel      - Number of elements
!         numnp      - Number of nodal points

!      Outputs:
!         ima(2,*)   - Number materials at each node.
!         numnm      - Number of entries in nodal material numbers.
!-----[--.----+----.----+----.-----------------------------------------]
      implicit   none

      include   'iofile.h'

      integer (kind=4) :: nen,nen1,numel, numnp, numnm
      integer (kind=4) :: ix(nen1,numel)
      integer (kind=4) :: ima(2,numnp)

!     Local variables

      integer (kind=4) :: i,n, ma, nod

      ima(:,:) = 0

      numnm = 0
      do n = 1,numel
        ma = ix(nen1,n)
        if(ma.gt.0) then
          do i = 1,nen
            nod = ix(i,n)
            if(nod.gt.0) then
              if(ima(2,nod).ne.ma) then
               ima(1,nod) = ima(1,nod) + 1
               ima(2,nod) = ma
               numnm      = numnm + 1
              endif
            endif ! nod > 0
          end do ! i
        endif ! ma > 0
      end do ! n

      end subroutine proj_matc
