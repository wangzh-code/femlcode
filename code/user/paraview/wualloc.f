c$Id:$
      logical function wualloc(num,name,length,precis,unit)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
c      Purpose: Write a dictionary entry for restart.
c               Pointer defined for integer (single) and real
c               (double precision arrays).

c      Inputs:
c         num        - Entry number for array (see below)
c         name       - Name of array          (see below)
c         length     - Length of array to save
c         precis     - Precision of array to save: 1 = integer; 2 = real
c         unit       - Logical unit to use for a write

c      Outputs:
c         none       - Information saved using 'unit'


      implicit   none

      include   'iofile.h'
      include   'pointer.h'
      include   'comblk.h'

      character  name*(*),dname*5
      integer    num, unit, length, precis, i

      save

c     Read length and precision

      dname = name
      write(unit) dname, length, precis

      if(precis.eq.1) then
        write(unit) (mr(up(num)+i),i=0,length-1)
      elseif(precis.eq.2) then
        write(unit) (hr(up(num)+i),i=0,length-1)
      else
        write(iow,3000) num,name
        if(ior.lt.0) then
          write(*,3000) num,name
        endif
        call plstop()
      endif

      wualloc = .true.

c     Format

3000  format(' **RESTART ERROR** Incorrect length or precision for',
     &       '                   Array Number',i4,' Named ',a/)

      end function wualloc
