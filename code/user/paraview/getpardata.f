!$Id:$      
      subroutine getpardata(pntaskso,pranko)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
!     Purpose:
      implicit  none
      
      integer pntaskso,pranko

      pntaskso = 1
      pranko   = 0
  
      end subroutine getpardata
