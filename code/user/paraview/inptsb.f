c     subroutine inptsb(d) input material parameters {{{1     
      subroutine inptsb(d)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
c     Purpose: input d(*) parameters
c  
c     REFERENCE LIST - Values not in this list are assumed to be free
c     1  - ELAStic,ISOTropic - elastic modulus
c     2  - ELAStic,ISOTropic - poisson ratio
c     3  - DENSity,MASS
c     4  - THERmal,DIFFusion - specific heat 
c     5  - THERmal,TQ - Taylor Quinney Coefficient
c     6  - PLANE - plane stress thickness
c     7  - THERmal,DIFFusion - conductivity
c     8  - FLOWlaw
c     9  - FLOWlaw
c     10 - FLOWlaw
c     11 - FLOWlaw
c     12 - FLOWlaw
c     13 - FLOWlaw
c     14 - FLOWlaw
c     15 - FLOWlaw
c     16 - shear modulus
c     17 - diffusivity
c     18 - heat generation parameter
c     19 - number of viscoleastic prony series
c     20 - relaxation shear modulus 1
c     21 - relxation time 1 
c     22 - relaxation shear modulus 2
c     23 - relxation time 2
c     24 - relaxation shear modulus 3
c     25 - relxation time 3
c     26 - relaxation shear modulus 4
c     27 - relxation time 4
c     28 - relaxation shear modulus 5
c     29 - relxation time 5
c     30 - relaxation shear modulus 6
c     31 - relxation time 6
c     32 - 
c     33 - lames constant
c     34 - D(1,1), elasticity tensor
c     35 - D(1,2), elasticity tensor
c     36 - D(4,4), elasticity tensor
c     37 - THERmal,EXPAnsion - expansion coeff
c     38 - PLANE,type
c     39 - IMPErfection
c     40 - IMPErfection
c     41 - IMPErfection
c     42 - IMPErfection
c     43 - IMPErfection,TYPE
c     44 - 
c     45 - FLOWlaw,type 
c     46 - NONDimensional - Char. Length
c     47 - NONDimensional - Char. Stress
c     48 - NONDimensional - Char. Time
c     49 - NONDimensional - Char. Temperature
c     50 - dimensionless stress divergence coefficient
c     51 - dimensionless body force coefficient
c     52 - dimensionless diffusion coefficient
c     53 - dimensionless heat source coefficient
c     54 - dimensionless elasticity coeficient
c     55 - dimensionless deformation rate coefficient
c     56 - dimensionless thermal expansion deformation rate coefficient
c     57 - dimensionless pf diffusion coefficient
c     58 - 
c     59 - 
c     60 - THERmal,CONVection - heat transfer coeff
c     61 - THERmal,CONVection - t infinity
c     62 - BBAR
c     63 - BBAR
c     64 - EXPLicit
c     65 - EXPLicit
c     66 - EXPLicit
c     67 - EXPLicit
c     68 - THERMal,SPECificHeat
c     69 - THERMal,SPECificHeat
c     70 - THERMal,SPECificHeat
c     71 - THERMal,SPECificHeat
c     72 - THERMal,SPECificHeat
c     73 - THERMal,SPECificHeat
c     74 - THERMal,SPECificHeat
c     75 - THERMal,SPECificHeat
c     76 - THERMAL,FTQ
c     77 - PFRActure
c     78 - PFRActure
c     79 - PFRActure
c     80 - PFRActure
c     81 - PFRActure
c     82 - PFRActure
c     83 - PFRActure
c     84 - THERmal,EXPAnsion - ref. temperature
c     85 - PFRActure
c     86 - PFRActure,SPLIt,type
c     87 - PFRActure
c     88 - PFRActure
c     89 - PFRActure
c     90 - PFRActure
c     91 - PFRActure
c     92 - PFRActure
c     93 - PFRActure
c     94 - PFRActure
c     95 - PFRActure
c     96 - PFRActure
c     97 - PFRActure
c     98 - FLOWlaw,SMOOth
c     99 - FLOWlaw,SMOOth
c     100- NURBs,NGP
c     101- EXPO,GPVAL
c     102- EXPO,GPVAL
c     103- EXPO,ELMAT
c     104- EXPO,ELMAT
c     105- EXPO,ELMAT
c     106- PFRActure
c     107- SUPG
c     108- SUPG
c     109- SUPG
c     110- THERmal,NCTQ
c     111- THERmal,NCTQ
c     112- THERmal,NCTQ
c     113- THERmal,NCTQ
c     114- THERmal,NCTQ
c     115- THERmal,NCTQ
c     116- THERmal,NCTQ
c     117- ORTHotropic, Theta1 z_g-z_l
c     118- ORTHotropic, Theta2 x_g-x_l
c     119- ORTHotropic, Theta3 y_g-y_l
c     120- FLOWlaw,STRAin,parameters
c     121- FLOWlaw,STRAin,parameters
c     122- FLOWlaw,STRAin,parameters
c     123- FLOWlaw,STRAin,parameters
c     124- FLOWlaw,STRAin,parameters
c     125- FLOWlaw,STRAin,parameters
c     126- FLOWlaw,STRAin,parameters
c     127- FLOWlaw,STRAin,parameters
c     128- FLOWlaw,THERmal,parameters
c     129- FLOWlaw,THERmal,parameters
c     130- FLOWlaw,THERmal,parameters
c     131- FLOWlaw,THERmal,parameters
c     132- FLOWlaw,THERmal,parameters
c     133- FLOWlaw,THERmal,parameters
c     134- FLOWlaw,THERmal,parameters
c     135- FLOWlaw,THERmal,parameters
c     136- FLOWlaw,RATE
c     137- FLOWlaw,STRAin
c     138- FLOWlaw,THERmal
c     139- Flag for volume integration
c     140- Flag for pressure function
c     141- Objective rate options
c     142- REACtive,KINEticLaw,parameters
c     143- REACtive,KINEticLaw,parameters
c     144- REACtive,KINEticLaw,parameters
c     145- REACtive,KINEticLaw,parameters
c     146- REACtive,KINEticLaw,parameters
c     147- REACtive,HEAT,parameters
c     148- REACtive,HEAT,parameters
c     149- Solver options
c     150- Flag for rate independent plasticity
c     151- Flag for quasi static
c     152- Flag for initial crack (strain energy)
c     153- left of initial crack
c     154- right of initial crack
c     155- Parameter alpha for initial crack
c     156- Parameter beta  for initial crack
c     157- ORTHotropic,Poisson's ratio nu12
c     158- ORTHotropic,Poisson's ratio nu13
c     159- ORTHotropic,Poisson's ratio nu23
c     160- PFracture,DEGR flag
c     161- PFracture,DEGR s value 
c     162- Mixed critical fracture energy flag
c     163- Mixed critical fracture shear energy
c     164- Mixed critical fracture tension energy
c     165- Dynamic fracture energy
c     166- ORTHotropic,Young's modulus E1
c     167- ORTHotropic,Young's modulus E2
c     168- ORTHotropic,Young's modulus E3
c     169- 
c     170- Elastic,XXXX,Parameter
c     171- Elastic,ARRUda,Parameter
c     172- 
c     173- Body Force Flag
c     174- Body Force x
c     175- Body Force y
c     176- Body Force z
c     177- Body Force phase field
c     178- Body Force thermal
c     179- Fictious body force  propotional number x
c     180- Fictious body force  propotional number y
c     181- Fictious body force  propotional number z
c     182- Fictious body force  propotional number phase field
c     183- Fictious body force  propotional number thermal
c     184- Fictitious body force x
c     185- Fictitious body force y
c     186- Fictitious body force z
c     187- Fictitious body force phase field
c     188- Fictitious body force thermal 
c     189- 
c     190- Energy threshold for standard phase field method 
c     191- Numerical jacobian matrix flag
c     192- portion of viscoelastic dissipation into heat
c     193- temperature denpendency flag
c     194- C1 of WLF equation
c     195- C2 of WLF equation
c     196- Reference Temperature of WLF
c     197- 
c     198- ORTHotropic,Shear modulus G12
c     199- ORTHotropic,Shear modulus G13
c     200- ORTHotropic,Shear modulus G23
c     201- ANISotropic,alpha
c     202- ANISotropic,beta
c     201- ANISotropic,chi
c     204- ANISotropic,psi
c     205- Orthotropic,Critical energy Gc1
c     206- Orthotropic,Critical energy Gc2
c     207- Orthotropic,Critical energy Gc3
c     208- Residual strength, eta
c     209- Cohesive geometrical parameter, ksi 
c     210- Cohesive degradation parameter, ft - strength
c     211- Cohesive degradation parameter, p  - polynominal order
c     212- Cohesive softening law
c          0 linear; 1 exponetial; 2 hyperbolic; 3 cornelissen's law
c     213- Viscosity regularization parameter in miehe's paper, eta
c     214- Decoupled system flag. 0 Coupling; 1. Decoupling
c     215- Decouping iteration threshold  
c     216- Cohesive Phase field,Parameter p from Sandia Report 
c     217- ORTHotropic,Criticla Shear Energy Gc12 
c     218- ORTHotropic,Criticla Shear Energy Gc23 
c     219- ORTHotropic,Criticla Shear Energy Gc13 
c     220- COHFracture,Energy,Gn 
c     221- COHFracture,Energy,Gt 
c     222- COHFracture,Strength,Tn_m 
c     223- COHFracture,Strength,Tt_m
c     224- COHFracture,Shape,alpha
c     225- COHFracture,Shape,beta
c     226- COHFracture,Initial slope indicator,ln 
c     227- COHFracture,Initial slope indicator,lt 
c     228- COHFracture,Thickness,th
c     229- COHFracture,DELTa,delta_n 
c     230- COHFracture,DELTa,delta_t 
c     231- COHFracture,GAUSs,n_GP 
c     232- 
c     233- 
c     234- 
c     235- 
c     236- 
c     237- 
c     238- 
c     239- 
c     240- 
c     241- 
c     242- 
c     243- 
c     244- 
c     245- 
c     246- 
c     247- 
c     248- 
c     249- 
c     250- 
c     251- 
c     252- 
c     253- 
c     254- 
c     255- 
c     256- 
c     257- 
c     258- 
c     259- 
c     260- 
c     261- 
c     262- 
c     263- 
c     264- 
c     265- 
c     266- 
c     267- 
c     268- 
c     269- 
c     270- 
c     271- 
c     272- 
c     273- 
c     274- 
c     275- 
c     276- 
c     277- 
c     278- 
c     279- 
c     280- 
c     281- 
c     282- 
c     283- 
c     284- 
c     285- 
c     286- 
c     287- 
c     288- 
c     289- 
c     290- 
c     291- 
c     292- 
c     293- 
c     294- 
c     295- 
c     296- 
c     297- 
c     298- 
c     299- 
c     300- 

c     Material Properties:
c     Elastic properties{{{2    
c
c     ELAStic,Model,elastic modulus,Poisson's ratio,params
c            d(172),     d(1)      ,     d(2)
c
c
c     Model can be ISOTropic, NEOHookean, or ARRUdaboyce
c
c     ELAStic,ISOTropic,elastic modulus,Poisson's ratio
c              d(172)  ,     d(1)      ,     d(2)
c
c     ELAStic,NEOHookean,elastic modulus,Poisson's ratio
c               d(172)  ,     d(1)      ,     d(2)
c
c     ELAStic,ARRudaboyce,elastic modulus,Poisson's ratio,# of chains,# of terms
c               d(172)   ,     d(1)      ,     d(2)      ,   d(170)  ,  d(171)
c
c     DENSity,MASS,density
c                   d(3)
c
c     Parameters computed by inptsb
c         d(16) = shear modulus
c         d(33) = lames constant
c         d(34) = D(1,1), elasticity tensor
c         d(35) = D(1,2), elasticity tensor
c         d(36) = D(4,4), elasticity tensor
c     2}}}
c     Analysis type {{{2
c
c     PLANE,type,plane stress thickness
c          d(38),        d(6)
c
c     Plane analysis types
c
c          type = STRESS -> d(38) = 1.0
c          type = STRAIN -> d(38) = 2.0
c          type = AXIS   -> d(38) = 3.0
c     2}}}
c     Thermal properties{{{2
c     THERmal,DIFFusion,conductivity,specific heat
c                           d(7)    ,    d(4)
c
c     THERmal,EXPAnsion,expansion coeff,ref. temperature
c                            d(37)     ,      d(84)
c
c     THERmal,CONVection,heat transfer coeff,t infinity
c                                d(60)      ,  d(61)
c
c     THERmal,TQ,taylor quinney coeff
c                        d(5)
c
c     THERMal,SPECificHeat,type,params (specify non constant specific heat)
c                         d(68),d(69-75)
c  
c     THERMAL,FTQ Flag to force use of constant TQ coefficient in elmt21  
c             d(76)
c
c     Non-constant Taylor-Quinney Coefficient (elm49 and 21)
c     THERmal,NCTQ,type,parameters
c                 d(110),d(111-116)
c
c     For elements with tq calculated from specified stored energy function (21)
c     Linear stored energy function:
c         type   = 1
c         d(111) = Slope of Stored energy function
c     Constant TQ
c         type   = 2
c     Longere Dragon method
c         type   = 3
c     
c
c     For elements with tq = f(g) (49)
c     Vural: 
c         d(110)    = 1.0,  Type and also flag for TQ
c         d(111)    = eA,   Critical strain rate
c         d(112)    = SA,   Range of transition factor
c         d(113-116)= 0.00, Empty
c
c     Parameters computed by inptsb
c         d(17) = diffusivity
c         d(18) = heat generation parameter
c     2}}}
c     Flow law{{{2
c     FLOWlaw,type,parameters
c            d(45), d(8-15)
c
c     Flow Law types{{{3
c     Modified litonski{{{4
c         type  = ML -> d(45) = 1.0
c         d(8)  = gr, reference strain rate
c         d(9)  = m1, rate sensitivity
c         d(10) = s0, yield stress
c         d(11) = g0, yield strain
c         d(12) = n1, strain hardening exponent
c         d(13) = t0, refernce temperature
c         d(14) = de, thermal softening parameter
c         d(15) = k1, thermal softening parameter
c     4}}}
c     Johnson Cook{{{4
c         type  = JC -> d(45) = 2.0
c         d(8)  = gr, reference strain rate
c         d(9)  = cc, rate sensitivity
c         d(10) = ja, yield stress
c         d(11) = jb, hardening parameter
c         d(12) = n1, strain hardening exponent
c         d(13) = t0, refernce temperature
c         d(14) = tm, melt temperature
c         d(15) = jm, temperature exponent
c     4}}}
c     Litonski{{{4
c         type  = LT -> d(45) = 3.0
c         d(8)  = gr, reference strain rate
c         d(9)  = m1, rate sensitivity
c         d(10) = s0, yield stress
c         d(11) = g0, yield strain
c         d(12) = n1, strain hardening exponent
c         d(13) = t0, refernce temperature
c         d(14) = de, thermal softening parameter
c         d(15) = empty
c     4}}}
c     Longere Dragon{{{4
c         type  = LY -> d(45) = 4.0
c         d(8)  = ri, R infinity, saturation stress
c         d(9)  = kp, strain hardenin parameter
c         d(10) = ap, thermal softening parameter
c         d(11) = r0, yield stress
c         d(12) = yr, viscosity parameter
c         d(13) = m1, rate hardening parameter
c         d(14) = empty
c         d(15) = empty
c     4}}}
c     Areis Belytsko{{{4
c         type  = AB -> d(45) = 5.0
c         d(8)  = gr, reference strain rate
c         d(9)  = m1, rate sensitivity
c         d(10) = s0, yield stress
c         d(11) = g0, yield strain
c         d(12) = n1, strain hardening exponent
c         d(13) = t0, refernce temperature
c         d(14) = de, thermal softening parameter
c         d(15) = k1, thermal softening parameter
c     4}}}
c     3}}}
c     For applying flowlaw smoothing
c     FLOWlaw,SMOOth,parameter
c              d(98), d(99)
c     d(98) =   , Flag to indicate flow law smoothing option
c     d(99) =   , smoothing function parameter
c
c     Flow laws of the form g = f(x) where x = s / (q(p) * r(T)){{{3
c
c     FLOWlaw,FUNC,type,parameters
c            d(45),d(136),d(8-15)        for rate function
c                 ,d(137),d(120-127)     for strain function
c                 ,d(138),d(128-135)     for thermal function
c
c         d(45) = 10.0 for FUNC = any of RATE, STRAin, or THERmal
c
c         FUNC  = RATE , specifies rate function f
c
c         type  = 1.0, power law hardening, f(x) =  gr*x^m1
c         
c         d(8)  = gr, reference strain rate
c         d(9)  = m1, rate sensitivity
c
c         FUNC  = STRAin, specifies strain function q
c
c         type  = 1.0 power law hardening, q(p) = s0 * (1 + p/p0) ^n1
c
c         d(120) = s0, yield stress
c         d(121) = p0, yield strain
c         d(122) = n1, strain hardening exponent
c
c         FUNC   = THERmal, specifies thermal function
c
c         type   = 1.0 linear softening, r(T) = 1.d0-a*(T-T0)
c         d(128) = t0, refernce temperature
c         d(129) = de, thermal softening parameter
c     3}}}
c     2}}}
c     Nondimensionalization{{{2
c
c     NONDimensional,char. length,char. stress,char. time, char. temp
c                       d(46)    ,    d(47)   ,   d(48)  ,    d(49)
c
c     parameters computed by inptsb
c         d(50) = dimensionless stress divergence coefficient
c         d(51) = dimensionless body force coefficient
c         d(52) = dimensionless diffusion coefficient
c         d(53) = dimensionless heat source coefficient
c         d(54) = dimensionless elasticity coeficient
c         d(55) = dimensionless deformation rate coefficient
c         d(56) = dimensionless thermal expansion deformation rate coefficient
c         d(57) = dimensionlesss pf diffusion coefficient
c     2}}}
c     Phase Field properties{{{2
c
c     PFRActure,GC,fracture toughness,process zone param.
c                          d(77)     ,       d(78)
c
c     PFRActure,MICRoinertia,micro inertia param
c                                  d(79)
c
c     PFRActure,RESIdual,residual strength in fractured phase
c                                     d(80)
c
c     PFRActure,SPLIt,type
c                     d(86)
c
c     Free energy split types
c         type = 0.0 -> volumetric - deviatoric split +ve vol. part damaged
c         type = 1.0 -> principal strain split +ve vol. and +ve pr. strain damaged
c         type = 2.0 -> volumetric - deviatoric split +ve vol. and dev. part damaged
c
c     PFRActure,IRREversibility (enforced by history function)
c                    d(81)
c
c     PFRActure,PRECrack, rectangle crack, crack left, crackright, crack wid, crackcentery, crackamp.
c                 d(152) ,    d(153-157)
c
c     PFRActure,THREshold,maximum value of c
c                               d(85)
c
c     PFRActure,DISSipation,params     (phase field dissipation function)
c                  d(87)   ,d(88-90)
c
c     PFRActure,INELastic,params       (inelastic contribution to fracture)
c                        ,d(91-97)
c
c     PFRActure,KDEG     (conductivity is degraded by damage)
c              ,d(106)
c
c     2}}}
c     Material prop imperfection{{{2
c
c     IMPErfection,TYPE
c                 d(43), d(39) ,d(40) ,    d(41)    ,  d(42)
c                 ,SECH,percent,radius,   x coord   , y coord
c                 ,LSCH,percent,radius, x(1) or y(2), coord
c
c     Types
c         TYPE = SECH -> d(43) = 0.d0  Radial Hyperbolic secant
c         TYPE = LSCH -> d(43) = 1.d0  Linear Hyperbolic secant
c     2}}}
c     BBar/FBar{{{2
c
c     BBAR,ON,type    (activate bbar or fbar)
c         d(62),d(63)
c
c         type = 0.0  element averaged J
c         type = 1.0  J at element center
c
c     FBAR,ON is equivalent
c     2}}}
c     For explicit calculation{{{2
c
c     EXPLicit,CFL,cfl number
c                    d(64)
c
c     parameters calculated by inptsb
c         d(65) = bulk modulus
c         d(66) = P-wave modulus
c         d(67) = P-wave speed
c     2}}}
c     For outputting gauss point quantities{{{2
c
c     EXPO,GPVAL,numFields,maxGP
c                 d(101)  ,d(102)
c
c         d(101), number of fields being exported from the GP
c         d(102), maximum number of GP (gauss-points) in the element
c
c     EXPO,ELMAT,el,step1,stepFinal
c            d(103),d(104),d(105)
c
c         d(103), element to export (-1 exports all)
c         d(104), first step to export
c         d(105), last step to export
c      2}}}
c     For NURBS specific input{{{2
c  
c     NURBs,NGP,ngp
c              d(100)
c
c     d(100)=ngp, number of gauss points for the simulation
c     2}}}
c     SUPG{{{2
c
c     SUPG,ON,SUPG constant,L_c for gr. plas.
c        d(107),  d(108)   ,     d(109)
c
c     d(107) =  , flag for SUPG and Regularization technique (elmt12)
c     d(108) =  , SUPG constant
c     d(109) =  , l_c characteristic length for gradient plasticity
c     2}}}
c     SOLVer stuff{{{2
c
c     SOLVer,TYPE,options 
c                ,d(149)
c
c     
c     2}}}
c     Volume and pressure options{{{2
c
c     PRES,VFUN,type 
c             ,d(139)
c
c     d(139) =  , Flag for form of volumetric free energy function U(J)
c
c     PRES,INT,type 
c             ,d(140)
c
c     d(140) =  , Flag for integration method to increment volume 
c     2}}}
c     Objective rate option{{{2
c
c     OBJE,RATE,type 
c              ,d(141)
c
c     d(141) =  , Flag for form of objective rate
c              0 = Jaumman rate
c              1 = Total Lie derivative
c              2 = Elastic Lie derivative
c
c     2}}}
c     Reactive material options{{{2
c
c     Specify parameters for the generic kinetic law:
c
c     d eta/ d t = K*eta^-p * exp(-m*eta)*exp(-E/(RT))
c
c     where K is a constant, p and m are parameters related to the order
c     of the reaction, E is the activation energy, R is the gas or bolzman
c     constant (depending on the units of E), and T is the temperature
c
c     REACtive,KINEticLaw,   K  ,   p  ,   m  ,   E  ,   R 
c                        ,d(142),d(143),d(144),d(145),d(146)
c
c     specify the reaction enthalpy h in joules per mol, and the moles of product 
c     per volume @ eta = 1, such that the heating due to the reaction is 
c     h * pmax * d eta/ d t
c
c     REACtive,HEAT,   h  , pmax
c                  ,d(147),d(148)
c
c
c     2}}}


c     Crystal Plasticity (Clayton's model)

c     type  = AB -> d(45) = 9.0
c     d(120) = sr, initial slip rate
c     d(121) = ms, strain rate sensitivity parameter
c     d(122) = qh, latent hardening ratio
c     d(123) = Aa, constant#1 related to hardening
c     d(124) = Bb, constant#2 related to hardening
c     d(125) = gy, initial yield stress
c     d(126) = ts, thermal softening coefficient
c     d(127) = ic, interaction coefficient for slip systems
c     d(128) = bv, Burgers vector
c     d(129) = ka, kappa (dimensionless material dependent scalar parameter
c                      independent of strain rate and temperature)
c     d(130) = ns, number of slip system
c     d(131) = t1, refernce temperature

ccccccccc     d(4)  = cp, specific heat
cccccccccc    d(7)  = kp, thermal conductivity
ccccccccc     d(3)  = , mass density
ccccccccc     d(16) =   , shear modulus
cccccccccc    d(33) =   , lames constant
cccccccccc    d(37) = al    , ceofficient of thermal expansion


c-------------------------------------------------------------------

      implicit none

      logical pcomp, errck, tinput,ualloc
      character*15 text(2)
      integer j
      real*8 td(16),d(*)

      include 'iofile.h'
      include 'pointer.h'
      include 'comblk.h'
      include 'cdata.h'
      include 'cdat1.h'
      !include 'sdata.h'

      save
      
      !zero d(*)
      call pzero(d,ndd)
      !Set default parameters if not zero{{{2
      !plane thickness{{{3
      d(6) = 1.d0
      !3}}}
      !volume-pressure function{{{3
      d(139) = 3.d0
      !3}}}
      !Nondimensionalization{{{3
      d(46:49) = 1.d0 !dimensionalization parameters default is 1, i.e.
      !no nondimennsioalization 
      !3}}}
      !Material prop imperfection {{{3
      !Default is no imperfection, i.e. imperfection % = 1
      d(39)    = 1.d0 !imperfection 
      d(40)    = 1.d0 !imperfection radius
      !3}}}
      !write(*,*) nen1

      !2}}}
      !read from input file{{{1
      text(1) = 'start'
      do while (.not.pcomp(text(1),'    ',4))

         errck = tinput(text,2,td,14)
         
         !Elastic properties {{{2
         !ELAStic,MODEL,elastic modulus,Poisson's ratio,params
         !                   d(1)      ,     d(2)
         if (pcomp(text(1),'ELAS',4)) then
             d(1:2) = td(1:2)
             if (pcomp(text(2),'NEOH',4))then
                 d(170) = 1.d0
             elseif (pcomp(text(2),'ARRU',4))then
                 d(170) = 2.d0
                 d(171:172) = td(3:4)
             elseif (pcomp(text(2),'NEO2',4))then
                 d(170) = 3.d0
             elseif (pcomp(text(2),'LOGA',4))then
                 d(170) = 4.d0
             endif
         !Body force/volume in 1-2-3 direction
         elseif(pcomp(text(1),'BODY',4)) then
             d(173) = td(1)
             d(174:178) = td(2:6) 
         !Fictitious body force and loading number
         elseif(pcomp(text(1),'GROU',4)) then
             d(179:188) = td(1:10)
         !Initial crack parameters
         elseif(pcomp(text(1),'INIT',4)) then
             d(152) = td(1)
             d(153:157) = td(2:6) 
         elseif(pcomp(text(1),'PSVS',4)) then ! Prony Series type of solid
             d(19)  = td(1)          ! nvis
            if(int(d(19)).eq.1) then
              d(20) = td(2)  !mu_i
              d(21) = td(3)
            elseif(int(d(19)).gt.1 .and. int(d(19)).le.3) then
              do j = 1,int(d(19))
                 d(19+2*j-1) = td(2*j)  !mu_i
                 d(19+2*j)   = td(2*j+1)    !tau_i
              enddo
            elseif(int(d(19)).gt.3 .and. int(d(19)).le.20) then
              do j = 1,int(d(19))
                 d(19+2*j-1) = td(2*j)  
                 d(19+2*j)   = td(2*j+1)    
              enddo 
            endif
         elseif (pcomp(text(1),'VEIF',4))then
             d(32)  = td(1)
             d(192) = td(2)
         !DENSity,MASS,density
         !           d(3)
         elseif (pcomp(text(1),'DENS',4))then
             d(3) = td(1)
         !2}}}
         !Analysis type{{{2
         elseif(pcomp(text(1),'PLAN',4))then
             if (pcomp(text(2),'STRE',4))then
                 d(38) = 1.d0
                 d(6)  = td(1)
             elseif (pcomp(text(2),'STRA',4))then
                 d(38) = 2.d0
             elseif (pcomp(text(2),'AXIS',4))then
                 d(38) = 3.d0
             endif
         !2}}}
         !Thermal properties{{{2
         elseif (pcomp(text(1),'THER',4)) then
             !THERmal,DIFFusion,conductivity,specific heat
             !                  d(7)    ,    d(4)
             if (pcomp(text(2),'DIFF',4)) then
                 d(7) = td(1)
                 d(4) = td(2)

             !THERmal,EXPAnsion,expansion coeff,ref. temperature
             !                   d(37)     ,      d(84)
             elseif (pcomp(text(2),'EXPA',4)) then
                 d(37) = td(1)
                 d(84) = td(2)

             !THERmal,CONVection,heat transfer coeff,t infinity
             !                       d(60)      ,  d(61)
             elseif (pcomp(text(2),'CONV',4)) then
                 d(60:61) = td(1:2)

             !THERmal,TQ,taylor quinney coeff
             !               d(5)
             elseif (pcomp(text(2),'TQ',2))then
                 d(5) = td(1)

             !THERMal,SPECificHeat,type,params (specify non constant specific heat)
             !                d(68),d(69-75)
             elseif (pcomp(text(2),'SPEC',4)) then
                 d(68:75) = td(1:8)
  
             !THERMAL,FTQ Flag to force use of constant TQ coefficient in elmt21  
             !    d(76)
             elseif (pcomp(text(2),'FTQ',3)) then
                 d(76) = 1.d0

             !THERMAL,NCTQ non constant taylor quinney  
             !    d(76)
             elseif (pcomp(text(2),'NCTQ',4)) then
                 d(110:116) = td(1:7)
             ! Williams-Landel-Ferry equation(WLF)
             ! C1, C2, Tr
             ! C1 =17.4, C2=51.6 Tr = Tg
             ! C1 =8.86, C2 = 101.6, Tr = Tg+43K/50K
             ! C1 =6.5,  C2 = 120,   Tr = 292.15K, room temperature.
             elseif (pcomp(text(2),'WLF',3)) then
                 d(193) = 0.d0
                 d(194:196) = td(1:3)
             elseif (pcomp(text(2),'ARRH',3)) then
                 d(193) = 1.d0
             endif
         !2}}}
          !Flow law or yield surface{{{2
          elseif(pcomp(text(1),'FLOW',4).or.
     &           pcomp(text(1),'YIEL',4)) then
              if (pcomp(text(1),'YIEL',4))then
                  d(150) = 1.d0
              else
                  d(150) = 0.d0
              endif
              !Modified litonski
              if (pcomp(text(2),'ML',2)) then
                  d(8:15) = td(1:8)
                  d(45) = 1.d0
              !Johnson cook
              elseif (pcomp(text(2),'JC',2)) then
                  d(8:15) = td(1:8)
                  d(45) = 2.d0
              !Litonski
              elseif (pcomp(text(2),'LT',2)) then
                  d(8:15) = td(1:8)
                  d(45) = 3.d0
              !Longere Dragon
              elseif (pcomp(text(2),'LY',2)) then
                  d(8:15) = td(1:8)
                  d(45) = 4.d0
              !Areias Belytschko
              elseif (pcomp(text(2),'AB',2)) then
                  d(8:15) = td(1:8)
                  d(45) = 5.d0
              !Modified Litonski - Areias Belytschko composite
              elseif (pcomp(text(2),'LA',2)) then
                  d(8:15) = td(1:8)
                  d(45) = 8.d0
              !Crystal Plasticity
              elseif (pcomp(text(2),'CP',2)) then
                  d(120:131) = td(1:12)
                  d(45) = 9.d0
              !Perfect plasticity
              elseif (pcomp(text(2),'PP',2)) then
                  d(10) = td(1)
                  d(45) = 11.d0
              elseif (pcomp(text(2),'SL',2)) then
                  d(10:13) = td(1:4)
                  d(45) = 12.d0
              !FLOWlaw,SMOOth,parameter, activate flow law smoothing
                       !d(98), d(99)
              elseif (pcomp(text(2),'SMOO',4))then
                  d(98) = 1.d0
                  d(99) = td(1)
                  write(iow,*) 'Plastic flow smoothing activated'
              elseif (pcomp(text(2),'RATE',4)) then
                  d(45)   = 10.d0
                  d(136)  = td(1)
                  d(8:15) = td(2:9)
              elseif (pcomp(text(2),'STRA',4)) then
                  d(45)   = 10.d0
                  d(137)  = td(1)
                  d(120:127) = td(2:9)
              elseif (pcomp(text(2),'THER',4)) then
                  d(45)   = 10.d0
                  d(138)  = td(1)
                  d(128:135) = td(2:9)
              endif
c     2}}}
          !Nondimensionalization{{{2
 
          !NONDimensional,TYPE,char. length,char. stress,char. time, char. temp
          !                      d(46)    ,    d(47)   ,   d(48)  ,    d(49)
          elseif (pcomp(text(1),'NOND',4)) then
              d(46:49) = td(1:4)
          !2}}}
         !Phase Field properties{{{2
          elseif (pcomp(text(1),'PFRA',4))then

             !PFRActure,GC,fracture toughness,process zone param.
             !                     d(77)     ,       d(78)
             if (pcomp(text(2),'GC',2))then
                 d(77:78) = td(1:2)

             !PFRActure,MICRoinertia,micro inertia param
             !                             d(79)
             elseif(pcomp(text(2),'MICR',4))then
                 d(79) = td(1)

             !PFRActure,RESIdual,residual strength in fractured phase
             !                                d(80)
             elseif(pcomp(text(2),'RESI',4))then
                 d(80) = td(1)

             !PFRActure,SPLIt,type
             !                d(86)
             elseif(pcomp(text(2),'SPLI',4))then
                 d(86) = td(1)

             !PFRActure,IRREversibility (enforced by history function)
             !               d(81)
             elseif(pcomp(text(2),'IRRE',4))then
                 d(81) = 1.d0

             !PFRActure,PRECrack,crack center
             !            d(82) ,    d(83)
             elseif(pcomp(text(2),'PREC',4))then
                 d(82:83) = td(1:2)

             !PFRActure,THREshold,maximum value of c
             !                          d(85)
             elseif(pcomp(text(2),'THRE',4))then
                 d(85) = td(1)

             !PFRActure,DISSipation,params     (phase field dissipation function)
             !             d(87)   ,d(88-90)
             elseif(pcomp(text(2),'DISS',4))then
                 d(87:90) = td(1:4)

             !PFRActure,INELastic,params       (inelastic contribution to fracture)
             !            d(91)  ,d(92-97)
             elseif(pcomp(text(2),'INEL',4))then
                 d(91:97) = td(1:7)

             !PFRActure,KDEG     (conductivity is degraded by damage)
             !         ,d(106)
             elseif(pcomp(text(2),'KDEG',4))then
                 d(106) = td(1)

             !PFRActure,SOLVer,d(149)  
             !         ,      
             elseif(pcomp(text(2),'SOLV',4))then
                 d(149) = td(1)

             !PFRActure,DEGR,d(160:161)  (degradation function m(c) )  
             !         ,      
             elseif(pcomp(text(2),'DEGR',4))then
                 d(160:161) = td(1:2)
             !PFRActure,DEGR,d(162:164)  (mixed fracture energy )  
             !         ,
             elseif(pcomp(text(2),'MIXE',4))then
                 d(162:164) = td(1:3)
             
             !PFRActure,Dyna,d(165)  (dyanmic fracture energy)  
             !         ,
             elseif(pcomp(text(2),'DYNA',4))then
                 d(165) = 1.d0
             !PFRActure,COHE,d(209) (new crack surface function) d(211) degradation order
              ! d(209) ksi, d(211) p, d(216) p1
             elseif(pcomp(text(2),'COHE',4))then
                 d(209) = td(1)
                 d(211) = td(2)
                 d(216) = td(3)
             !PFRActure,COHFt, d(210) (cohesive strength)
              !         ,
             elseif(pcomp(text(2),'COHF',4))then
                 d(210) = td(1)
             !PFRActure,SOFT,d(212) softening law
             ! 0 linear softening; 1 exponential softening; 2 co
             ! softening
             elseif(pcomp(text(2),'SOFT',4))then
                 d(212) = td(1)
             ! PFRActure,ENER,d(190) Energy threshold for the formation
             ! of phase field. If it<=0.0 then compute the threshold
             ! based on 1D approximation at the bottom of this
             ! subroutine.
             elseif(pcomp(text(2),'ENER',4))then
                 if(td(1).le.0.d0) then
                    d(190)=-1.d0
                 else
                    d(190)=td(1)
                 endif
             endif
             
         !Cohesive elements properties{{{2
          elseif (pcomp(text(1),'COHF',4))then

             !COHFracture,Energy in the normal/shear directions
             ! Gn, Gt
             if(pcomp(text(2),'ENER',4))then
                 d(220:221) = td(1:2)

             !COHFracture,Strength, Critical strength.
             ! Tn_m, Tt_m
             elseif (pcomp(text(2),'STRE',4))then
                 d(222:223) = td(1:2)

             !COHFracture,SHAPe, shape factors for cohesive damage law
             ! Alpha, Beta
             elseif(pcomp(text(2),'SHAP',4))then
                 d(224:225) = td(1:2)

             !COHFracture,Initial slope indicators in the normal/shear directions
             ! ln, lt
             elseif(pcomp(text(2),'INIT',4))then
                 d(226:227) = td(1:2)

             !COHFracture,Thickness, Out of plane-direction
             ! th
             elseif(pcomp(text(2),'THIC',4))then
                 d(228) = td(1)

             !COHFracture,DELTa, critical separation
             ! th
             elseif(pcomp(text(2),'DELT',4))then
                 d(229:230) = td(1:2)
             endif
             !2}}}
          !Material prop imperfection{{{2
          elseif (pcomp(text(1),'IMPE',4))then
              !IMPErfection,TYPE,params
              !           d(43), d(39:42)
              !Hyperbolic secant imperfection
              if (pcomp(text(2),'SECH',4))then
                  d(43) = 0.d0
                  d(39:42) = td(1:4)
              !Linear (non-radial) hyperbolic secant imperfection
              elseif (pcomp(text(2),'LSCH',4))then
                  d(43) = 1.d0
                  d(39:42) = td(1:4)
              elseif (pcomp(text(2),'LORE',4))then
                  d(43) = 2.d0
                  d(39:42) = td(1:4)
              elseif (pcomp(text(2),'BONE',4))then
                  d(43) = 3.d0
                  d(39:42) = td(1:4)
              elseif (pcomp(text(2),'COMP',4)) then
                  d(43) = 4.d0
                  d(39:42) = td(1:4)
              elseif (pcomp(text(2),'NOIM',4))then !no imperfection
                  d(43) = 8.d0
              endif
          !2}}}
          !BBar/FBar{{{2

          !BBAR,ON,type    (activate bbar or fbar)
          !    d(62),d(63)
         elseif(pcomp(text(1),'BBAR',4).or.pcomp(text(1),'FBAR',4))then
            d(62) = 1.d0
            d(63) = td(1)
          !2}}}
          !For explicit calculation{{{2

          !EXPLicit,CFL,cfl number
          !               d(64)
         elseif (pcomp(text(1),'cfl',3)) then
            d(64) = td(1)

          !2}}}
          !For outputting gauss point quantities{{{2
          elseif(pcomp(text(1),'EXPO',4))then
          

              !EXPO,GPVAL,numFields,maxGP
              !            d(101)  ,d(102)
              if (pcomp(text(2),'GPVA',4))then
                  d(101:102) = td(1:2)
                  !Allocate GPVAL to store values at GP level
                  errck = ualloc(101,'GPVAL',numel*int(d(101)*d(102)),2)
                  errck = ualloc(102,'GPVnu',2,1)
                  mr(up(102))=int(d(101))
                  mr(up(102)+1)=int(d(102))
                  !Stored as hr(up(101)+ngp*numfields*(el-1)+numfields*(gp-1)+i-1)

              !EXPO,ELMAT,el,step1,stepFinal
              !       d(103),d(104),d(105)
              elseif(pcomp(text(2),'ELMA',4))then
                  d(103:105) = td(1:3)!el,stepi,stepj
                  if (d(105).eq.0) then
                     d(105)=d(104)
                  endif
              endif

          ! 2}}}
          !For NURBS specific input{{{2
          elseif (pcomp(text(1),'NURB',4))then

              !NURBs,NGP,ngp
              !         d(100)
              if (pcomp(text(2),'NGP',3))then
                  d(100) = td(1)
              endif
          !2}}}
         !SUPG{{{2
         elseif (pcomp(text(1),'SUPG',4))then
             !SUPG,ON,SUPG constant,L_c for gr. plas.
             !   d(107),  d(108)   ,     d(109)
             if (pcomp(text(2),'ON',2))then
                 d(107) = 1.d0
                 d(108:109) = td(1:2)
             endif
         !2}}}
         !SOLVer stuff{{{2
         elseif (pcomp(text(1),'SOLV',4))then
             d(149) = td(1)
         !2}}}
         !Pressure and volume functions{{{2
         elseif (pcomp(text(1),'PRES',4))then
             if (pcomp(text(2),'VFUN',4))then
                 d(139) = td(1)
             elseif (pcomp(text(2),'INT',4))then
                 d(140) = td(1)
             endif
         !2}}}
         !Objective rate options{{{2
         elseif (pcomp(text(1),'OBJE',4))then
             d(141) = td(1)
         !2}}}
         !Quasi static flag{{{2
         elseif (pcomp(text(1),'QUAS',4))then
             d(151) = 1.d0
         !2}}}
         !Reactive material options{{{2
         elseif (pcomp(text(1),'REAC',4))then
             if (pcomp(text(2),'KINE',4))then
                 d(142:146) = td(1:5)
             elseif (pcomp(text(2),'HEAT',4))then
                 d(147:148) = td(1:2)
             endif
         ! Bone Material assignment
         elseif (pcomp(text(1),'BONE',4))then
            d(200) = 1.d0
            if (pcomp(text(2),'VAGC',4)) then
               d(199) = td(1)
            elseif (pcomp(text(2),'VADE',4)) then
               d(198) = 1.d0
            endif
         !2}}}
         elseif (pcomp(text(1),'NUME',4))then
            d(191) = 1.d0
         ! input othotropic material parameters
         elseif (pcomp(text(1),'ORTH',4))then
            ! Modulus paramters E1,E2,E3,G12,G13,G23
            if (pcomp(text(2),'MODU',4)) then
               d(166:168) = td(1:3)
               d(198:200) = td(4:6)
            ! Poisson's ratio
            elseif (pcomp(text(2),'POIS',4)) then
               d(157:159) = td(1:3)
            elseif (pcomp(text(2),'ANGL',4)) then
               d(117:119) = td(1:3)
            elseif (pcomp(text(2),'ENER',4)) then
               d(205:207) = td(1:3)
               d(217:219) = td(4:6)
            elseif (pcomp(text(2),'RESI',4)) then
               d(208)     = td(1)
            endif
         elseif (pcomp(text(1),'ANIS',4))then
            ! Anisotropic parameters for four different energy componenets.
            d(201:204) = td(1:4)
         elseif (pcomp(text(1),'VISC',4))then
            ! Viscous regularization parameters.
            d(213) = td(1)
         elseif (pcomp(text(1),'DCOU',4))then
            ! Decouping settings
            d(214:215) = td(1:2)
         endif
         
      end do
      !1}}}
      !Compute other parameters{{{2
      !elastic constants{{{3
c     shear modulus
      d(16) = d(1)/(2.d0*(1.d0+d(2)))
c     lames constant
      d(33) = d(1)*d(2)/((1.d0+d(2))*(1.d0-2.d0*d(2)))
      !scale elasicity constants
      d(33) = d(33)/d(47)
      d(16) = d(16)/d(47)
      d(1)  = d(1)/d(47)

c     Elasticity tensor components:{{{4
      if (d(38).eq.1.0) then
c     D(1,1)
         d(34) = d(54)*4.d0*d(16)*(d(33)+d(16))/(d(33)
     &        +2.d0*d(16))
c     D(1,2)
         d(35) = d(54)*2.d0*d(33)*d(16)/(d(33)+2.d0*d(16))
c     D(4,4)
         d(36) = d(54)*d(16)

         if(ior.lt.0) then
            write(*,3010)
         end if
         write(iow,3010)
      else if(d(38).eq.2.0) then
c     D(1,1)
         d(34) = d(33) + 2.d0*d(16)
c     D(1,2)
         d(35) = d(33)
c     D(4,4)
         d(36) = d(16)

         if(ior.lt.0) then
            write(*,3020)
         end if
         write(iow,3020)
      else if(d(38).eq.3.0) then
c     D(1,1)
         d(34) = d(33) + 2.d0*d(16)
c     D(1,2)
         d(35) = d(33)
c     D(4,4)
         d(36) = d(16)
         
         if(ior.lt.0) then
            write(*,3030)
         end if
         write(iow,3030)
      end if
      !4}}}
      !3}}}
      !Thermal constants{{{3
c     diffusivity             
      d(17) = d(7)/(d(3)*d(4))
c     heat generation const
      d(18) = d(5)/(d(3)*d(4))
      !3}}}
c     Compute dimensionless coefficients{{{3
      if (d(3).eq.0.d0 .or. d(151).eq.1.d0)then
          d(50) = d(47)/d(46)
      else
          d(50) = d(48)*d(48)*d(47)/(d(3)*d(46)*d(46))
      endif
      d(51) = d(48)*d(48)/(d(3)*d(46))
      d(52) = d(7)*d(48)/(d(3)*d(4)*d(46)*d(46)) 
      d(53) = d(48)*d(47)/(d(3)*d(4)*d(49))
!      d(53) = d(5)*d(48)*d(47)/(d(3)*d(4)*d(49))
      d(54) = d(48)/d(47)
      d(55) = 1.d0/d(48)
      d(56) = d(37)*d(49)/d(48)
      d(84) = d(84)/d(49)

      !scale pf constants
      d(57) = 4.d0*d(78)**2/(d(46)**2)
      d(77) = d(77)/d(47)
      !Non dimensionalize flow law constants{{{4
      if (d(45) .eq. 1.d0) then !Modified Litonski
          d(10) = d(10)/d(47)
          d(13) = d(13)/d(49)
          d(15) = d(15)/d(49)
      elseif (d(45) .eq. 2.d0)then !JC
          d(10) = d(10)/d(47)
          d(11) = d(11)/d(47)
          d(13) = d(13)/d(49)
          d(14) = d(14)/d(49)
      elseif (d(45) .eq. 3.d0)then !Litonski
          d(10) = d(10)/d(47)
          d(13) = d(13)/d(49)
          d(14) = d(14)*d(49)
      elseif (d(45) .eq. 4.d0)then !Longere Dragon
          d(8)  = d(8)/d(47)
          d(10) = d(10)*d(49)
          d(11) = d(11)/d(47)
          d(12) = d(12)/d(47)
      elseif (d(45) .eq. 9.d0)then !Crystal Plasticity
          d(125) = d(125)/d(47)
          d(132) = d(132)/d(49)
          d(133) = d(133)/d(47)
          d(128) = d(128)/d(46)
      endif
      !4}}}

      ! Perhaps compute the threshold based 1D approximation
      if(d(190) .lt. 0.d0) then
         d(190) = 81.d0/7698.d0 * d(77)/d(78) ! = sigma_c^2/(2*E), where sigma_c =9/16*sqrt(E*Gc/(6*l_0))
      endif

      !3}}}
      !wave speed{{{3
      !Bulk modulus
      d(65) = d(33) + (2.d0/3.d0)*d(16)
      !P-wave modulus
      d(66) = d(1)*(1.d0-d(2))/((1.d0+d(2))*(1.d0-2.d0*d(2)))
      !P-wave speed
      d(67) = sqrt(d(66)/d(3))
      !3}}}
      !2}}}

      if(ior.lt.0) then
         write(*,3000) d(1),d(2),d(3),d(4),d(5),d(6),d(7),d(8),d(9),
     &        d(10),d(11),d(12),d(13),d(14),d(15)
      endif
      
      write(iow,3000) d(1),d(2),d(3),d(4),d(5),d(6),d(7),d(8),d(9),
     &     d(10),d(11),d(12),d(13),d(14),d(15)
      
      if(d(107).ne.0.0) then
         write(iow,3040) d(107), d(108), d(109)
      endif
      
 3000 format(/5x,'2D   Phase field Element'//
     &     10x,'e1 = ',1pe13.5/
     &     10x,'nu = ',1pe13.5/
     &     10x,'rh = ',1pe13.5/
     &     10x,'cp = ',1pe13.5/
     &     10x,'tq = ',1pe13.5/
     &     10x,'tk = ',1pe13.5/
     &     10x,'kp = ',1pe13.5/
     &     10x,'gr = ',1pe13.5/
     &     10x,'m1 = ',1pe13.5/
     &     10x,'s0 = ',1pe13.5/
     &     10x,'g0 = ',1pe13.5/
     &     10x,'n1 = ',1pe13.5/
     &     10x,'t0 = ',1pe13.5/
     &     10x,'de = ',1pe13.5/
     &     10x,'k1 = ',1pe13.5/x)
      
 3010 format(10x,'PLANE STRESS ANALYSIS'//)
 3020 format(10x,'PLANE STRAIN ANALYSIS'//)
 3030 format(10x,'AXIS-SYMMETRIC ANALYSIS'//)

 3040 format(10x,'Regularized/Stabalized fromulation'//
     &     10x,'flag  = ',1pe13.5/
     &     10x,'gamma = ,'1pe13.5/
     &     10x,'lc    = ,'1pe13.5/x)

      end subroutine inptsb
