c     $Id:nc   $
      subroutine umacr1(lct,ctl)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
c     Purpose:  Create VTK file

c     Inputs:
c     lct       - Command character parameters
c     ctl(3)    - Command numerical parameters
c                 (1) - 
c                 (2) - Set with value 1 to export to OUT folder
c                 (3) - Set the step for the vtus
        ! Save step '1' and every 'sstep' steps only (PVIE,,,,sstep)
c     Outputs:
c     N.B.  Users are responsible for command actions.  See
c     programmers manual for example.
c-----[--.----+----.----+----.-----------------------------------------]

!TO ADD NEW PROJECTED FIELDS
!   1- Add number of new fields to numNames
!   2- Open file 'eloutinfo.f'
!   3- In the subroutine getnames() add the fields
!   4- In the subroutine getncomp() add the components of the fields
!   5- In the element subroutine getndataXX() add the fields  
!   6- In the element subroutine prjXX() add the fields  
        
      implicit  none

      include  'cdat1.h'
      include  'cdata.h'
      include  'comfil.h'
      include  'counts.h'
      include  'elname.h'
      include  'iofile.h'
      include  'iodata.h'
      include  'pointer.h'
      include  'sdata.h'
      include  'eldata.h'
      include  'strnum.h'
      include  'tdata.h'
      include  'umac1.h'
      include  'setups.h'
      include  'pfeapb.h'
      include  'comblk.h'
      include  'prflag.h'
      include  'prjnam.h'
      include  'idptr.h'
      include  'pdata3.h'

      logical   pcomp,errck,ualloc,tinput,tinput2,setvar,fail
      logical   rualloc, wualloc, palloc
      character lct*15,text*30,fnamep*128,wfname*128
      character fname*128,fnameeig*128,fname2*128
      integer   i,ii,node,j,ix(4),jj, EV, GP, nfields, ngpex
      real*8    ctl(3),td(15),crval
      integer   kn,kp,pvdct,nnum,numnpp,sstep,ncomp,nw,expf
      integer   mat,elt,nenc,nev,max_ph,fill,elt1,pntasks,prank
      integer   ratei,ratej,elnums(num_user_el,2),nndel,numelpn
      integer   pmaxr, pmaxs
      
      save

      integer  trk

!     initialize trk subroutine pointer
      trk = 8
c     Define Fixed Parameters
      numNames = 200 !Number of array names
      numWArrs = 6   !Number of types of arrays (4 program + 2 user)

c     Set command word
      if(pcomp(uct,'mac1',4)) then ! Usual    form
         uct = 'pvew'           ! Specify 'name'
      elseif(urest.eq.1) then   ! Read  restart data
         setvar = rualloc(6 ,'PVDCT',ios)
         setvar = rualloc(11,'PROJI',ios)
         setvar = rualloc(12,'NPROJ',ios)
         setvar = rualloc(66,'NUMPP',ios)
         setvar = rualloc(67,'NPLND',ios)

      elseif(urest.eq.2) then  ! Write restart data
         setvar = wualloc(6 ,'PVDCT',1,1,ios) 
         setvar = wualloc(11,'PROJI',5*numWArrs*numNames,1,ios)
         setvar = wualloc(12,'NPROJ',2*numWArrs,1,ios)
         setvar = wualloc(66,'NUMPP',1,1,ios)
         setvar = wualloc(67,'NPLND',2*numnp,1,ios) 

      elseif (pcomp(lct,'AUTO',4))then
c        Set-up Automatic Arrays Export {{{
         text = 'start'
         elt1   = mr(np(32) + nie-2) !element type for material set 1
         setvar = ualloc(11,'PROJI',5*numWArrs*numNames,1)
         setvar = ualloc(12,'NPROJ',2*numWArrs,1)
         !Initialize variables ProjData(200,6,5)=mr(up(11))
         !Initialize variables PROjNums(6,2)=mr(up(12))
         call pzeroi(mr(up(11)),5*numWArrs*numNames) 
         call pzeroi(mr(up(12)),2*numWArrs)
         
         do !Read Arrays Line by line
            errck = tinput2(text,1,td,1)
            if (pcomp(text,'    ',4)) then
               exit
            else
               call setupPData(text,mr(up(11)),mr(up(12)),elt1)
            endif
         enddo
c        }}}         
      else

         if (pcomp(lct,'NEWSM',5)) then 
            ! Allocate for new Simulation {{{
            errck = ualloc(6,'PVDCT',1,1) !Allocate PVD counter
            !Allocate projection array
            write(*,*) 'numnm,npstr',numnm,npstr
            setvar = palloc(58,'NDNP',numnm*npstr,2,trk)
            mr(up(6)) = 0
            if(up(66).eq.0) then ! Allocate NUMPP 
               errck =  ualloc(66,'NUMPP',1,1) 
               if(.not.errck) write(*,*) 'UALLOC ERROR - NUMPP' 
               errck =  ualloc(67,'NPLND',2*numnp,1) 
               if(.not.errck) write(*,*) 'UALLOC ERROR - NPLND' 
               call getnumnpp (mr(up(67)),mr(up(66)))
            endif
            ! }}}
         endif
         !Get pvd step and data from input 
         pvdct = mr(up(6))
         expf  = merge(nint(ctl(2)),0,ctl(2).ne.0)   !Export to folder
         sstep = merge(nint(ctl(3)),1,ctl(3).ne.0)   !Step of Export
         if((mod(nstep,sstep).ne.0).and.(pvdct.gt.1)) then
           return
         endif
         
         ! Try to correct the projection array for multiple materials
         setvar = palloc(321,'PTEM1',2*numnp,1,trk)
         call proj_matc(mr(np(33)),nen,nen1,numel,mr(np(321)),
     &                  numnp, numnm)
         call sum_multiple_material_projected_quantities(mr(np(321)), 
     &                                                   hr(np(58)) )
         setvar = palloc(321,'PTEM1',0,1,trk)

         if (pfeap_on)then
             call getParData(pntasks,prank)
         else
             pntasks = 1
             prank   = 0
         endif
         
         !Set Filename {{{
         if(pntasks.gt.1) then
            j = index(fplt,' ')
            fname(1:128) = ' '
            fname(1:j-1)=fplt(1:j-1)
            fname(j:j)  = '_'
            write(fname(j+1:j+6),'(i6.6)') pvdct
            call addext(fname,'vtu',128,3)
            
            !Now that we create the parallel file .vtm
            if(prank.eq.0) then
               fnamep(1:128) = ' '
               fnamep(1:j-1)=fplt(1:j-1)
               write(fnamep(j-4:j+1),'(i6.6)') pvdct
               call addext(fnamep,'vtm',128,3)           
               if (expf.gt.0) call addfolder(fname,finp)
               open(unit=98,file=fnamep,access='sequential')
               write(98,7002) !Start xml VTM file
               write(98,7041)
               do ii = 1,pntasks
                  wfname(1:128) = fname(1:128)
                  write(wfname(j-4:j-1),'(i4.4)') ii
                  write(98,7043) ii-1,trim(wfname)
               enddo
               write(98,7042)
               write(98,7001) !End VTM
               close(98)
            endif
                               
            if (expf.gt.0) call addfolder(fname,finp)
            open(unit=99,file=fname,access='sequential') ! every task open a new file
            if(prank.eq.0) then
               write(*,*) 'Saving Parallel data to ',trim(fnamep)  ! just for output show some data were written into a seperate file
            endif
         else
            j = index(fplt,' ')
            fname(1:128) = ' '
            fname(1:j-1)=fplt(1:j-1)
            write(fname(j:j+5),'(i6.6)') pvdct
            call addext(fname,'vtu',128,3)
            if (expf.gt.0) call addfolder(fname,finp)
            open(unit=99,file=fname,access='sequential')
            write(*,*) 'Saving PARAVIEW data to ',trim(fname)
         endif
         ! }}}
         numnpp = mr(up(66))         !Number of plotting nodes
         !numnpp = 5
         elt1   = mr(np(32) + nie-2) !Element type for material set 1
         
          write(*,*) 'elt1  ',elt1   !Element type for material set 1
          write(*,*) 'pntasks',pntasks !Number of partition
          write(*,*) 'prank  ', prank  ! the ith partition
          write(*,*) 'numnpp ',numnpp !Number of plotting nodes
          write(*,*) 'numpn  ' ,numpn !Number of partition nodes
          write(*,*) 'numel  ',numel  !Number of  element
          write(*,*) 'numtn  ',numtn  !Number of total problem nodes
          write(*,*) 'numpeq ',numpeq !Number of totl problem eqns
         ! Data exchange for correct parallel output values
         
         pmaxr = 0
         pmaxs = 0
         do i = 0, ntasks-1
            pmaxr = max( pmaxr, mr(np(248)+i) )
            pmaxs = pmaxs + mr(np(250)+i)
         end do
         pmaxr = pmaxr*ndf
         pmaxs = pmaxs*ndf

         setvar = palloc( 120, 'TEMP0', max(1,pmaxr+pmaxs), 2,trk)
c $$$         call FeapSendProj(,mr(np(248)),mr(np(249)),mr(np(250)),
c $$$     &        mr(np(251)),mr(id31),ndf,hr(np(120)),hr(np(120+pmaxr)))
         setvar = palloc( 120, 'TEMP0',    0, 2,trk)
         
         ! Write VTU top header 
         write(99,1000) 

c        -- FIELD DATA --
         write(99,1080) !Open Field Data
         !Write Time {{{
         write(99,1036) 'Float64',1,2,'time'
         write(99,5000) ttim
         write(99,5000) dt
         write(99,5500)      ! new line
         write(99,1031)
         !}}}
         !Write global eigenvalues {{{
         if (up(85).ne.0)then
           nev    = mr(up(84))
           !Eigenvalues {{{
           write(99,1036) 'Float64',nev,3,'Global Eigenvalues'
           do i = 0,nev-1
                write(99,5000) hr(up(85)+i*3+0)
                write(99,5000) hr(up(85)+i*3+1)
                write(99,5000) hr(up(85)+i*3+2)
                write(99,5500)      ! new line
           end do
           write(99,1031)!}}}
           !Hermitian Eigenvalues {{{
           write(99,1036) 'Float64',nev,3,'Global Herm Eigenvalues'
           do i = 0,nev-1
                write(99,5000) hr(up(120)+i*3+0)
                write(99,5000) hr(up(120)+i*3+1)
                write(99,5000) hr(up(120)+i*3+2)
                write(99,5500)      ! new line
           end do
           write(99,1031)!}}}
           !Rayleigh quoient{{{
           write(99,1036) 'Float64',1,2,'Rayleigh Quotient'
                write(99,5000) hr(up(115)+0)
                write(99,5000) hr(up(115)+1)
                write(99,5500)      ! new line
           write(99,1031)!}}}
           !Norm Proj Ration{{{
           write(99,1036) 'Float64',1,1,'Norm Proj Ratio'
                write(99,5000) hr(up(115)+2)
                write(99,5500)      ! new line
           write(99,1031)!}}}
           !Herm Rayleigh quoient{{{
           write(99,1036) 'Float64',1,2,'Herm Rayleigh Quotient'
                write(99,5000) hr(up(115)+3)
                write(99,5000) hr(up(115)+4)
                write(99,5500)      ! new line
           write(99,1031)!}}}
           !Dot Product{{{
           write(99,1036) 'Float64',nev,1,'GlobEV Dot Product'
           do i = 0,nev-1
                write(99,5000) hr(up(116)+i*2+0)
                write(99,5500)      ! new line
           end do
           write(99,1031)!}}}
           !Non-Orth Decomposition{{{
           write(99,1036) 'Float64',nev,1,'GlobEV NonOrth Decomp'
           do i = 0,nev-1
                write(99,5000) hr(up(116)+i*2+1)
                write(99,5500)      ! new line
           end do
           write(99,1031)!}}}
         endif
         ! }}}
         !Write rank of problem {{{
         if(pntasks.gt.1) then
            write(99,1036) 'Float64',1,1,'ProcID'
            write(99,'(i6.6)') prank
            write(99,1031)
         endif
         ! }}}
         write(99,1081) !Close Field Data

c        -- PIECE DATA --
         write(99,1010) numnpp,numel ! Start Mesh/Piece Section
         ! MESH {{{
         ! Point Data {{{
         write(99,1020)         ! Start Point/Node data
         write(99,1030) 'Float64','nodes',3

         if(pntasks.gt.1) then
            if(up(111).eq.0) then
               errck = ualloc(111,'LTOGN',numnpp,1)
               errck = ualloc(112,'GTOLN',numnp, 1)
               call getpordering(mr(up(111)),mr(up(112)),numnpp)
               call iprint(mr(up(111)),1,numnpp,1,'LTOGN')
               call iprint(mr(up(112)),1,numnp,1,'GTOLN')
            endif
            do i = 1,numnpp
               node = mr(up(111)-1 + i) ! Global node number
               do ii = 1,ndm
                  write(99,5000) hr(np(43)+(node-1)*ndm+(ii-1))
!                  write(99,5000) hr(np(43)+(i-1)*ndm+(ii-1)) 
               end do
               do ii = 1,3-ndm
                  write(99,5000) 0.0
               end do
            end do
         else
            do i = 1,numnpp
               do ii = 1,ndm
                  write(99,5000) hr(np(43)+(i-1)*ndm+(ii-1))
               end do
               do ii = 1,3-ndm
                  write(99,5000) 0.0
               end do
            end do
         endif
         

         write(99,1031)         ! Close Node data
         write(99,1021)         ! Close Points section
         ! }}}
         write(99,1050)         ! Start Cell Section
         !Element Data {{{
         write(99,1035) 'Int32','connectivity' ! Start Elements

         setvar = palloc(111,'TEMP1',numel+1,1,trk) !Allocate Offsets array
         mr(np(111)) = 0         
!         if (nstep.eq.0) then
!         do i=1,numnpp
!         	write(*,*) 'local to global',i, mr(up(111) +i -1)
!         enddo
!         
!         do i=1,numnpp
!         	write(*,*) 'global to local',i, mr(up(112) +i -1)
!         enddo
!         endif
         do i = 1,numel
            mr(np(111)+i) = mr(np(111)+i-1)
            mat = mr(np(33) + nen1*(i-1) + nen1-1)
            elt= mr(np(32) + nie*(mat-1) + nie-2)
            if (elt.lt. 0) then
               nenc = nen
            else
               call getelnums(elnums)
               nenc = elnums(elt,1)
            endif
            do ii = 1,nenc
               node = mr(np(33) + (ii-1) + nen1*(i-1)) ! local node number
               if (node .ne. 0) then
                  if(pntasks.gt.1) then
                     write(99,6000) mr(up(112)-1 + node)-1
!                     write(99,6000) node-1 
                  else
                     write(99,6000) node-1
                  endif           
                  mr(np(111)+i) = mr(np(111)+i) + 1
               endif
            end do
            write(99,5500)      ! new line
         end do

         write(99,1031)         ! Close Elements       
         ! }}}
         ! Offset Data {{{
         write(99,1030) 'Int32','offsets',1 ! Start Offsets
         do i = 1,numel
            write(99,6000) mr(np(111)+i)
         end do

         write(99,1031) ! Close Offsets
         ! }}}
         !Element Type Data {{{
         write(99,1030) 'UInt8','types',1 ! Start Element types
         do i = 1,numel
               numelpn=mr(np(111)+i)-mr(np(111)+i-1)
               if (ndm .eq. 1) then
                  if (numelpn .eq. 1) then ! 2 node line
                     write(99,6000) 1
                  elseif (numelpn .eq. 2) then ! 2 node line
                     write(99,6000) 3
                  elseif (numelpn .eq. 3) then ! 3 node quadractic line
                     write(99,6000) 21
                  endif
               elseif(ndm .eq. 2) then
                  !write(*,*)'numelpn',numelpn
                  if (numelpn.eq.2)then        ! 2 node line
                     write(99,6000) 3
                  elseif (numelpn .eq. 3) then ! 3 node triangle
                     write(99,6000) 5
                  elseif (numelpn .eq. 4) then 
                     write(99,6000) 9          ! 4 node quad
                  elseif (numelpn .eq. 6) then 
                     write(99,6000) 22         ! 6 node quadratic quad
                  elseif (numelpn .eq. 8) then 
                     write(99,6000) 23          ! 8 node quadracitc quad
                  endif
               elseif (ndm .eq. 3) then
                  if (numelpn .eq. 2) then ! 2 node line
                     !write(*,*)'numelpn',numelpn
                     write(99,6000) 3
                  elseif (numelpn .eq. 4) then      ! 4 node tetrahedon
                    write(99,6000) 10   
                  elseif (numelpn .eq. 5) then  ! 5 node pyramid
                    write(99,6000) 14
                  elseif (numelpn .eq. 6) then  ! 6 node wedge
                    write(99,6000) 13    
                  elseif (numelpn .eq. 8) then  ! 8 node brick
                    write(99,6000) 12
                  elseif (numelpn .eq. 10) then ! 10 node quadratic tetrahedron
                    write(99,6000) 24  
                  elseif (numelpn .eq. 27) then ! 27 node brick (plot as 20 node)
                    write(99,6000) 25
                  endif
               endif
         enddo
         setvar = palloc(111,'TEMP1',0,1,trk)

         write(99,1031)         ! Close Element types
         !}}}
         write(99,1051)         ! Close Cell Section
         !}}}        
         ! POINT DATA {{{
         write(99,1060)         ! Start Point Data
         call WritePointData(elt1,ndf,numnpp,numnp,.False.)
         write(99,1061)         ! Close Point Data Section
         ! }}}
         ! CELL DATA {{{
         write(99,1070) !start Cell data
         if(pntasks.gt.1) then
            write(99,1030) 'Float64','ProcID',1
            do i = 1,numel
               write(99,6000) prank
            enddo
            write(99,1031)
         endif
!        Export Element/GaussP Eigenvalues  {{{
         if (up(82).ne.0)then
            do EV = 1,mr(up(83))
               if (mr(up(97)).eq.1) then
                  write(99,1040) 'Float64','Element Eval.',EV,2  ! Start Element Ev.
                  do i = 1,numel
                     ii=((i-1)*mr(up(83))*mr(up(97))+EV-1)*2
                     write(99,5000) hr(up(82)+ii)
                     write(99,5000) hr(up(82)+ii+1)
                     write(99,*) '' !Adds new line
                  enddo
                  write(99,1031) ! Close Element Ev.
               else
                  do GP = 1,mr(up(97))
                     write(99,1041) 'Float64','Eval.',EV,' Gauss-P',GP,2! Start Gauss Ev.
                     do i = 1,numel
                        ii=((i-1)*mr(up(83))*mr(up(97))
     &                     +(GP-1)*mr(up(83))+EV-1)*2
                        write(99,5000) hr(up(82)+ii)
                        write(99,5000) hr(up(82)+ii+1)
                        write(99,*) '' !Adds new line
                     enddo
                     write(99,1031) ! Close Element Ev.
                  enddo
               endif
            end do
         endif
c        }}}   
!        Export Element/GaussP IG and RQ  {{{
         if (up(150).ne.0)then
            if (mr(up(97)).eq.1) then
               write(99,1030) 'Float64','Element IG',1  ! Start Element IG
               do i = 1,numel
                  write(99,5000) hr(up(150)+(i-1)*2*mr(up(97))-1+1)
                  write(99,*) '' !Adds new line
               enddo
               write(99,1031) ! Close Element IG
               write(99,1030) 'Float64','Element RQ',1  ! Start Element RQ
               do i = 1,numel
                  write(99,5000) hr(up(150)+(i-1)*2*mr(up(97))-1+2)
                  write(99,*) '' !Adds new line
               enddo
               write(99,1031) ! Close Element RQ
            else
               write(*,*) 'IGRQ for Gauss points not implemented'
            endif
         endif
c        }}}   
         ! Export GP Values {{{
         if (up(101).ne.0) then
             nfields=mr(up(102))
             ngpex=mr(up(102)+1)
             do GP = 1,ngpex
                 write(99,1040) 'Float64','Values of GP',GP,nfields 
                 do i = 1,numel
                     do j = 1,nfields
                         ii=ngpex*nfields*(i-1) +nfields*(GP-1)+j
                         crval = hr(up(101)+ii-1)
                         if (dabs(crval).gt.1e-20)then
                             write(99,5000) crval
                         else
                             write(99,5000) 0.d0
                         endif
                     end do
                     write(99,*) '' !Adds new line
                 end do
                 write(99,1031) !Close GP Values
             enddo
         endif
c        }}}         
         write(99,1071) ! Close Cell data
         ! }}}
         write(99,1011)         ! Close Mesh/Piece
         
         ! Close VTU
         write(99,1001)
         close(99)
         
         mr(up(6)) = pvdct + 1 !Update pvd counter
         return

      endif

!Formats{{{1
 1000 format('<?xml version="1.0"?>',/
     *     '<VTKFile type="UnstructuredGrid" version="0.1" ',
     &     'byte_order="LittleEndian">'/
     *     '<UnstructuredGrid>')
      
 1001 format('</UnstructuredGrid> </VTKFile>')

 1002 format('<?xml version="1.0"?>',/
     *     '<VTKFile type="Collection" version="0.1">',/
     *     '<Collection>')

 1010 format('<Piece NumberOfPoints="',i10,'" NumberOfCells="',i10,'">')
 1011 format('</Piece>')


 1020 format('<Points>')
 1021 format('</Points>')

 1030 format('<DataArray type="',a,'" 
     *Name="',a,'"
     *NumberOfComponents="',i2,'" format="ascii">')
 1031 format('</DataArray>')
 1035 format('<DataArray type="',a,'" 
     *Name="',a,'" format="ascii">')
      
 1036 format('<DataArray type="',a,'" 
     *NumberOfTuples="',i2,'"
     *NumberOfComponents="',i2,'"
     *Name="',a,'" format="ascii">')

 1040 format('<DataArray type="',a,'" 
     *Name="',a,i2,'"
     *NumberOfComponents="',i2,'" format="ascii">')

 1041 format('<DataArray type="',a,'" 
     *Name="',a,i2,a,i2'"
     *NumberOfComponents="',i2,'" format="ascii">')
      
 1050 format('<Cells>')
 1051 format('</Cells>')

 1060 format('<PointData>')
 1061 format('</PointData>')

 1070 format('<CellData>')
 1071 format('</CellData>')

 1080 format('<FieldData>')
 1081 format('</FieldData>')

 2000 format('<DataSet timestep="',es13.7,'" part="0" file="',a,'"/>')

 3000 format('</Collection>',/
     *     '</VTKFile>')

 5000 format(1pe17.10,' ',$)
 5500 format(/,$)
 6000 format(i6,' ',$)
 6001 format(i10,' ',$)

 7000 format('<?xml version="1.0"?>',/
     *     '<VTKFile type="PUnstructuredGrid" version="0.1" ',
     &     'byte_order="LittleEndian">')
 7001 format('</VTKFile>')
 7002 format('<?xml version="1.0"?>',/
     *     '<VTKFile type="vtkMultiBlockDataSet" version="1.0" ',
     &     'byte_order="LittleEndian">')

 7010 format('<PUnstructuredGrid GhostLevel="0">')
 7011 format('</PUnstructuredGrid>')
 7020 format('<PCellData>')
 7021 format('</PCellData>')

 7030 format('<PPoints>')
 7031 format('</PPoints>')

 7040 format('<Piece Source="',a,'"/>')
 7041 format('<vtkMultiBlockDataSet>')
 7042 format('</vtkMultiBlockDataSet>')
 7043 format('<DataSet index="',i2,'" file="',a,'"/>')

 7050 format('<PDataArray type="',a,'" 
     *NumberOfTuples="',i2,'"
     *NumberOfComponents="',i2,
     *'" Name="',a,'"/>')
 7051 format('<PDataArray type="',a,'" Name="',a,'"
     *NumberOfComponents="',i2,'" format="ascii"/>')
 7060 format('<PPointData>')
 7061 format('</PPointData>')
!1}}}
      end subroutine umacr1

      !WritePointData{{{1
      subroutine WritePointData(el,ndf,numnpp,numnp,pvtu)
      
      implicit none
      
      include  'pointer.h'
      include  'comblk.h'
      
      integer el,ndf,numnpp,numnp,kn,i
      integer*8 l
      integer autoelem(59),nev
      logical isauto,pvtu

      autoelem=(/1,2,3,4,5,6,7,8,21,22,20,24,25,26,27,28,29,30,31,32
     &  ,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,54,56
     &  ,57,58,59,60,61,62,63,64,65,66,67,68,69,70,90,91,92,93,94/) !List of the elements prepared for AUTO format
      isauto=any(el.eq.autoelem) !Check if el is in list
      nev = mr(up(84))

      if (isauto) then
          call WritePointAuto(mr(up(11)),mr(up(12)),numnpp,numnp,ndf,
     &        pvtu)

      !1D Velocity Based SB elements (1) {{{2
      elseif (el .eq. 1 ) then 
         call vtkoutr3(hr(np(40)),2,2,1,1,1,numnpp,1,ndf,numnp,pvtu)!Velocities
         call vtkoutr3(hr(np(40)),4,1,2,2,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         
         !Projected quantities
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,1,2,2,1,numnpp,numnp,pvtu)!Stresses
            call vtkoutr2(hr(np(58)),6,1,4,4,1,numnpp,numnp,pvtu)!EQPS
            call vtkoutr2(hr(np(58)),26,1,3,3,1,numnpp,numnp,pvtu)!Rate of Deformation
            call vtkoutr2(hr(np(58)),9,1,5,5,1,numnpp,numnp,pvtu)!Diffusive Term
            call vtkoutr2(hr(np(58)),10,1,6,6,1,numnpp,numnp,pvtu)!Source Term
            call vtkoutr2(hr(np(58)),11,1,7,7,1,numnpp,numnp,pvtu)!Stability Criteria
            call vtkoutr2(hr(np(58)),32,1,10,10,1,numnpp,numnp,pvtu)!Druckers Postulate
            call vtkoutr2(hr(np(58)),33,1,11,11,1,numnpp,numnp,pvtu)!Element Eigenvalues
            call vtkoutr2(hr(np(58)),39,1,12,12,1,numnpp,numnp,pvtu)!WMA Stability Criterion
            call vtkoutr2(hr(np(58)),42,1,13,13,1,numnpp,numnp,pvtu)!NLTQ Stability Criterion
         endif
         !displacements
         if (up(3) .ne.0)then
            kn = mr(up(3))
            call vtkoutr2(hr(up(kn)),1,1,1,1,1,numnpp,numnpp,pvtu)!Displacements
         endif
      !2}}}
      !2D Displacement Based Plasticity element (3) {{{2
      elseif (el.eq.3) then
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Displacements
         call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)), 5,6, 2, 5,4,numnpp,numnp,pvtu)!Stresses
c            call vtkoutr2(hr(np(58)), 5,6, 2, 5,4,numnpp,numnp)!Strain
            call vtkoutr2(hr(np(58)), 6,1,15,15,1,numnpp,numnp,pvtu)!Eq. Pl. Strain
         endif
      !2D Velocity Based SB elements (18) {{{2
      elseif (el .eq. 3 .or. el .eq.18) then 
         call vtkoutr3(hr(np(40)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
         call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)), 5,6, 2, 5,4,numnpp,numnp,pvtu)!Stresses
            call vtkoutr2(hr(np(58)), 7,1, 6, 6,1,numnpp,numnp,pvtu)!VM Stress
            call vtkoutr2(hr(np(58)),26,6, 7,10,4,numnpp,numnp,pvtu)!Rate of Deformation
            call vtkoutr2(hr(np(58)), 6,1,11,11,1,numnpp,numnp,pvtu)!Eq. Pl. Strain
            call vtkoutr2(hr(np(58)), 9,1,12,12,1,numnpp,numnp,pvtu)!Diffusive term
            call vtkoutr2(hr(np(58)),10,1,13,13,1,numnpp,numnp,pvtu)!Source term
            call vtkoutr2(hr(np(58)),11,1,14,14,1,numnpp,numnp,pvtu)!Stability Criteria
            call vtkoutr2(hr(np(58)),32,1,20,20,1,numnpp,numnp,pvtu)!Druckers Postulate
            call vtkoutr2(hr(np(58)),33,1,21,21,1,numnpp,numnp,pvtu)!Element Eigenvalues
            call vtkoutr2(hr(np(58)),39,1,22,22,1,numnpp,numnp,pvtu)!WMA Stability Criterion
            call vtkoutr2(hr(np(58)),27,1,38,38,1,numnpp,numnp,pvtu)!Mechanical Work Rate
            call vtkoutr2(hr(np(58)),84,1,39,39,1,numnpp,numnp,pvtu)!Elastic Work Rate
            call vtkoutr2(hr(np(58)),85,1,40,40,1,numnpp,numnp,pvtu)!Thermal Work Rate
            call vtkoutr2(hr(np(58)),54,1,41,41,1,numnpp,numnp,pvtu)!Inelastic Work Rate
            call vtkoutr2(hr(np(58)),86,1,42,42,1,numnpp,numnp,pvtu)!Kinetic Energy Rate
         endif
         !Global Eigenvectors
         if (up(74) .ne. 0) then
            l = up(74) + ndf*numnp*0
            call vtkoutr3(hr(l),25,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(l),13,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
            l = up(74) + ndf*numnp*1
            call vtkoutr3(hr(l),64,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(l),65,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
            l = up(74) + ndf*numnp*2
            call vtkoutr3(hr(l),69,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(l),70,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
            l = up(74) + ndf*numnp*3
            call vtkoutr3(hr(l),74,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(l),75,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature

            if (np(58).ne.0)then  !With Gauss-Points
               call export58(14,6,14,17,pvtu)!Stress
               call export58(15,1,19,19,pvtu)!EQPS
               call export58(66,6,22,25,pvtu)!Stress
               call export58(67,1,26,26,pvtu)!EQPS
               call export58(71,6,27,30,pvtu)!Stress
               call export58(72,1,31,31,pvtu)!EQPS
               call export58(76,6,32,35,pvtu)!Stress
               call export58(77,1,36,36,pvtu)!EQPS
            endif

         endif
         !Displacements
         if (up(3) .ne.0)then
            kn = mr(up(3))
            call vtkoutr2(hr(up(kn)),1,3,1,2,2,numnpp,numnpp,pvtu)!Displacements
         endif
      !2}}}
      !1D displacement based SB elements{{{2
      elseif ( el.eq. 39)then
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),4,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !Temperature
         
         !Projected quantitites
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,1,2,2,1,numnpp,numnp,pvtu) !Stresses
            call vtkoutr2(hr(np(58)),8,1,3,3,1,numnpp,numnp,pvtu) !Infinitessimal Strain
            call vtkoutr2(hr(np(58)),6,1,4,4,1,numnpp,numnp,pvtu) !EQPS
            call vtkoutr2(hr(np(58)),9,1,5,5,1,numnpp,numnp,pvtu) !Diffusive Term
            call vtkoutr2(hr(np(58)),10,1,6,6,1,numnpp,numnp,pvtu) !Source Term
            call vtkoutr2(hr(np(58)),27,1,7,7,1,numnpp,numnp,pvtu) !Mechanical work rate
            call vtkoutr2(hr(np(58)),37,1,8,8,1,numnpp,numnp,pvtu) !Rate of stress
            call vtkoutr2(hr(np(58)),38,1,9,9,1,numnpp,numnp,pvtu) !TQ
         endif

         !Rate Quantities
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Velocities
            call vtkoutr3(hr(np(42)),3,3,1,1,1,numnpp,2,ndf,numnp,pvtu) !Accelerations
         endif
         
         !Export Global Eigenvectors
         if (up(74) .ne. 0) then  !Existence of Eigenvectors
           call vtkoutr3(hr(np(74)),12,3,1,1,1,numnpp,1,ndf,numnp,pvtu)!Displacements
           call vtkoutr3(hr(np(74)),13,1,2,2,1,numnpp,1,ndf,numnp,pvtu)!Temperature
           if (np(58).ne.0)then !With Gauss-Point Nodes
             call vtkoutr2(hr(np(58)),14,6,10,10,1,numnpp,numnp,pvtu)!Stresses
             call vtkoutr2(hr(np(58)),15,1,11,11,1,numnpp,numnp,pvtu)!EQPS
           endif
         endif
      !2}}}
      !1D displacement based SB elements{{{2
      elseif (el .eq. 4 )then
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),4,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !Temperature
         
         !Projected quantitites
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,1,2,2,1,numnpp,numnp,pvtu) !Stresses
            call vtkoutr2(hr(np(58)),6,1,4,4,1,numnpp,numnp,pvtu) !EQPS
            call vtkoutr2(hr(np(58)),8,1,3,3,1,numnpp,numnp,pvtu) !Infinitessimal Strain
            call vtkoutr2(hr(np(58)),9,1,5,5,1,numnpp,numnp,pvtu) !Diffusive Term
            call vtkoutr2(hr(np(58)),10,1,6,6,1,numnpp,numnp,pvtu) !Source Term
            call vtkoutr2(hr(np(58)),27,1,7,7,1,numnpp,numnp,pvtu) !Mechanical Work Rate
         endif

         !Rate Quantities
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Velocities
            call vtkoutr3(hr(np(42)),3,3,1,1,1,numnpp,2,ndf,numnp,pvtu) !Accelerations
         endif
         
         !Export Global Eigenvectors
         if (up(74) .ne. 0) then  !Existence of Eigenvectors
           call vtkoutr3(hr(np(74)),12,3,1,1,1,numnpp,1,ndf,numnp,pvtu)!Displacements
           call vtkoutr3(hr(np(74)),13,1,2,2,1,numnpp,1,ndf,numnp,pvtu)!Temperature
           if (np(58).ne.0)then !With Gauss-Point Nodes
             call vtkoutr2(hr(np(58)),14,6,8,8,1,numnpp,numnp,pvtu)!Stresses
             call vtkoutr2(hr(np(58)),15,1,9,9,1,numnpp,numnp,pvtu)!EQPS
           endif
         endif
      !2}}}
      !1D displacement based SB elements{{{2
      elseif (el .eq. 45)then
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),4,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !Temperature
         
         !Projected quantitites
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,1,2,2,1,numnpp,numnp,pvtu) !Stresses
            call vtkoutr2(hr(np(58)),6,1,4,4,1,numnpp,numnp,pvtu) !EQPS
            call vtkoutr2(hr(np(58)),8,1,3,3,1,numnpp,numnp,pvtu) !Infinitessimal Strain
            call vtkoutr2(hr(np(58)),9,1,5,5,1,numnpp,numnp,pvtu) !Diffusive Term
            call vtkoutr2(hr(np(58)),10,1,6,6,1,numnpp,numnp,pvtu) !Source Term
            call vtkoutr2(hr(np(58)),27,1,7,7,1,numnpp,numnp,pvtu) !Mechanical Work Rate
            call vtkoutr2(hr(np(58)),37,1,8,8,1,numnpp,numnp,pvtu) !Rate of stress
         endif

         !Rate Quantities
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Velocities
            call vtkoutr3(hr(np(42)),3,3,1,1,1,numnpp,2,ndf,numnp,pvtu) !Accelerations
         endif
         
         !Export Global Eigenvectors
         if (up(74) .ne. 0) then  !Existence of Eigenvectors
           call vtkoutr3(hr(np(74)),12,3,1,1,1,numnpp,1,ndf,numnp,pvtu)!Displacements
           call vtkoutr3(hr(np(74)),13,1,2,2,1,numnpp,1,ndf,numnp,pvtu)!Temperature
           if (np(58).ne.0)then !With Gauss-Point Nodes
             call vtkoutr2(hr(np(58)),14,6,9,9,1,numnpp,numnp,pvtu)!Stresses
             call vtkoutr2(hr(np(58)),15,1,10,10,1,numnpp,numnp,pvtu)!EQPS
           endif
         endif
      !2}}}
       !2D Displacement Based SB elements{{{2 
       elseif (el.eq.40) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Displacements
         call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)), 5,6, 2, 5,4,numnpp,numnp,pvtu)!Stresses
            call vtkoutr2(hr(np(58)), 7,1, 6, 6,1,numnpp,numnp,pvtu)!VM Stress
            call vtkoutr2(hr(np(58)), 8,6, 7,10,4,numnpp,numnp,pvtu)!Infinitessimal strain
            call vtkoutr2(hr(np(58)),34,6,11,14,4,numnpp,numnp,pvtu)!Infinitessimal strain rate
            call vtkoutr2(hr(np(58)), 6,1,15,15,1,numnpp,numnp,pvtu)!Eq. Pl. Strain
            call vtkoutr2(hr(np(58)), 9,1,16,16,1,numnpp,numnp,pvtu)!Diffusive term
            call vtkoutr2(hr(np(58)),10,1,17,17,1,numnpp,numnp,pvtu)!Source term
            call vtkoutr2(hr(np(58)),27,1,18,18,1,numnpp,numnp,pvtu)!Mechanical Work Rate
         endif
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu)!Accelerations
         endif
!2}}}
       !2D Large strain Displacement Based SB elements{{{2
       elseif (el .eq. 8) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Displacements
         call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,6,2,5,4,numnpp,numnp,pvtu)!Stresses
            call vtkoutr2(hr(np(58)),7,1,6,6,1,numnpp,numnp,pvtu)!VM Stress
            call vtkoutr2(hr(np(58)),26,6,7,10,4,numnpp,numnp,pvtu)!Rate of Deformation
            call vtkoutr2(hr(np(58)),6,1,11,11,1,numnpp,numnp,pvtu)!Eq. Pl. Strain
            call vtkoutr2(hr(np(58)),9,1,12,12,1,numnpp,numnp,pvtu)!Diffusive term
            call vtkoutr2(hr(np(58)),10,1,13,13,1,numnpp,numnp,pvtu)!Source term
            call vtkoutr2(hr(np(58)),27,1,14,14,1,numnpp,numnp,pvtu)!Mechanical Work Rate
         endif
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu)!Accelerations
         endif
         !Global Eigenvectors
         if (up(74) .ne. 0) then
             call vtkoutr3(hr(up(74)),12,3,1,2,2,numnpp,1,ndf,numnp,
     &           pvtu)!Displacement
             call vtkoutr3(hr(up(74)),13,1,3,3,1,numnpp,1,ndf,numnp,
     &            pvtu)!Temperature
             !With Gauss Points
             if (np(58).ne.0)then  !With Gauss-Points
               call vtkoutr2(hr(np(58)),14,6,15,18,4,numnpp,numnp,pvtu)!Stress
               call vtkoutr2(hr(np(58)),15,1,19,19,1,numnpp,numnp,pvtu)!EQPS
             endif
         endif
      !2}}}
       !2D Large strain Hyperelastic SB elements{{{2
       elseif (el .eq. 48) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Displacements
         call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,6,2,5,4,numnpp,numnp,pvtu)!Stresses
            call vtkoutr2(hr(np(58)),7,1,6,6,1,numnpp,numnp,pvtu)!VM Stress
            call vtkoutr2(hr(np(58)),41,6,7,10,4,numnpp,numnp,pvtu)!El. L CG
            call vtkoutr2(hr(np(58)),6,1,11,11,1,numnpp,numnp,pvtu)!Eq. Pl. Strain
            call vtkoutr2(hr(np(58)),9,1,12,12,1,numnpp,numnp,pvtu)!Diffusive term
            call vtkoutr2(hr(np(58)),10,1,13,13,1,numnpp,numnp,pvtu)!Source term
            call vtkoutr2(hr(np(58)),40,1,14,14,1,numnpp,numnp,pvtu)!Jacobian Det
         endif
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu)!Accelerations
         endif
         !Global Eigenvectors
         if (up(74) .ne. 0) then
             call vtkoutr3(hr(up(74)),12,3,1,2,2,numnpp,1,ndf,numnp,
     &            pvtu)!Displacement
             call vtkoutr3(hr(up(74)),13,1,3,3,1,numnpp,1,ndf,numnp,
     &            pvtu)!Temperature
             !With Gauss Points
             if (np(58).ne.0)then  !With Gauss-Points
               call vtkoutr2(hr(np(58)),14,6,15,18,4,numnpp,numnp,pvtu)!Stress
               call vtkoutr2(hr(np(58)),15,1,19,19,1,numnpp,numnp,pvtu)!EQPS
             endif
         endif
      !2}}}
      !1D PFSB elements{{{2
      elseif (el .eq. 25 ) then  
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),16,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !Phase Field
         call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,ndf,numnp,pvtu) !Temperature
         
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5 ,1,2,2,1,numnpp,numnp,pvtu) !Stresses
            call vtkoutr2(hr(np(58)),8 ,1,3,3,1,numnpp,numnp,pvtu) !Infinitessimal Strain
            call vtkoutr2(hr(np(58)),6 ,1,4,4,1,numnpp,numnp,pvtu) !EQPS
            call vtkoutr2(hr(np(58)),17,1,5,5,1,numnpp,numnp,pvtu) !Elastic Helmholtz free energy
            call vtkoutr2(hr(np(58)),24,1,6,6,1,numnpp,numnp,pvtu) !Surface Energy
            call vtkoutr2(hr(np(58)),37,1,7,7,1,numnpp,numnp,pvtu) !Rate of stress
            call vtkoutr2(hr(np(58)),38,1,8,8,1,numnpp,numnp,pvtu) !TQ
            call vtkoutr2(hr(np(58)),35,1,9,9,1,numnpp,numnp,pvtu) !Int. Var FE
            call vtkoutr2(hr(np(58)),27,1,10,10,1,numnpp,numnp,pvtu) !Mechanical Work Rate
         endif
         
         !Rate Quantities
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Velocities
            call vtkoutr3(hr(np(42)),3,3,1,1,1,numnpp,2,ndf,numnp,pvtu) !Accelerations
            call vtkoutr3(hr(np(42)),19,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !PF Velocities
            call vtkoutr3(hr(np(42)),20,1,2,2,1,numnpp,2,ndf,numnp,pvtu) !PF Accelerations
         endif
         
         !Export Global Eigenvectors
         if (up(74) .ne. 0) then !Existence of Eigenvectors
            call vtkoutr3(hr(np(74)),12,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
            call vtkoutr3(hr(np(74)),21,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !PF
            call vtkoutr3(hr(np(74)),13,1,3,3,1,numnpp,1,ndf,numnp,pvtu) !Temperature
            if (np(58).ne.0)then !With Gauss-Point Nodes
               call vtkoutr2(hr(np(58)),14,6,10,10,1,numnpp,numnp,pvtu) !Stresses
               call vtkoutr2(hr(np(58)),15,1,11,11,1,numnpp,numnp,pvtu) !EQPS
            endif
         endif
!2}}}
      !1D PF elements{{{2
      elseif (el .eq. 29 ) then 
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),16,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !Phase Field
         
         !Projected Quantities
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5 ,1,2,2,1,numnpp,numnp,pvtu) !Stresses
            call vtkoutr2(hr(np(58)),8 ,1,3,3,1,numnpp,numnp,pvtu) !Infinitessimal Strain
            call vtkoutr2(hr(np(58)),17,1,4,4,1,numnpp,numnp,pvtu) !El Helm. Free Energy
            !call vtkoutr2(hr(np(58)),24,1,5,5,1,numnpp,numnp,pvtu) !Surface Energy
            !call vtkoutr2(hr(np(58)),18,1,6,6,1,numnpp,numnp,pvtu) !PF Diffusive Term
            call vtkoutr2(hr(np(58)),22,1,7,7,1,numnpp,numnp,pvtu) !Effective Gc
            !call vtkoutr2(hr(np(58)),23,1,8,8,1,numnpp,numnp,pvtu) !PF Source Term
         endif
         
         !Rate Quantitites
         if (np(42).ne.0) then
            !call vtkoutr3(hr(np(42)),2,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Velocities
            !call vtkoutr3(hr(np(42)),3,3,1,1,1,numnpp,2,ndf,numnp,pvtu) !Accelerations
            !call vtkoutr3(hr(np(42)),19,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !PF Velocities
            !call vtkoutr3(hr(np(42)),20,1,2,2,1,numnpp,2,ndf,numnp,pvtu) !PF Accelerations
         endif

         !Export Global Eigenvectors
         if (up(74) .ne. 0) then !Existence of Eigenvectors
            call vtkoutr3(hr(np(74)),12,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
            call vtkoutr3(hr(np(74)),21,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !PF
            if (np(58).ne.0)then !With Gauss-Point Nodes
               call vtkoutr2(hr(np(58)),14,6,9,9,1,numnpp,numnp,pvtu) !Stresses
            else                !Without Gauss-Point Nodes
               call vtkoutr3(hr(np(74)),14,6,3,3,1,numnpp,1,ndf,numnp,
     &              pvtu) !Stresses
            endif
         endif
      !2}}}
      !Visco element (28) {{{2
      elseif(el.eq.28) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Displacements
         if (np(58).ne.0) then
           call vtkoutr2(hr(np(58)),8,6,2,5,4,numnpp,numnp,pvtu) !Total Strain
           call vtkoutr2(hr(np(58)),5,6,6,9,4,numnpp,numnp,pvtu) !Stresses
           call vtkoutr2(hr(np(58)),7,1,10,10,1,numnpp,numnp,pvtu) !von Mises stress
           call vtkoutr2(hr(np(58)),28,3,11,13,3,numnpp,numnp,pvtu) !Damage
           call vtkoutr2(hr(np(58)),34,6,14,17,4,numnpp,numnp,pvtu) !Strain Rate
           call vtkoutr2(hr(np(58)),82,6,18,21,4,numnpp,numnp,pvtu) !Deviatoric Stress
           call vtkoutr2(hr(np(58)),83,6,22,24,3,numnpp,numnp,pvtu) !Volumetric Stress
           call vtkoutr2(hr(np(58)),96,1,25,25,1,numnpp,numnp,pvtu) !Chi used in damage
         endif!2}}}
      !Elastic Damage element {{{
      elseif(el.eq.5) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Displacements
         if(np(58).ne.0) then
            call vtkoutr2(hr(np(58)), 5,6, 2, 5,4,numnpp,numnpp,pvtu) !Stresses
            call vtkoutr2(hr(np(58)), 8,6, 6, 9,4,numnpp,numnpp,pvtu) !Strains
            call vtkoutr2(hr(np(58)),49,1,10,10,1,numnpp,numnpp,pvtu) !Local damage
            call vtkoutr2(hr(np(58)),50,1,11,11,1,numnpp,numnpp,pvtu) !Non Local damage
         endif !}}}
       !2D PFSBCW elements{{{2 
       elseif (el.eq.32 ) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Displacements
         call vtkoutr3(hr(np(40)),16,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Phase Field
         call vtkoutr3(hr(np(40)),4,1,4,4,1,numnpp,1,ndf,numnp,pvtu)!Temperature
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)), 5,6, 2, 5,4,numnpp,numnp,pvtu)!Stresses
            call vtkoutr2(hr(np(58)), 7,1, 6, 6,1,numnpp,numnp,pvtu)!VM Stress
            call vtkoutr2(hr(np(58)), 8,6, 7,10,4,numnpp,numnp,pvtu)!Infinitessimal strain
            call vtkoutr2(hr(np(58)),34,6,11,14,4,numnpp,numnp,pvtu)!Infinitessimal strain rate
            call vtkoutr2(hr(np(58)), 6,1,15,15,1,numnpp,numnp,pvtu)!Eq. Pl. Strain
            call vtkoutr2(hr(np(58)), 9,1,16,16,1,numnpp,numnp,pvtu)!Diffusive term
            call vtkoutr2(hr(np(58)),10,1,17,17,1,numnpp,numnp,pvtu)!Source term
            call vtkoutr2(hr(np(58)),27,1,18,18,1,numnpp,numnp,pvtu)!Mechanical Work Rate
            call vtkoutr2(hr(np(58)),17,1,19,19,1,numnpp,numnp,pvtu)!Elastic free energy
            call vtkoutr2(hr(np(58)),18,1,20,20,1,numnpp,numnp,pvtu)!PF Diffusive term
            call vtkoutr2(hr(np(58)),35,1,21,21,1,numnpp,numnp,pvtu)!Int var FE
            call vtkoutr2(hr(np(58)),24,1,22,22,1,numnpp,numnp,pvtu)!Fracture energy
         endif
         if (np(42).ne.0) then
            call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu)!Velocities
            call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu)!Accelerations
         endif

      !1D Viscoelastic regularized damage element(43) {{{2
      elseif(el.eq.43) then 
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),28,1,2,2,1,numnpp,1,ndf,numnp,pvtu) !Damage

         if (np(58).ne.0) then
           call vtkoutr2(hr(np(58)),8,1,2,2,1,numnpp,numnp,pvtu) !Total Strain
           call vtkoutr2(hr(np(58)),5,1,3,3,1,numnpp,numnp,pvtu) !Stresses
           call vtkoutr2(hr(np(58)),95,1,4,4,1,numnpp,numnp,pvtu)!Dissipated Energy Rate
           call vtkoutr2(hr(np(58)),10,1,5,5,1,numnpp,numnp,pvtu)!Source term in Damage Eqn.
         endif

         !Rate Quantities
         if (np(42) .ne. 0) then
           call vtkoutr3(hr(np(42)),2,3,1,1,1,numnpp,1,ndf,numnp,pvtu) ! Velocities (Displacement rate)
           call vtkoutr3(hr(np(42)),3,3,1,1,1,numnpp,2,ndf,numnp,pvtu) ! Accelerations
           call vtkoutr3(hr(np(42)),92,1,2,2,1,numnpp,1,ndf,numnp,pvtu)! Damage rate (Velocity)
           call vtkoutr3(hr(np(42)),93,1,2,2,1,numnpp,2,ndf,numnp,pvtu)! Damage Acceleration
         endif  !2}}}
      !1D Viscoelastic with damage history variable element(16) {{{2
      elseif(el.eq.16) then 
         call vtkoutr3(hr(np(40)),1,3,1,1,1,numnpp,1,ndf,numnp,pvtu) !Displacements

         if (np(58).ne.0) then
           call vtkoutr2(hr(np(58)),8,1,2,2,1,numnpp,numnp,pvtu) !Total Strain
           call vtkoutr2(hr(np(58)),5,1,3,3,1,numnpp,numnp,pvtu) !Stresses
           call vtkoutr2(hr(np(58)),28,1,4,4,1,numnpp,numnp,pvtu) !Damage
         endif !2}}}
      !2D Viscoelastic with regularized visco-damage element (51) {{{2
      elseif(el.eq.51) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Displacements
         call vtkoutr3(hr(np(40)),28,1,3,3,1,numnpp,1,ndf,numnp,pvtu) !Damage
         if (np(58).ne.0) then
           call vtkoutr2(hr(np(58)),8,6,2,5,4,numnpp,numnp,pvtu) !Total Strain
           call vtkoutr2(hr(np(58)),5,6,6,9,4,numnpp,numnp,pvtu) !Stresses
           call vtkoutr2(hr(np(58)),7,1,10,10,1,numnpp,numnp,pvtu) !von Mises stress
           call vtkoutr2(hr(np(58)),34,6,11,14,4,numnpp,numnp,pvtu) !Strain Rate
           call vtkoutr2(hr(np(58)),82,6,15,18,4,numnpp,numnp,pvtu) !Deviatoric Stress
           call vtkoutr2(hr(np(58)),83,6,19,21,3,numnpp,numnp,pvtu) !Volumetric Stress
           call vtkoutr2(hr(np(58)),10,1,22,22,1,numnpp,numnp,pvtu) !Damage Source term
         endif
          !Rate Quantities
         if (np(42) .ne. 0) then
           call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu) ! Velocities (Displacement rate)
           call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu) ! Accelerations
           call vtkoutr3(hr(np(42)),92,1,3,3,1,numnpp,1,ndf,numnp,pvtu)! Damage rate (Velocity)
           call vtkoutr3(hr(np(42)),93,1,3,3,1,numnpp,2,ndf,numnp,pvtu)! Damage Acceleration
         endif  !2}}}
       !2D Viscoelastic with regularized Chi element (53) {{{2
      elseif(el.eq.19) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Displacements
         !call vtkoutr3(hr(np(40)),96,1,3,3,1,numnpp,1,ndf,numnp,pvtu)!Chi
         if (np(58).ne.0) then
           !call vtkoutr2(hr(np(58)),8,6,2,5,4,numnpp,numnp,pvtu) !Total Strain
           call vtkoutr2(hr(np(58)),5,6,6,9,4,numnpp,numnp,pvtu) !Stresses
           !call vtkoutr2(hr(np(58)),7,1,10,10,1,numnpp,numnp,pvtu) !von Mises stress
           !call vtkoutr2(hr(np(58)),34,6,11,14,4,numnpp,numnp,pvtu) !Strain Rate
           !call vtkoutr2(hr(np(58)),82,6,15,18,4,numnpp,numnp,pvtu) !Deviatoric Stress
           !call vtkoutr2(hr(np(58)),83,6,19,21,3,numnpp,numnp,pvtu) !Volumetric Stress
           call vtkoutr2(hr(np(58)),28,1,22,22,1,numnpp,numnp,pvtu) !Damage 
           !call vtkoutr2(hr(np(58)),17,1,23,23,1,numnpp,numnp,pvtu) !El.Helmholtz Free Energy Rate , PSI
           !call vtkoutr2(hr(np(58)),95,1,24,24,1,numnpp,numnp,pvtu) !Dissipated Energy Rate, \mathcal{D} 
           !call vtkoutr2(hr(np(58)),111,1,25,25,1,numnpp,numnp,pvtu) !Damage Dissipated Energy Rate, \mathcal{D}^D 
           !call vtkoutr2(hr(np(58)),112,1,26,26,1,numnpp,numnp,pvtu) !viscous Dissipated Energy Rate, \mathcal{D}^{visc} 
           call vtkoutr2(hr(np(58)),113,3,27,28,2,numnpp,numnp,pvtu) ! Reaction forces
         endif
          !Rate Quantities
         if (np(42) .ne. 0) then
           !call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu) ! Velocities (Displacement rate)
           !call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu) ! Accelerations
           !call vtkoutr3(hr(np(42)),97,1,3,3,1,numnpp,1,ndf,numnp,pvtu)! Chi rate (Velocity)
           !call vtkoutr3(hr(np(42)),98,1,3,3,1,numnpp,2,ndf,numnp,pvtu)! Chi Acceleration
         endif  !2}}}
      !2D Visco element (46) {{{2        
      elseif(el.eq.46) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Displacements
         if (np(58).ne.0) then
           call vtkoutr2(hr(np(58)),8,6,2,5,4,numnpp,numnp,pvtu) !Total Strain
           call vtkoutr2(hr(np(58)),5,6,6,9,4,numnpp,numnp,pvtu) !Stresses
           call vtkoutr2(hr(np(58)),7,1,10,10,1,numnpp,numnp,pvtu) !von Mises stress
           call vtkoutr2(hr(np(58)),28,1,11,11,1,numnpp,numnp,pvtu) !Damage
           call vtkoutr2(hr(np(58)),34,6,12,15,4,numnpp,numnp,pvtu) !Strain Rate
           call vtkoutr2(hr(np(58)),82,6,16,19,4,numnpp,numnp,pvtu) !Deviatoric Stress
           call vtkoutr2(hr(np(58)),83,6,20,22,3,numnpp,numnp,pvtu) !Volumetric Stress
          
         endif!2}}}
      elseif(el.eq.23) then 
         call vtkoutr3(hr(np(40)),1,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Displacements
         if (np(58).ne.0) then
           call vtkoutr2(hr(np(58)),177,3,2,3,2,numnpp,numnp,pvtu) !Separations 
           call vtkoutr2(hr(np(58)),178,3,4,5,2,numnpp,numnp,pvtu) !Tractions
         endif
      else ! FEAP element{{{2
         call vtkoutr3(hr(np(40)),1,3,1,3,3,numnpp,1,ndf,numnp,pvtu) !Displacements
          
         if (np(58).ne.0) then
            call vtkoutr2(hr(np(58)),5,6,2,5,4,numnpp,numnp,pvtu) !Stresses
            !call VolDevStrs(hr(np(58)),82,6,2,5,4,numnpp,numnp,pvtu)!Deviatoric Stress
            !call VolDevStrs(hr(np(58)),83,6,2,5,4,numnpp,numnp,pvtu)!Volumetric Stress
         endif
         
         if (np(42).ne.0) then
            !call vtkoutr3(hr(np(42)),2,3,1,2,2,numnpp,1,ndf,numnp,pvtu) !Velocities
            !call vtkoutr3(hr(np(42)),3,3,1,2,2,numnpp,2,ndf,numnp,pvtu) !Accelerations
         endif
      !2}}}
      endif
      end
      !1}}}

c     --- Write to VTU functions ---
      !subroutine vtkoutr3{{{1
      subroutine vtkoutr3(array,ann,ncomp,nb,ne,nw,numnpp,n3,ndf,numnp,
     &     pvtu)
      
c-----[----------------------------------------------------------------]
c     Modification log                                Date (dd/mm/year)
c     Original version                                        /  /    
c-----[----------------------------------------------------------------]
c     Purpose:  Export data in Paraview Fromat

c     Inputs:
c     array     - Array to export 
c     ann       - Index for the Name
c     ncomp     - Number of components of the data 
c     nb        - Index of First Component to write
c     ne        - Index of Last Component to write
c     nw        - Number of computed components (=ne-nb+1)
c     numnpp    - number of nodes for plotting
c     n3        - 
c     ndf       - number of dofs
c     numnp     - number of nodes per element
c-----[----------------------------------------------------------------]
      
      implicit none

      include  'prjnam.h'
      include  "comblk.h"
      include  "pointer.h"
      include  'pfeapb.h'


      integer    i,ii,nw,ncomp,npad,n3,numnpp,j,ann,ndf,numnp,ne,nb,ct
      integer    pntasks,prank
      real*8     array(ndf,numnp,*),pr,pra(ncomp),crval
      character  names(numNames)*30,aname*30
      logical    pvtu

      call getNames(names)

      if (pfeap_on)then
          call getParData(pntasks,prank)
      else
          pntasks = 1
          prank   = 0
      endif

      aname = names(ann)
      j = index(aname,'  ')-1
      if(pvtu) then
         write(98,1032) 'Float64',aname(1:j),ncomp
      else
         npad = ncomp - nw
         write(99,1030) 'Float64',aname(1:j),ncomp ! Start array
         do i = 1,numnpp
            ct = 1
            pra = 0.d0
            do ii = nb,ne
               if(pntasks.gt.1 .and. up(112).ne.0) then
                  crval = array(ii,mr(up(111)-1 + i),n3)
!                  crval = array(ii,i,n3)
               else
                  crval = array(ii,i,n3)
               endif
               if (dabs(crval).gt. 1.0E-20) then
                  pra(ct) = crval
               endif
               ct      = ct+1
            end do
            write(99,*) pra
         end do
         write(99,1031)         ! Close array
      endif

 1030 format('<DataArray type="',a,'" 
     *Name="',a,'"
     *NumberOfComponents="',i2,'" format="ascii">')
 1031 format('</DataArray>')
 1032 format('<PDataArray type="',a,'" 
     *Name="',a,'" 
     *NumberOfComponents="',i2,'" format="ascii"/>')
 5000 format(1pe15.5,' ',$)
      end ! vtkoutr3
      !1}}}
      !subroutine vtkoutr2{{{1
      subroutine vtkoutr2(array,ann,ncomp,nb,ne,nw,numnpp,dim1,pvtu)

c-----[----------------------------------------------------------------]
c     Modification log                                Date (dd/mm/year)
c     Original version                                        /  /    
c-----[----------------------------------------------------------------]
c     Purpose:  Export data in Paraview Fromat

c     Inputs:
c     array     - Array to export 
c     ann       - Index for the Name
c     ncomp     - Number of components of the data 
c     nb        - Index of First Component to write
c     ne        - Index of Last Component to write
c     nw        - Number of computed components (=ne-nb+1)
c     numnpp    - number of nodes for plotting
c     dim1      - Size of first dimension of the array (typically numnp)
c-----[----------------------------------------------------------------]

      implicit none

      include  'prjnam.h'
      include  "comblk.h"
      include  "pointer.h"
      include  'pfeapb.h'
      include  'cdata.h'

      integer    i,ii,nw,ncomp,npad,n3,numnpp,j,ann,ndf,dim1,ne,nb,ct
      integer    pntasks,prank
      real*8     array(dim1,*),pra(ncomp),crval
      character  names(numNames)*30,aname*30
      logical    pvtu

      call getNames(names)

      if (pfeap_on)then
          call getParData(pntasks,prank)
      else
          pntasks = 1
          prank   = 0
      endif
      
      aname = names(ann)
      j = index(aname,'  ')-1

      if(pvtu) then
         write(98,1032) 'Float64',aname(1:j),ncomp
      else
         !write(*,*) 'j=',j
         npad = ncomp - nw
         write(99,1030) 'Float64',aname(1:j),ncomp ! Start array
         do i = 1,numnpp
            ct = 1
            pra = 0.d0
            do ii = nb,ne
               if(pntasks.gt.1 .and. up(112).ne.0) then
                  crval = array(mr(up(111)-1 + i),ii)
!                  crval = array(i,ii)
               else
                  crval = array(i,ii)
               endif
               if (dabs(crval) .gt. 1.0E-20)then
                  pra(ct) = crval
               endif
               ct = ct+1
            end do
            write(99,*) pra
         end do
         write(99,1031)         ! Close array
      endif

 1030 format('<DataArray type="',a,'" 
     *Name="',a,'"
     *NumberOfComponents="',i2,'" format="ascii">')
 1031 format('</DataArray>')
 1032 format('<PDataArray type="',a,'" 
     *Name="',a,'" 
     *NumberOfComponents="',i2,'" format="ascii"/>')
 5000 format(1pe15.5,' ',$)
      end !vtkoutr2
      !1}}}
      !subroutine export58{{{1
      subroutine export58(ann,ncomp,nb,ne,pvtu)

c     Inputs:
c     ann       - Index for the Name
c     ncomp     - Number of components of the data 
c     nb        - Index of First Component to write
c     ne        - Index of Last Component to write

      implicit none

      include  "comblk.h"
      include  "pointer.h"
      include  "cdata.h"

      integer    ncomp,numnpp,ann,ne,nb
      logical    pvtu

      numnpp = mr(up(66))         !Number of plotting nodes
      call vtkoutr2(hr(np(58)),ann,ncomp,nb+1,ne+1,ne-nb+1,
     &               numnpp,numnp,pvtu)

      end 
      !1}}}

c     --- Automated Export Functions ---
      !subroutine setupPData{{{1
      subroutine setupPData(text,projData,projNums,elt)

      implicit none

      include  'prjnam.h'
      include  'cdat1.h'
      include  'sdata.h'
      include  'cdata.h'

      character names(numNames)*30,text*30,cname*30
      integer   arri,i,projData(numNames,numWarrs,*),ncomp,nw,nbc,ne
      integer   projNums(numWArrs,2),prji,ln,lt,lc,elt,locf,nb,nbel
      logical   found,pcomp
      integer   ix(ndf,1)
      real*8    d(ndd,1),ul(ndf,nen,1),xl(ndm,nen),tl,p(nst),s(nst,nst)
     
      call getNames(names)

      found = .false.
      do i = 1, numNames !Scan all names
          cname = names(i)
          ln    = len_trim(cname)
          lt    = len_trim(text)
          lc    = min(ln,lt)
          if (lc.ne.0 .and.pcomp(text,cname,30) .and. .not. found)then
             
              call getncomp(ncomp,i)  ! get name of the ith field 
              ix(1,1) = i
              call elmlib(d,ul,xl,ix,tl,s,p,
     &                    ndf,ndm,nst,elt,61,8)

              nw   = int(p(1)) ! components of filds, 
              locf = int(p(2)) ! location flag,
              nbel = int(p(3)) ! nstart 

              if (locf.ne.0)then   !Found text in Element
                  arri = projNums(locf,1) + 1
                  prji = projNums(locf,2)
                  if (locf.le.3)then
                      nb = nbel
                      ne = nbel + nw - 1
                  else
                      nb = prji + 1
                      ne = prji + nw
                  endif

                  projData(arri,locf,1) = i         !index for name
                  projData(arri,locf,2) = ncomp     !number of components
                  projData(arri,locf,3) = nb        !index of first component to write
                  projData(arri,locf,4) = ne        !index of last component to write
                  projData(arri,locf,5) = nw        !number of computed components

                  projNums(locf,1) = arri
                  projNums(locf,2) = prji + nw
              else
                  print *, text,' not present on element',elt
                  print *
              endif
              found = .true.
          elseif (lc.ne.0 .and.pcomp(text,cname,30) .and. found)then
              print *, text,' matches multiple arrays, including '
     &              ,names(i)
              print *,names(projData(arri,locf,1)), ' has been assumed'
              print *
          endif
      enddo

      if (.not.found) then
         print *, "array ",text," not found"
         print *
      endif

      end
      !1}}}
      !subroutine WritePointAuto{{{1
      subroutine WritePointAuto(projData,projNums,numnpp,numnp,ndf,pvtu)

      implicit none

      include  'prjnam.h'
      include  'pointer.h'
      include  'comblk.h'
      include  'strnum.h'

      integer   i,projData(numNames,numWArrs,*),ann,ncomp,nb,ne,nw
      integer   projNums(numWArrs,2),prji,numnpp,numnp,n3,ndf,j,kn
      logical   fail,pcomp,pvtu

c     ann       - Index for the Name
c     ncomp     - Number of components of the data 
c     nb        - Index of First Component to write
c     ne        - Index of Last Component to write
c     nw        - Number of computed components (=ne-nb+1)
c     locf      - flag for location of global array

      do j = 1,numWArrs !loop on array types
          if (projNums(j,1).gt.0)then
              do i = 1, projNums(j,1) !loop on number of fields
                  ann   = projData(i,j,1)
                  ncomp = projData(i,j,2)
                  nb    = projData(i,j,3) 
                  ne    = projData(i,j,4)
                  nw    = projData(i,j,5)
                  if (j.eq.1)then                        !Disp quatities
                      n3 = 1
                      call vtkoutr3(hr(np(40)),ann,ncomp,nb,ne,nw
     &                            ,numnpp,n3,ndf,numnp,pvtu)
c                      call vtkoutr3(hr(np(40)),4,1,3,3,1,numnpp,1,
c     &                             ndf,numnp,pvtu)!Temperature
                  elseif (j.eq.2 .and. np(40).ne. 0)then !Vel Quantities
                      n3 = 1
                      call vtkoutr3(hr(np(42)),ann,ncomp,nb,ne,nw
     &                            ,numnpp,n3,ndf,numnp,pvtu)
                  elseif (j.eq.3 .and. np(40).ne. 0)then !Accel Quatities
                      n3 = 2
                      call vtkoutr3(hr(np(42)),ann,ncomp,nb,ne,nw
     &                            ,numnpp,n3,ndf,numnp,pvtu)
                  elseif (j.eq.4 .and. np(58).ne. 0) then !Projected Quantities 
                      call vtkoutr2(hr(np(58)),ann,ncomp,nb+1,ne+1,nw
     &                         ,numnpp,numnm,pvtu)
                  elseif (j.eq.5 .and. up(3).ne. 0) then !Integrated Displacements
                     kn = mr(up(3))
                     call vtkoutr2(hr(up(kn)),ann,ncomp,nb,ne,nw
     &                         ,numnpp,numnpp,pvtu)
                  elseif (j.eq.6 .and. up(74).ne. 0) then !Global Eigenvectors
                      call vtkoutr3(hr(np(74)),ann,ncomp,nb,ne,nw
     &                            ,numnpp,1,ndf,numnp,pvtu)
                  endif
              enddo
          endif
      enddo

      end
      !1}}}
      !subroutine getnpstr{{{1
      subroutine getnpstr(projNums,npstr)

      implicit none

      include  'prjnam.h'

      integer npstr,projNums(numWArrs,2)

      npstr = projNums(4,2) + 1

      end
      !1}}}

c     --- Auxiliary Functions ---
      !subroutine addfolder {{{1
      subroutine addfolder(fname,finp)
      !128 size is harcoded
      implicit none
      
      logical    dir_e
      integer    n,nn
      integer    pos_dash, pos_inp, pos_fold
      character fname*128,foldn*128,finp*128
      
      foldn(1:128) = ' '
      
      !Get Position of last character before '_'
      pos_dash = scan(finp, '_', .true.) - 1
      if (pos_dash .le. 0) then
         pos_inp = verify(finp, ' ', .true.)
      else
         pos_inp = pos_dash
      endif
      
      !Define Folder Name
      foldn(1:pos_inp-1)      = finp(2:pos_inp)
      pos_fold                = pos_inp+3
      foldn(pos_inp:pos_fold) = '_OUT'
      
      !Test if Folder exists and create it    
      inquire(file='./'//foldn(1:pos_fold)//'/.', exist=dir_e)
      if ( dir_e ) then
              !write(*,*) "dir exists!"
      else !Calls system command mkdir
        call system('mkdir '//foldn(1:pos_fold))
      endif
      
      !Add Folder to the filename 
      fname(pos_fold+2:128) = fname(1:128-pos_fold-1)
      fname(1:pos_fold+1) = foldn(1:pos_fold)//'/'
        
      end
c     1}}}  
c     subroutine getpordering(ltog,gtol,numnpp) {{{
      subroutine getpordering(ltog,gtol,numnpp)
      
      implicit none
      
      include "cdata.h"
      include "cdat1.h"
      include "sdata.h"
      include "comblk.h"
      include "pointer.h"
      include "eldata.h"
      include "counts.h"
      
      integer  ltog(numnpp), gtol(numnp), uelmn(99,2), numnpp
      integer  i, ii, mat, elt, nenc, node, cntr
      
      call pzeroi(ltog,numnpp)
      call pzeroi(gtol,numnp)
      call getelnums(uelmn)
      
      cntr = 0
      do i = 1,numel ! number of element for each processor
         mat = mr(np(33) + nen1*(i-1) + nen1-1)
         elt = mr(np(32) + nie*(mat-1) + nie-2)         
         if(elt.gt.0) then
            nenc = uelmn(elt,1)
         else
            nenc = nen
         endif
         do ii = 1,nenc
            node = mr(np(33)-1 + (i-1)*nen1 + ii)
            if(node.ne.0 .and. gtol(node).eq.0) then
               cntr = cntr + 1
               gtol(node) = cntr
               ltog(cntr) = node
            endif
!            write(*,*) 'numel,i,gtol,ltog,node      ',
!     &                  numel,i,gtol(node),ltog(cntr),node

         enddo
         
      enddo
      
      
!      do i = 1,numnpp ! number of element for each processor
! 
!!        write(*,*) 'numnp,numnpp',numnp,numnpp
!        node = mr(np(244)-1 + i)
!	    gtol(node) = i
!        ltog(i)    = node
!!        write(*,*) 'numel,i,gtol,ltog,node      ',
!!     &              numel,i,gtol(node),ltog(i),node
!         
!      enddo
      
      end


      subroutine sum_multiple_material_projected_quantities(ima,
     &                                                prj_array)
      implicit none
      
      include 'pdata3.h'
      include 'cdata.h'
      include 'strnum.h'
      
      
      integer ima(2,numnp), i, j, row
      real*8 prj_array(numnm, npstr), temp(npstr)
      !call mprint(prj_array,numnm,npstr,numnm,'58')
      !write(*,*)'numnm',numnm,npstr
      row = 1
      do i = 1,numnp
        if (ima(1,i).gt.1) then
          temp = 0.d0
          do j = 1,ima(1,i)
            temp = temp + prj_array(row + j - 1, :)
          enddo
          temp = temp / float(ima(1,i))
        else
            temp = prj_array(row, :)
        endif
        prj_array(i, :) = temp
        row = row + ima(1,i)
      enddo
      
      end


      !subroutine VolDevStrs{{{1
      subroutine VolDevStrs(array,ann,ncomp,nb,ne,nw,numnpp,dim1,pvtu)

c-----[----------------------------------------------------------------]
c     Modification log                                Date (dd/mm/year)
c     Original version                                        /  /    
c-----[----------------------------------------------------------------]
c     Purpose:  Export data in Paraview Fromat of Deviatoric and Volum-
c               etric stresses from the stress array of FEAP elements.

c     Inputs:
c     array     - Array to export (Stress array of FEAP elements) 
c     ann       - Index for the Name: 82 -> Deviatoric, 83 -> Volumetric
c     ncomp     - Number of components of the stress data 
c     nb        - Index of First Stress Component to write
c     ne        - Index of Last Stress Component to write
c     nw        - Number of computed components (=ne-nb+1)
c     numnpp    - number of nodes for plotting
c     dim1      - Size of first dimension of the array (typically numnp)
c-----[----------------------------------------------------------------]

      implicit none

      include  'prjnam.h'
      include  "comblk.h"
      include  "pointer.h"
      include  'pfeapb.h'

      integer    i,ii,nw,ncomp,npad,n3,numnpp,j,ann,ndf,dim1,ne,nb,ct,k
      integer    pntasks,prank
      real*8     array(dim1,*),pra(ncomp),crval,sig_kk,sig_vol(3)
      character  names(numNames)*30,aname*30
      logical    pvtu

      call getNames(names)

      if (pfeap_on)then
          call getParData(pntasks,prank)
      else
          pntasks = 1
          prank   = 0
      endif

      aname = names(ann)
      j = index(aname,'  ')-1
      if(pvtu) then
         write(98,1032) 'Float64',aname(1:j),ncomp
      else
         !write(*,*) 'j=',j
         npad = ncomp - nw
         write(99,1030) 'Float64',aname(1:j),ncomp ! Start array
         do i = 1,numnpp
            ct = 1
            pra = 0.d0
            sig_kk = 0.d0
            sig_vol = 0.d0
            do ii = nb,ne
               if(pntasks.gt.1 .and. up(112).ne.0) then
                  crval = array(mr(up(111)-1 + i),ii)
               else
                  crval = array(i,ii)
               endif
               if (dabs(crval) .gt. 1.0E-20)then
                  pra(ct) = crval
               endif
               ct = ct+1
            end do


            ! Compute Volumetric Stress
            sig_kk = (pra(1) + pra(2) + pra(3))
            do k = 1,3
               sig_vol(k) = sig_kk/3.0
            end do ! i

            if (ann.eq.82)then   ! Compute Deviatoric Stress
               do k = 1,3
                  pra(k) = pra(k) - sig_vol(k)
               end do ! k
               do k = 4,nw
                  pra(k) = pra(k)
               end do ! k
            elseif(ann.eq.83)then ! Compute Volumetric Stress
               do k=1,3
                  pra(k) = sig_vol(k)
               end do !k

            endif
            write(99,*) pra
         end do
         write(99,1031)         ! Close array
      endif

 1030 format('<DataArray type="',a,'" 
     *Name="',a,'"
     *NumberOfComponents="',i2,'" format="ascii">')
 1031 format('</DataArray>')
 1032 format('<PDataArray type="',a,'" 
     *Name="',a,'" 
     *NumberOfComponents="',i2,'" format="ascii"/>')
 5000 format(1pe15.5,' ',$)
      end !vtkoutr2
      !1}}}
