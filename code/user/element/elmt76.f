!$Id:$
      subroutine elmt76(d,ul,xl,ix,tl,s,p,ndf,ndm,nst,isw,pk)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
!     Purpose:

!     Inputs:
!       d(*)          - Material set parameters
!       ul(ndf,nen,*) - Solution parameters
!       xl(ndm,nen)   - Element nodal coordinates
!       ix(nen1)      - Element global node numbers
!       tl(nen)       - Element vector (e.g., nodal temperatures)
!       ndf           - Maximum no dof's/node
!       ndm           - Mesh spatial dimension
!       nst           - Element matrix/vector dimension
!       isw           - Switch parameter (set by input commands)
!       pk            - pointer for subroutine

!     Outputs:
!       s(nst,nst)  - Element matrix (stiffness, mass, etc.)
!       p(nst)      - Element vector (residual, lump mass, etc.)
!-----[--.----+----.----+----.-----------------------------------------]
      implicit none

      include 'eldata.h'
      include 'cdata.h'                    ! numnp,numel,nummat,nen
      include 'pointer.h'
      include 'comblk.h'
      include 'strnum.h'                   ! istv,iste,istp,istc
      integer (kind=4) ::  pk              !pointerfor subroutine
      integer (kind=4) ::  ndf             ! Max DOF's/node
      integer (kind=4) ::  ndm             ! Mesh spatial dimention
      integer (kind=4) ::  nst             ! Matrix 's' dimension
      integer (kind=4) ::  isw             ! Switch for solution steps
      integer (kind=4) ::  ix(*)           ! Element global nodes
      real    (kind=8) ::  d(*)            ! Material set parameters
      real    (kind=8) ::  ul(ndf,nen,*)       ! Local nodal solutions
      real    (kind=8) ::  xl(ndm,*)       ! Local nodal coordinates
      real    (kind=8) ::  tl(*)           ! Local nodal load array
      real    (kind=8) ::  s(nst,*)        ! Element matrix
      real    (kind=8) ::  p(nst)          ! Element vector
      !write(*,*)'elmt04=isw',isw,nst
      if(isw.lt.0) then
!       utx(1) = 'Name_U_Want'             ! 15 character naming option
!                                          ! Access: MATErial "ma"
!                                          ! Name_U_Want (instead of USER 1)
      elseif(isw.eq.0) then                ! WARNING: Use previous stmt
         write(*,2000)                     ! DELETE THIS when adding
!                                          ! the user module.
      elseif(isw.eq.1) then                ! Input material set data
!       pstyp = ???                        ! Sets plot dimension (1,2,3)
         istv = 31                         !Maxinum number fo element projection
      elseif(isw.eq.2) then                ! Check input data
      elseif(isw.eq.3) then                ! Compute tangent s(nst,snt)
      elseif(isw.eq.6) then                ! Compute residual p(nen*ndf)
      elseif(isw.eq.4) then                ! Output element data
      elseif(isw.eq.5) then                ! Compute mass matrix
      elseif(isw.eq.8) then                ! Compute projection
      elseif(isw.eq.14) then               ! Initialize history variables
      elseif(isw.eq.61) then               ! obtain displacement stress
      endif

!     Formats

2000  format('    Elmt  76: *WARNING* Dummy subroutine called')

      end subroutine elmt76
