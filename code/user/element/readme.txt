Add use element
1. open dir user/element  add elmtXX.f
2. open dir include 
3. In the elname.h add num_user_el = nu_user_el + 1
   for example:  before modification  num_user_e =99
                 after modification num_user_el=100
4 Open dir program 
5. In the subroutine elmlib.f 
   at User elment routines 
   add
   elseif (jel.eq.XX) then
      call elmtXX(d,u,x,ix,t,s,p,i,j,k,isw)
   where XX is the number of element
6. open dir paraview
   open eloutinfo.f
   In the getelnums(uelmn)
   add
   uelmn(num_user_el,1:2) = (/XX,XX:/)

!extend use element
1. modified：elname.h pcontr.f elmlib.f umacr1.f pmatin.f pmacr3.f 
2. eloutinfo.f->uelmn
