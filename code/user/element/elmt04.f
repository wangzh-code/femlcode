!$Id:$
      subroutine elmt04(d,ul,xl,ix,tl,s,p,ndf,ndm,nst,isw,pk)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2017: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
!     Purpose:

!     Inputs:
!       d(*)          - Material set parameters
!       ul(ndf,nen,*) - Solution parameters
!       xl(ndm,nen)   - Element nodal coordinates
!       ix(nen1)      - Element global node numbers
!       tl(nen)       - Element vector (e.g., nodal temperatures)
!       ndf           - Maximum no dof's/node
!       ndm           - Mesh spatial dimension
!       nst           - Element matrix/vector dimension
!       isw           - Switch parameter (set by input commands)
!       pk            - pointer for subroutine

!     Outputs:
!       s(nst,nst)  - Element matrix (stiffness, mass, etc.)
!       p(nst)      - Element vector (residual, lump mass, etc.)
!-----[--.----+----.----+----.-----------------------------------------]
      implicit none

      include 'eldata.h'
      include 'cdata.h'                    ! numnp,numel,nummat,nen
      include 'pointer.h'
      include 'comblk.h'
      include 'strnum.h'                   ! istv,iste,istp,istc
      integer (kind=4) ::  pk              !pointerfor subroutine
      integer (kind=4) ::  ndf             ! Max DOF's/node
      integer (kind=4) ::  ndm             ! Mesh spatial dimention
      integer (kind=4) ::  nst             ! Matrix 's' dimension
      integer (kind=4) ::  isw             ! Switch for solution steps
      integer (kind=4) ::  ix(*)           ! Element global nodes
      real    (kind=8) ::  d(*)            ! Material set parameters
      real    (kind=8) ::  ul(ndf,nen,*)       ! Local nodal solutions
      real    (kind=8) ::  xl(ndm,*)       ! Local nodal coordinates
      real    (kind=8) ::  tl(*)           ! Local nodal load array
      real    (kind=8) ::  s(nst,*)        ! Element matrix
      real    (kind=8) ::  p(nst)          ! Element vector
      !write(*,*)'elmt04=isw',isw,nst
      if(isw.lt.0) then
!       utx(1) = 'Name_U_Want'             ! 15 character naming option
!                                          ! Access: MATErial "ma"
!                                          !   Name_U_Want (instead of USER 1)
!     elseif(isw.eq.0) then
      elseif(isw.eq.0) then                ! WARNING: Use previous stmt
         write(*,2000)                     ! DELETE THIS when adding
!                                          ! the user module.
      elseif(isw.eq.1) then                ! Input material set data
!       pstyp = ???                        ! Sets plot dimension (1,2,3)
         istv = 31                ! Maxinum number fo element projection
      elseif(isw.eq.2) then                ! Check input data
      elseif(isw.eq.3 .or.isw.eq.6) then   ! Compute residual/tangent
         !Compute residual p(ndf*nen), tangents(nst,nst)
         !write(*,*) 'ndf,ndm,nst',ndf,ndm,nst
         !write(*,*) 'ix',ix(1),ix(2),ix(3),ix(4)
         !write(*,*) 'xl',xl(1,1),xl(2,1)
         call ps04(d,ul,xl,s,p,ndf,ndm,nst,ix,isw)
      elseif(isw.eq.4) then                ! Output element data
      elseif(isw.eq.5) then                ! Compute mass matrix
      elseif(isw.eq.8) then                ! Compute projection
         call pro04(ul,xl,s,p,ndf,ndm,ix,mr(up(11)),mr(up(12)))
      elseif(isw.eq.14) then               ! Initialize history variables
      elseif(isw.eq.61) then               ! obtain displacement stress
         call wget04(ix(1),p)
      endif

!     Formats

2000  format('    Elmt  4: *WARNING* Dummy subroutine called')

      end subroutine elmt04
      ! Comppute tangent / residual
      subroutine ps04(d,ul,xl,s,p,ndf,ndm,nst,ix,isw)
      implicit none
      include 'cdata.h'
      include 'eltran.h'
      integer (kind=4) ::  ndf             ! Max DOF's/node
      integer (kind=4) ::  ndm             ! Mesh spatial dimention
      integer (kind=4) ::  nst             ! Matrix 's' dimension
      integer (kind=4) ::  isw             ! Switch for solution steps
      integer (kind=4) ::  ix(*)           ! Element global nodes
      real    (kind=8) ::  d(*)            ! Material set parameters
      real    (kind=8) ::  ul(ndf,nen,*)   ! Local nodal solutions
      real    (kind=8) ::  xl(ndm,*)       ! Local nodal coordinates
      real    (kind=8) ::  s(nst,*)        ! Element matrix
      real    (kind=8) ::  p(nst)          ! Element vector
      !----------------------------------------------------!
      integer (kind=4) :: lgp, lint
      real    (kind=8) :: gip(3,4)
      ! lgp - number of points in each direction
      ! lint - total number of points
      ! gip(3,4) -Gauss integral point and weights
      ! gip(1,*)-x gip(2,*) -y gip(3,*)-weights
      integer (kind=4) :: igp,j,i
      !igp  loop for Gauss integral points
      !j,i    loop for
      real   (kind=8) :: shp(3,4),xx,yy,xsj
      ! shp(1,i) dN_i/dx shp(2,i) dN_i/dy  shp(3,i) N_i
      ! xx yy globle coordinate, xsj Jacobian
      real  (kind=8):: dstraindu(3,8),strain(3),stress(3)
      ! dstraindu -B matrix
      ! strain    -
      real  (kind=8) :: matc(3,3)
      !matc -C matrix material properties
      real  (kind=8) :: tranb(8,3), matcdstraindu(3,8),tempk(8,8)
      !----------------------------------------------------!
      !-----Gauss integral points and weights-------!
      !p = 0.0d0
      !s = 0.0d0
      lgp = 2
      call int2d(lgp,lint,gip)
      !-----Gauss integral points and weights-------!
      !write(*,*) 'lgp,lint,gip',lint,gip(1,2),gip(2,2),gip(3,2)
      do igp=1,lint     !lint
         call shp2d(gip(1,igp),xl,shp,xsj,ndm,4,ix,.false.)
         xsj = xsj*gip(3,igp)  !Jacobian*weights
         !write(*,*) 'xsj=',xsj,shp(1,1)
         !----initialize ------------------------!
         xx=0.0d0
         yy=0.0d0
         strain = 0.0d0
         stress =0.0d0
         !---------------------------------------!
         do j =1,4  ! 4 denots the number of shap function
            xx = xx + shp(3,j)*xl(1,j) !Compute globle coordniate x
            yy = yy + shp(3,j)*xl(2,j) !Compute globle coordinate y
            dstraindu(1,1+2*(j-1))=shp(1,j) !Compute B matrix
            dstraindu(1,2+2*(j-1))=0.0d0
            dstraindu(2,1+2*(j-1))=0.0d0
            dstraindu(2,2+2*(j-1))=shp(2,j)
            dstraindu(3,1+2*(j-1))=shp(2,j)
            dstraindu(3,2+2*(j-1))=shp(1,j)
            !Compute strain
            strain(1)=strain(1) + shp(1,j)*ul(1,j,1)
            strain(2)=strain(2) + shp(2,j)*ul(2,j,1)
            strain(3)=strain(3) + shp(2,j)*ul(1,j,1) +
     &                shp(1,j)*ul(2,j,1)
         enddo
         ! Compute materials properties
         call mat04(matc,3,3,xx,yy)
         !Compute stress(3)
         stress(1) = matc(1,1)*strain(1)+matc(1,2)*strain(2)
         stress(2) = matc(2,1)*strain(1)+matc(2,2)*strain(2)
         stress(3) = matc(3,3)*strain(3)
         !write(*,*)'igp-----igp=',igp
         !write(*,*) 'BB=1', dstraindu(1:1,1:8)
         !write(*,*) 'BB-2',dstraindu(2:2,1:8)
         !write(*,*) 'BB-3',dstraindu(3:3,1:8)
         !Compute residual
         do i=1,4
             p(1+2*(i-1)) =p(1+2*(i-1)) - shp(1,i)*stress(1)
     &                     -shp(2,i)*stress(3)
             p(2+2*(i-1)) =p(2+2*(i-1)) - shp(2,i)*stress(2)
     &                     -shp(1,i)*stress(3)
         enddo
         if(isw.eq.3)then ! Compute tangent
             tranb = 0.0d0
             matcdstraindu =0.0d0
             tempk = 0.0d0
             tranb = transpose(dstraindu)
             matcdstraindu = matmul(matc,dstraindu)
             tempk = matmul(tranb,matcdstraindu)
             !write(*,*)'igp---tempk--tempk--igp',igp
             !write(*,*) 'tempk-1', tempk(1:1,1:8)
             !write(*,*) 'tempk-2', tempk(2:2,1:8)
             !write(*,*) 'tempk-3', tempk(3:3,1:8)
             !write(*,*) '-----jacbain------',xsj,ctan(1)
             do i=1,8
                 do j=1,8
                     s(i,j) = s(i,j)+tempk(i,j)*xsj*ctan(1)
                 enddo !end j
             enddo !endj
         endif
         !call mprint(s,8,8,8,'ss')
         !call mprint(dstraindu,3,8,3,'BB')
         !call mprint(matc,3,3,3,'CC')
         !write(*,*) 'dstraindu=',dstraindu

      enddo !end igp
         !call mprint(s,8,8,8,'s')
         !call mprint(matc,3,3,3,'mc')
         !write(*,*) '---------------s-------------------'
         !write(*,*) 'tempk-1', s(1:1,1:8)
         !write(*,*) 'tempk-2', s(2:2,1:8)
         !write(*,*) 'tempk-3', s(3:3,1:8)
         !write(*,*) 'tempk-4', s(4:4,1:8)
         !write(*,*) 'tempk-5', s(5:5,1:8)
         !write(*,*) 'tempk-6', s(6:6,1:8)
         !write(*,*) 'tempk-7', s(7:7,1:8)
         !write(*,*) 'tempk-8', s(8:8,1:8)
      end
      subroutine mat04(c,row,colum,x,y)
      implicit none
      integer (kind=4) :: row,colum
      real (kind=8) :: x,y,c(row,colum)
      !input:
      !x -coordinate
      !y -coordinate
      !row -Number of rows of the matrix c
      !colum -Number of colum of the matrix c
      !Output
      !c(row,colum) - matrix c row x colum
      !----------------------------------------!
      real (kind=8)  :: e0,v0
      integer (kind=4) :: key !swith for material type
      !e0   -E Young's modulus
      !v0   -v possion's ratio
      c =0.0d0
      key = 2
      if(key.eq.1)then !homogeneou material plane stress
         e0=1000.0
         v0=0.25
         c(1,1) = e0/(1-v0**2)
         c(1,2) = c(1,1)*v0
         c(2,1) = c(1,2)
         c(2,2) = c(1,1)
         c(3,3) = 0.5*e0/(1+v0)
      elseif(key.eq.2)then ! plane strain
         e0=1000.0
         v0=0.25
         !c(1,1) =(1-v0)*e0/((1-2*v0)*(1+v0))
         !c(1,2) = c(1,1)*v0/(1-v0)
         !c(2,1) = c(1,2)
         !c(2,2) = c(1,1)
         !c(3,3) = 0.5*e0/(1+v0)
         c(1,1) =126.0d9
         c(1,2) =53.0d9
         c(2,1) =53.0d9
         c(2,2) =117.0d9
         c(3,3) =35.3d9
      endif
      !----------------------------------------!
      end subroutine mat04

      subroutine wget04(i,ndata)
      implicit none
      include 'prjnam.h'
      include 'strnum.h'
      integer ndat(numNames,3),i
      real*8 ndata(3)
      ! - 0 -> not present
      ! - 1 -> dispalcement in hr(np(40))
      ! - 2 -> rate in hr(np(42))
      ! - 3 -> acceleration in hr(np(42))
      ! - 4 -> projected in hr(np(58))
      ! - 5 and beyond, user array
      ndat=0
      !nw,location flag,nstart (only used if locf = 1,2,or 3{{{2
      ! name of number, components of filds, location flag, nstart
      ndat(1 ,:) = (/2,1,1/)  !'Displacements'
      ndat(5 ,:) = (/4,4,1/)  !'Stesses'
      ndat(8 ,:) = (/4,4,1/)
      ndat(24,:) = (/1,4,1/)
      !---------------------------------------------!
      ndata(1) = real(ndat(i,1))
      ndata(2) = real(ndat(i,2))
      ndata(3) = real(ndat(i,3))
      end subroutine wget04
c------------------------------------------------------------------------------c
      subroutine pro04(ul,xl,s,p,ndf,ndm,ix,projData,projNums)
      !-------------------------------------------------------------!
      ! Purpose:Get projections
      !Input:

      !Output:
      ! p(nen)  -Weights for 'lumped' projection
      ! s(nen,*) -ingergral for variables
      !-------------------------------------------------------------!
      implicit none
      include 'prjnam.h' !numNmaes numWArrs
      ! numNames=200     !Number of array names
      ! numWArrs=6       !Number of types of arrays (4 program + 2 user)
      include 'cdata.h'  ! numnp,numel,numat,nen
      include 'eldata.h' !dm, n_el, ma,mct,iel,nel
      include 'strnum.h' !istv,iste,istp,istc
      integer (kind=4) :: ndf   !Max DOF's/node
      integer (kind=4) :: ndm   ! Mesh spatial dimention
      integer (kind=4) ::  ix(*) ! Element global nodes
      real    (kind=8) :: ul(ndf,nen,*) ! local nodal solution
      real    (kind=8) :: xl(ndm,*)
      real    (kind=8) :: s(nen,*),p(nen)
      integer (kind=4) :: projData(numNames,numWArrs,*)
      integer (kind=4) :: projNums(numWArrs,2)
      ! projData(i,j,1)  - index for name
      ! projData(i,j,2)  - number of compoents
      ! projData(i,j,3)  - index of first component
      ! projData(i,j,4)  - index of last component
      ! projData(i,j,5)  - number of computed components
      ! j=1,2,3,4,5,6    - flag for location of global array: types of
      ! arrays (4 program + 2 user)
      ! projNmus(j,2)    - number of projections
      ! projNums(j,1)=i  - loop on the number of fields for j
      ! i                -the ith projection
      !--------------------------------------------------------------!
      integer (kind=4) :: lgp, lint
      real    (kind=8) :: gip(3,4)
      ! lgp  -number of points in each direction
      ! lint -total number of points
      ! gip(3,4)  -gauss integral points and weights
      ! gip(1,*)-x gip(2,*)-y gip(3,*)- weights
      integer (kind=4) :: igp,i,j,comp,JJ
      !igp   loop for Gauss intergral points
      !i,j,k   loop for
      ! comp   loop for compnent of projection filed
      real    (kind=8) :: shp(3,4),xx,yy,xsj
      !shp(1,i) dN_i/dx shp(2,i) dN_i/dy shp(3,i) N_i
      !xx yy globle coordiante, sxj Jacobian
      real   (kind=8) :: dstraindu(3,8), strain(4), stress(4)
      !dstraindu   -B matrix
      !strain     -
      !stress    -
      real   (kind=8) :: matc(3,3)
      ! igp loop for Gauss integral points
      real   (kind=8) :: energy
      lgp = 2
      call int2d(lgp,lint,gip)
      do igp=1,lint
         call shp2d(gip(1,igp),xl,shp,xsj,ndm,4,ix,.false.)
         xsj = xsj*gip(3,igp)   !Jacobian*weights
         !--------initialize----------------------------------------!
         xx = 0.0d0
         yy = 0.0d0
         strain = 0.0d0
         stress = 0.0d0
         energy =0.d0
         !---------initialize---------------------------------------!
         do j =1,nen ! nen  denots Maximum number of shap function
             xx = xx + shp(3,j)*xl(1,j) !Compute globle coordniate x
             yy = yy + shp(3,j)*xl(2,j) !Compute globle coordinate y
             dstraindu(1,1+2*(j-1))=shp(1,j) !Compute B matrix
             dstraindu(1,2+2*(j-1))=0.0d0
             dstraindu(2,1+2*(j-1))=0.0d0
             dstraindu(2,2+2*(j-1))=shp(2,j)
             dstraindu(3,1+2*(j-1))=shp(2,j)
             dstraindu(3,2+2*(j-1))=shp(1,j)
             !Compute strain
             strain(1)=strain(1) + shp(1,j)*ul(1,j,1)
             strain(2)=strain(2) + shp(2,j)*ul(2,j,1)
             strain(3)=0.0
             strain(4)=strain(4) +shp(2,j)*ul(1,j,1) +
     &                shp(1,j)*ul(2,j,1)
         enddo
         !write(*,*) 'strain(1)=',strain(1)
             ! Compute materials properties
         call mat04(matc,3,3,xx,yy)
         !Compute stress(3)
         stress(1) = matc(1,1)*strain(1)+matc(1,2)*strain(2)
         stress(2) = matc(2,1)*strain(1)+matc(2,2)*strain(2)
         stress(3) = 0.25*(stress(1)+stress(2))
         stress(4) = matc(3,3)*strain(4)
         energy = 0.5*dot_product(stress,strain)
         !write(*,*) '----strain---',strain(1:4)
         !write(*,*) 'projNums(4,1)',projNums(4,1)
         do JJ =1, nel ! loop for node per elment
             p(JJ) = p(JJ) + shp(3,JJ)*xsj
             ! loop on number of projection
             do i = 1, projNums(4,1)
                 comp = 0
                 do j = projData(i,4,3),projData(i,4,4)
                     comp = comp +1
                     if (projData(i,4,1).eq.5) then !stress
                         s(JJ,j) = s(JJ,j) + stress(comp)*shp(3,JJ)*xsj
                     elseif(projData(i,4,1).eq.8) then ! strain
                         s(JJ,j) = s(JJ,j) + strain(comp)*shp(3,JJ)*xsj
                      elseif(projData(i,4,1).eq.24) then
                         s(JJ,j) = s(JJ,j) + energy*shp(3,JJ)*xsj
                     endif
                 enddo !end j
             enddo !end i
         enddo !end JJ1

      enddo   ! end igp
      iste = projNums(4,2)
      !write(*,*) 'p----p=',p(1:4)
      !write(*,*) '---stain---',s(1:4,5)
      !write(*,*) '-----projection----'
      end subroutine pro04
