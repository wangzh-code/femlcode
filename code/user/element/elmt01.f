!$Id:$
      subroutine elmt01(d,ul,xl,ix,tl,s,p,ndf,ndm,nst,isw,pk)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2017: Harbin University of Science and Technology
!                               All rights reserved
!      Athor: Zhi-hai Wang
!      Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
!-----[--.----+----.----+----.-----------------------------------------]
!     Purpose:

!     Inputs:
!       d(*)          - Material set parameters
!       ul(ndf,nen,*) - Solution parameters
!       xl(ndm,nen)   - Element nodal coordinates
!       ix(nen1)      - Element global node numbers
!       tl(nen)       - Element vector (e.g., nodal temperatures)
!       ndf           - Maximum no dof's/node
!       ndm           - Mesh spatial dimension
!       nst           - Element matrix/vector dimension
!       isw           - Switch parameter (set by input commands)
!       pk            - pointer for subroutine

!     Outputs:
!       s(nst,*)      - Element matrix (stiffness, mass, etc.)
!       p(nst)        - Element vector (residual, lump mass, etc.)
!-----[--.----+----.----+----.-----------------------------------------]
      implicit none

      include 'eldata.h'
      include 'cdata.h'
      include 'umac1.h'                    ! utx(1)
      include 'eltran.h'
      include 'iofile.h'
      include 'pointer.h'
      include 'comblk.h'
      include 'strnum.h'
      integer (kind=4) ::  pk              ! pointer for subroutine
      integer (kind=4) ::  ndf             ! Max DOF's/node
      integer (kind=4) ::  ndm             ! Mesh spatial dimention
      integer (kind=4) ::  nst             ! Matrix 's' dimension
      integer (kind=4) ::  isw             ! Switch for solution steps
      integer (kind=4) ::  ix(*)           ! Element global nodes
      real    (kind=8) ::  d(*)            ! Material set parameters
      real    (kind=8) ::  ul(ndf,nen,*)   ! Local nodal solutions
      real    (kind=8) ::  xl(ndm,*)       ! Local nodal coordinates
      real    (kind=8) ::  tl(*)           ! Local nodal load array
      real    (kind=8) ::  s(nst,*)        ! Element matrix
      real    (kind=8) ::  p(nst)        ! Element vector
c-------------------------------------------------------------
      integer (kind=4) :: i,j,ii,jj,i1,j1
      real (kind=8) :: l2,lr,l,eps,bb(ndm,nen),cmd,cmo,db(ndm,nen)
      real (kind=8) :: body,dd,stre
      !write(*,*)'isw ix(:)=',isw,xl(:,1:13)
      !do i=1,1
      !  do j=1,nen
      !    write(*,*) 'i=',i,'ix(j)=',ix(j)
      !  end do
      !end do
c-------------------------------------------------------------
      if(isw.lt.0) then
!       utx(1) = 'Name_U_Want'             ! 15 character naming option
!                                          ! Access: MATErial "ma"
!                                          !   Name_U_Want (instead of USER 1)
!     elseif(isw.eq.0) then
      elseif(isw.eq.0) then! WARNING: Use previous stmt
         write(*,2000)                     ! DELETE THIS when adding
c                                           ! the user module.
      elseif(isw.eq.1) then                ! Input material set data
        !pstyp = ???                       ! Sets plot dimension (1,2,3)
        call inptsb(d)                     ! material properties
        !d(3) density; d(1)elastic modulus,d(2)Poisson's ratio
        !d(62) = 0.d0
        write(*,*)'material', d(1),d(2),d(3)
        istv=31
        !Return: istv = maxiumum number of elment projections
        !--------------------- (default:project 8 quantities)
        !Return: nh1 = number of nh1/nh2 words/element
        !Retuin: nhs = number of nh3     words/element
      elseif(isw.eq.2) then  ! Check input data, element for errors
      elseif(isw.eq.3 .or.isw.eq.6) then
        !Compute residual p(dnf,nen), tangent s(nst,nst)
        !history data base: previous tim step hr(nh1)
        !current tim step hr(nh2), time independent hr(nh3)
        write(*,*)'ix01',ix(1),ix(2)
        l2 = 0.0d0
        do i = 1,ndm
          l2 = l2+(xl(i,2)-xl(i,1))**2
        end do
        l=sqrt(l2)
c       compute strain-displacement matrix
        lr=1.0/l2
        eps = 0.0
        do i = 1,ndm
          bb(i,1)=-(xl(i,2)-xl(i,1))*lr
          bb(i,2)=-bb(i,1)
          eps = eps +bb(i,2)*(ul(i,2,1)-ul(i,1,1))
        end do
        !write(*,*)bb(:,1)
         ! Compute mass terms
        cmd = d(3)*d(2)*l/3.0
        cmo = 0.5d0*cmd
        body = 0.0              !Compute body/inertia force vector
        stre = d(1)*eps*l       !Cpmpute stress
        do i=1,ndm
          p(i) = body*0.5*l-bb(i,1)*stre -cmd*ul(i,1,5)-cmo*ul(i,2,5)
          p(i+3) = body*0.5*l-bb(i,2)*stre -cmd*ul(i,1,5)-cmo*ul(i,2,5)
        end do
        if(isw.eq.3)then
            dd = ctan(1)*d(1)*d(2)*l
            do i = 1,ndm
                db(i,1) = dd*bb(i,1)
                db(i,2) = -db(i,1)
            end do
            i1 = 0
            do ii = 1,nen
                j1 = 0
                do jj = 1,nen
                    do i = 1, ndm
                        do j = 1,ndm
                            s(i+i1,j+j1) = db(i,ii)*bb(j,jj)
                        end do
                    end do
                    j1 = j1+ndf
                end do
                i1 = i1+ndf
            end do
            cmd = ctan(3)*d(3)*d(2)*l/3.0d0
            cmo = cmd*0.5d0
            do i =1,ndm
                j = 1 + ndf
                s(i,i) = s(i,i) + cmd
                s(i,j) = s(i,j) + cmo
                s(j,i) = s(j,i) + cmo
                s(j,j) = s(j,j) + cmd
            end do
            !write(*,*)'s(1,:)',s(1,1:6)
            !write(*,*)'s(2,:)',s(2,1:6)
            !write(*,*)'s(3,:)',s(3,1:6)
            !write(*,*)'s(4,:)',s(4,1:6)
            !write(*,*)'s(5,:)',s(5,1:6)
            !write(*,*)'s(6,:)',s(6,1:6)
        endif
      elseif(isw.eq.4) then    ! Output element data
      elseif(isw.eq.5) then    ! Compute mass matrix
      elseif(isw.eq.8) then    ! Compute projetion
         !p(nen) projections weight: wt(nen)
         !s(nen,*) projection values: st(nen,*)
         !Return: iste = number of projections
         call  prj01(d,xl,s,p,ndm,ndf,ix,ul,mr(up(11)),mr(up(12)),isw)
      elseif(isw.eq.14) then   ! Initialize history variables
      elseif(isw.eq.61) then   !
         call wget01(ix(1),p)
      endif
!     Formats

2000  format('    Elmt  1: *WARNING* Dummy subroutine called')

      end subroutine elmt01
      subroutine wget01(i,ndata)
         implicit none
         include 'prjnam.h'
         include 'strnum.h'
         integer ndat(numNames,3),i
         real*8 ndata(3)
         ! - 0 -> not present
         ! - 1 -> dispalcement in hr(np(40))
         ! - 2 -> rate in hr(np(42))
         ! - 3 -> acceleration in hr(np(42))
         ! - 4 -> projected in hr(np(58))
         ! - 5 and beyond, user array
         ndat=0
         !nw,location flag,nstart (only used if locf = 1,2,or 3{{{2
         !! name of number, components of filds, location flag, nstart
         ndat(1 ,:) = (/3,1,1/)!'Displacements'
         !---------------------------------------------!
         ndata(1) = real(ndat(i,1))
         ndata(2) = real(ndat(i,2))
         ndata(3) = real(ndat(i,3))
      end subroutine wget01
      subroutine prj01(d,xl,s,p,ndm,ndf,ix,ul,projData,projNums,isw)
         ! purpose: get projections
         ! Inputs:
         ! d(*)             - Material set parameters
         ! ul(ndf,nen,*)    - Nodal solution parameters for elment
         ! ndf              - number dof per node
         ! ndm              - Dimension of element arrays
         ! projNums         - Number of projetion fields
         ! Outpu
         ! p(nen)           -element Lumped mass matrix
         ! s(nen,*)         -
         include 'cdata.h'
         include 'prjnam.h'
         include 'eldata.h'
         include 'strnum.h'
         integer (kind=4) ::  ndm
         integer (kind=4) ::  ndf
         integer (kind=4) ::  isw, ix(ndf,*)
         integer projData(numNames,numWArrs,*), projNums(numWArrs,2)
         real*8 xl(ndm,*),ul(ndf,nen,*), d(*)
         real*8 p(nen),s(nen,*)
         iste = projNums(4,2)
      end subroutine prj01
