!$Id:$
      subroutine pzeroi(ii,nn)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Zero integer array of data

!      Inputs:
!         nn     - Length of array

!      Outputs:
!         ii(*)  - Array with zero values
!-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      integer       :: nn
      integer       :: ii(nn)

      save

!     Zero integer array
      ii(1:nn) = 0

      end subroutine pzeroi
