!$Id:$
      subroutine elmlib(d,u,x,ix,t,s,p,i,j,k,jel,isw,pk)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Element library driver routine
!               N.B. Must set library flags in Subroutine PELNUM
!                    for new program modules
!     add pk subpot.h track subroutine
!      Inputs:
!         d(*)    - Material parameters
!         u(*)    - Element solution parameters
!         x(*)    - Element nodal coordinates
!         ix(*)   - Element nodal numbers
!         t(*)    - Element temperatures
!         i       - Number dof/node           (ndf)
!         j       - Spatial dimension of mesh (ndm)
!         k       - Size of element arrays    (nst)
!         jel     - Element type number
!         pk      - pointer name of subroutine
!      Outputs:
!         d(*)    - Material parameters (isw = 1 only)
!         s(*,*)  - Element array
!         p(*)    - Element vector
!-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      include  'eldata.h'
      include  'iofile.h'
      include  'subpot.h'


      integer       :: i,j,k,jel,isw, ix(*)
      real (kind=8) :: p(*),s(*),d(*),u(*),x(*),t(*)
      integer       :: pk

      save

      if(np_sub(2).eq.1)then
         write(*,*) name_sub(pk),'->elmlib'
         write(iow,*) name_sub(pk),'->elmlib'
      endif
      if(isw.ge.3 .and. k.gt.0) then
        call pzero(s,k*k)
        call pzero(p,k  )
      endif

!     User element routines
      if(jel.gt.0) then
        if(    jel.eq. 1) then
          call elmt01(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 2) then
          call elmt02(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 3) then
          call elmt03(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 4) then
          call elmt04(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 5) then
          call elmt05(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 6) then
          call elmt06(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 7) then
          call elmt07(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 8) then
          call elmt08(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq. 9) then
          call elmt09(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.10) then
          call elmt10(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.11) then
          call elmt11(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.12) then
          call elmt12(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.13) then
          call elmt13(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.14) then
          call elmt14(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.15) then
          call elmt15(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.16) then
          call elmt16(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.17) then
          call elmt17(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.18) then
          call elmt18(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.19) then
          call elmt19(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.20) then
          call elmt20(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.21) then
          call elmt21(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.22) then
          call elmt22(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.23) then
          call elmt23(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.24) then
          call elmt24(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.25) then
          call elmt25(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.26) then
          call elmt26(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.27) then
          call elmt27(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.28) then
          call elmt28(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.29) then
          call elmt29(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.30) then
          call elmt30(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.31) then
          call elmt31(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.32) then
          call elmt32(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.33) then
          call elmt33(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.34) then
          call elmt34(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.35) then
          call elmt35(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.36) then
          call elmt36(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.37) then
          call elmt37(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.38) then
          call elmt38(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.39) then
          call elmt39(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.40) then
          call elmt40(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.41) then
          call elmt41(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.42) then
          call elmt42(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.43) then
          call elmt43(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.44) then
          call elmt44(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.45) then
          call elmt45(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.46) then
          call elmt46(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.47) then
          call elmt47(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.48) then
          call elmt48(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.49) then
          call elmt49(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.50) then
          call elmt50(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.51) then
          call elmt51(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.52) then
          call elmt52(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.53) then
          call elmt53(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.54) then
          call elmt54(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.55) then
          call elmt55(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.56) then
          call elmt56(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.57) then
          call elmt57(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.58) then
          call elmt58(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.59) then
          call elmt59(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.60) then
          call elmt60(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.61) then
          call elmt61(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.62) then
          call elmt62(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.63) then
          call elmt63(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.64) then
          call elmt64(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.65) then
          call elmt65(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.66) then
          call elmt66(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.67) then
          call elmt67(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.68) then
          call elmt68(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.69) then
          call elmt69(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.70) then
          call elmt70(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.71) then
          call elmt71(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.72) then
          call elmt72(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.73) then
          call elmt73(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.74) then
          call elmt74(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.75) then
          call elmt75(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.76) then
          call elmt76(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.77) then
          call elmt77(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.78) then
          call elmt78(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.79) then
          call elmt79(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.80) then
          call elmt80(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.81) then
          call elmt81(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.82) then
          call elmt82(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.83) then
          call elmt83(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.84) then
          call elmt84(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.85) then
          call elmt85(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.86) then
          call elmt86(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.87) then
          call elmt87(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.88) then
          call elmt88(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.89) then
          call elmt89(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.90) then
          call elmt90(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.91) then
          call elmt91(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.92) then
          call elmt92(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.93) then
          call elmt93(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.94) then
          call elmt94(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.95) then
          call elmt95(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.96) then
          call elmt96(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.97) then
          call elmt97(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.98) then
          call elmt98(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.99) then
          call elmt99(d,u,x,ix,t,s,p,i,j,k,isw,2)
        elseif(jel.eq.100) then
          call elmt100(d,u,x,ix,t,s,p,i,j,k,isw,2)
        else
          go to 400
        endif

!     Program element library routines
      elseif(isw.gt.0) then

!       1-D Element library
        if(j.eq.1) then

          if(    jel.eq. -1) then
            call solid1d(d,u,x,ix,t,s,p,i,j,k,isw,2)
          elseif(jel.eq. -2) then
            call trussnd(d,u,x,ix,t,s,p,i,j,k,isw,2)
          elseif(jel.eq. -3) then
            write(iow,*) ' No 1-d frame element available. n_el =',n_el
            write(  *,*) ' No 1-d frame element available. n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -4) then
            write(iow,*) ' No 1-d plate element available. n_el =',n_el
            write(  *,*) ' No 1-d plate element available. n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -5) then
            write(iow,*) ' No 1-d shell element available. n_el =',n_el
            write(  *,*) ' No 1-d shell element available. n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -6) then
            write(iow,*) ' No 1-d membrane element available.',
     &                   '  n_el =',n_el
            write(  *,*) ' No 1-d membrane element available.',
     &                   '  n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -7) then
            write(iow,*) ' No 1-d thermal element available.',
     &                   '  n_el =',n_el
            write(  *,*) ' No 1-d thermal element available.',
     &                   '  n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -8) then
            write(iow,*) ' No 1-d thermal convectionelement available.',
     &                   '  n_el =',n_el
            write(  *,*) ' No 1-d thermal convectionelement available.',
     &                   '  n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -9) then
            call pointnd(d,u,s,p,i,k,isw)
          else
            go to 400
          endif

!       2-D element library
        elseif(j.eq.2) then

          if(    jel.eq. -1) then
            call solid2d(d,u,x,ix,t,s,p,i,j,k,isw,2)
          elseif(jel.eq. -2) then
            call trussnd(d,u,x,ix,t,s,p,i,j,k,isw,2)
          elseif(jel.eq. -3) then
            call frame2d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -4) then
            call plate2d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -5) then
            call shell2d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -6) then
            write(iow,*) ' No 2-d membrane element available.',
     &                   '  n_el =',n_el
            write(  *,*) ' No 2-d membrane element available.',
     &                   '  n_el =',n_el
            call plstop(.true.)
          elseif(jel.eq. -7) then
            call therm2d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -8) then
            call convec2d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -9) then
            call pointnd(d,u,s,p,i,k,isw)
          else
            go to 400
          endif

!       3-D Element library
        elseif(j.eq.3) then

          if(    jel.eq. -1) then
            call solid3d(d,u,x,ix,t,s,p,i,j,k,isw,2)
          elseif(jel.eq. -2) then
            call trussnd(d,u,x,ix,t,s,p,i,j,k,isw,2)
          elseif(jel.eq. -3) then
            call frame3d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -4) then
            write(iow,*) ' No 3-d plate element available:',
     &                   '  Use SHELL.',n_el
            write(  *,*) ' No 3-d plate element available:',
     &                   '  Use SHELL.',n_el
            call plstop(.true.)
          elseif(jel.eq. -5) then
            call shell3d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -6) then
            call membr3d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -7) then
            call therm3d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -8) then
            call convec3d(d,u,x,ix,s,p,i,j,k,isw)
          elseif(jel.eq. -9) then
            call pointnd(d,u,s,p,i,k,isw)
          else
            go to 400
          endif

        endif

      endif

      return

!     Error
400   write(iow,4000) n_el,jel,isw
      write(  *,4000) n_el,jel,isw
      call plstop(.true.)

!     Format
4000  format('  *ERROR* ELMLIB: Element:',i6,', type number',i3,
     &       ' input, isw =', i3)

      end subroutine elmlib
