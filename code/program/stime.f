!$Id:$
      subroutine stime()

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Initialize time clock

!      Inputs:
!         none

!      Outputs:
!         none      - Output is tim0 in common etime1
!-----[--.----+----.----+----.-----------------------------------------]

      implicit  none

      include  'etime1.h'

      real (kind=4) :: etime, tt(2)

      save

      tim0 = 0.0d0
      tim0 = etime(tt)
      tim0 = tt(1)

      end
