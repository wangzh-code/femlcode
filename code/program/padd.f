!$Id:$
      function padd(val)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

      implicit  none

      real (kind=8) :: padd, val, xval

      save

      data      xval /0.0d0/

!     Look at parameter

      if(val.eq.0.0d0) then
        xval = 0.0d0
      else
        xval = xval + val
      endif

      padd = xval

      end function padd
