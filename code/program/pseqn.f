!$Id:$
      subroutine pseqn(ip,numnp)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27

!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose: Set sequential numbers to integer vector

!      Inputs:
!         numnp  - Number nodes in mesh

!      Outputs:
!         ip(*)  - Equation numbers
!-----[--.----+----.----+----.-----------------------------------------]
      implicit  none

      integer       :: n, numnp
      integer       :: ip(*)

      save

      do n = 1,numnp
        ip(n) = n
      end do

      end subroutine pseqn
