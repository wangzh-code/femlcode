!$Id:$
      function sind(x)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Harbin University of Science and Technology
!                               All rights reserved
!....  Author: Zhi-hai Wang
!....  Email: wangzh@hrbust.edu.cn
!....  date: 2020-4-27
      implicit none

      real (kind=8) :: sind, x

      sind = sin(atan(1.d0)*x/45.d0)

      end
